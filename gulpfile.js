// package vars
const pkg = require('./package.json');

// gulp
const gulp = require('gulp');

// load all plugins in "devDependencies" into the variable $
const $ = require('gulp-load-plugins')({
  pattern: ['*'],
  scope: ['devDependencies'],
});

// error logging
const onError = err => {
  console.log('ERROR');
  console.log(err);
};

// Our banner
const banner = (() => {
  let result = '';
  try {
    result = [
      '/**',
      ' * @project        <%= pkg.name %>',
      ' * @author         <%= pkg.author %>',
      ' * @build          ' + $.moment().format('llll') + ' ET',
      ' * @release        ' +
        $.gitRevSync.long() +
        ' [' +
        $.gitRevSync.branch() +
        ']',
      ' * @copyright      Copyright (c) ' +
        $.moment().format('YYYY') +
        ', <%= pkg.copyright %>',
      ' *',
      ' */',
      '',
    ].join('\n');
  } catch (err) {}
  return result;
})();

const reload = $.browserSync.reload;

// Clean the public folder
gulp.task('clean', function() {
  $.fancyLog('-> Cleaning ' + pkg.config.dist.assets);
  $.del(pkg.config.dist.assets);
});

// Move the image folder
gulp.task('images', function() {
  $.fancyLog('-> Minimizing images in ' + pkg.config.src.img);
  gulp
    .src(pkg.config.src.img + '**/*.{png,jpg,jpeg,gif,svg}')
    .pipe($.plumber({ errorHandler: onError }))
    .pipe(gulp.dest(pkg.config.dist.img))
    .on('end', reload);
});

// Move the fonts folder
gulp.task('fonts', function() {
  $.fancyLog('-> Moving fonts from ' + pkg.config.src.fonts);
  gulp
    .src(pkg.config.src.fonts)
    .pipe($.plumber({ errorHandler: onError }))
    .pipe(gulp.dest(pkg.config.dist.assets))
    .on('end', reload);
});

// scss - build the scss to the build folder, including the required paths, and writing out a sourcemap
gulp.task('scss', () => {
  $.fancyLog('-> Compiling scss');
  gulp
    .src(pkg.config.src.scss + '**/*.scss')
    .pipe($.plumber({ errorHandler: onError }))
    .pipe($.sourcemaps.init({ loadMaps: true }))
    .pipe(
      $.sass({
        includePaths: pkg.config.scss,
      }).on('error', $.sass.logError)
    )
    .pipe($.cached('sass_compile'))
    .pipe($.autoprefixer())
    .pipe($.sourcemaps.write('./'))
    .pipe($.size({ gzip: true, showFiles: true }))
    .pipe(gulp.dest(pkg.config.dist.css))
    .pipe($.browserSync.stream());
});

gulp.task('js-lint', function() {
  $.fancyLog('-> Linting javascript');
  gulp
    .src([
      pkg.config.src.js + '**/*.js',
      '!' + pkg.config.src.js + 'vendors/**/*.js',
    ])
    .pipe($.plumber({ errorHandler: onError }))
    .pipe($.eslint())
    .pipe($.eslint.format())
    .pipe($.eslint.failAfterError());
});

//Move shared js files to the correct place, if they are needed
gulp.task('js-vendors', function() {
  $.fancyLog('-> Moving js vendors');
  gulp
    .src([pkg.config.src.js + 'vendors/**/*.js'])
    .pipe($.plumber({ errorHandler: onError }))
    .pipe(gulp.dest(pkg.config.dist.js + 'vendors/'));
});

// js
gulp.task('js', ['js-vendors', 'js-lint'], function() {
  $.fancyLog('-> Building js');
  //Combining and minifying js
  gulp
    .src([
      pkg.config.src.js + '**/*.js',
      '!' + pkg.config.src.js + '**/_*.js',
      '!' + pkg.config.src.js + 'vendors/**/*.js',
    ])
    .pipe($.plumber({ errorHandler: onError }))
    .pipe($.sourcemaps.init())
    .pipe(
      $.includeExt({
        includePaths: [pkg.config.src.js, './node_modules/'],
        separateInputs: true,
      })
    )
    .pipe($.uglify())
    .pipe($.sourcemaps.write('/maps'))
    .pipe($.header(banner, { pkg: pkg }))
    .pipe($.size({ gzip: true, showFiles: true }))
    .pipe(gulp.dest(pkg.config.dist.js))
    .on('end', reload);
});

// Static Server + watching scss/twig files
gulp.task('default', ['fonts', 'images', 'js', 'scss'], function() {
  $.browserSync.init({
    proxy: pkg.config.devUrl,
    port: 3000,
    notify: false,
    open: false,
    reloadOnRestart: true,
    ghostMode: {
      clicks: false,
      forms: false,
      scroll: false,
    },
  });

  //Watch for filechange
  gulp.watch([pkg.config.src.scss + '**/*.scss'], ['scss']);
  gulp.watch([pkg.config.src.img + '**/*.{png,jpg,jpeg,gif,svg}'], ['images']);
  gulp.watch([pkg.config.src.fonts + '**/*'], ['fonts']);
  gulp.watch([pkg.config.src.js + '**/*.js'], ['js']);
  gulp.watch(pkg.config.templates + '**/*.twig', reload);
});

// Cache busting task
gulp.task('cachebust', function() {
  return gulp
    .src(pkg.config.templates + '_layouts/*.twig')
    .pipe($.replace(/app.css\?v=([0-9]*)/g, 'app.css?v=' + $.gitRevSync.long()))
    .pipe($.replace(/app.js\?v=([0-9]*)/g, 'app.js?v=' + $.gitRevSync.long()))
    .pipe(gulp.dest(pkg.config.templates + '_layouts/'));
});

gulp.task('build', ['fonts', 'images', 'js', 'scss', 'cachebust']);
