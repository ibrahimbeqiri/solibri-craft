/* global YT, trialDownloadApp, _, mobilecheck */

var player, iframe, playerState;
var videoId = '';

// eslint-disable-next-line no-unused-vars
var loadYoutube = function(VId) {
  if (!_.isUndefined(YT)) {
    player = null;
    var containerSelector = '.site';
    videoId = !_.isUndefined(VId) && VId.length ? VId : videoId;

    var $videoContainer = $(containerSelector).find('.video_will_be_here');
    $videoContainer.html('<div id="player"></div>');

    if (!_.isUndefined(YT.Player)) {
      player = new YT.Player('player', {
        videoId: !_.isUndefined(VId) && VId.length ? VId : videoId,
        playerVars: {
          controls: 0,
          showinfo: 0,
          loop: 0,
          rel: 0,
          showsearch: 0,
          iv_load_policy: 3,
        },
        events: {
          onReady: function(event) {
            // console.log('it is ready');
            player = event.target;
            iframe = $('#player')[0];
            // player.loadVideoById(videoId);
            if (_.isUndefined(trialDownloadApp)) {
              event.target.playVideo();
            } else {
              $videoContainer.parents('.youtube-video').addClass('is-paused');
            }
            setupListener(containerSelector);
            videoResize(containerSelector);
          },
          onStateChange: function(event) {
            // console.log('State Change');
            // console.log(event);
            playerState = event.data; // 0: ended, 1: playing, 2: pause

            if (playerState === YT.PlayerState.PLAYING) {
              $('.video_will_be_here iframe').css('opacity', 1);
              $videoContainer
                .parents('.youtube-video')
                .removeClass('mobile-init');
              $videoContainer
                .parents('.youtube-video')
                .removeClass('is-paused');
              $videoContainer.parents('.youtube-video').addClass('is-playing');
            }

            if (playerState === YT.PlayerState.PAUSED) {
              $videoContainer
                .parents('.youtube-video')
                .removeClass('is-playing');
              $videoContainer.parents('.youtube-video').addClass('is-paused');
            }

            if (playerState === YT.PlayerState.ENDED) {
              if ($('.progress-bar').length > 0) {
                clearIntervals();
              }
              if (
                !_.isUndefined(trialDownloadApp) &&
                trialDownloadApp._isMounted
              ) {
                trialDownloadApp.video.is_finnished = true;
                trialDownloadApp.updateStorage();
              }
            }
          },
          onError: function() {
            // console.log('Error!');
            // console.log(error);
            player.stopVideo();
            player.loadVideoById('');
          },
          onApiChange: function(event) {
            // console.log('API Change');
            // console.log(event.target);
            $videoContainer.parents('.youtube-video').addClass('is-loaded');
            var $iframePlayer = $videoContainer.find('iframe#player');

            $iframePlayer.bind('DOMNodeRemoved', function() {
              $videoContainer
                .parents('.youtube-video')
                .removeClass('is-loaded');
            });

            if (mobilecheck()) {
              $videoContainer.parents('.youtube-video').addClass('is-mobile');
              $videoContainer.parents('.youtube-video').addClass('mobile-init');
            }

            if ($('.progress-bar').length > 0) {
              updateAll();
              setIntervals();
            }
          },
        },
      });
    }
  }
};

var isFullscreen = false;
// Custom controller for the video
function setupListener(containerSelector) {
  $(containerSelector)
    .find('.fullscreen-btn')
    .on('click', fullscreen);
  $(containerSelector)
    .find('.youtube-video__overlay')
    .on('click', toogleVideo);
}

function fullscreen() {
  var requestFullScreen =
    iframe.requestFullScreen ||
    iframe.mozRequestFullScreen ||
    iframe.webkitRequestFullScreen ||
    iframe.msRequestFullscreen;
  if (requestFullScreen) {
    requestFullScreen.bind(iframe)();
  }
}

function toogleVideo() {
  if (playerState === YT.PlayerState.PLAYING) {
    player.pauseVideo();
  } else {
    player.playVideo();
  }
}

$(document).on(
  'webkitfullscreenchange mozfullscreenchange fullscreenchange onmsfullscreenchange MSFullscreenChange',
  function() {
    var isInFullScreen =
      _.isNull(document.fullscreenElement) ||
      _.isNull(document.webkitFullscreenElement) ||
      _.isNull(document.mozFullScreenElement) ||
      _.isNull(document.msFullscreenElement);
    if (isInFullScreen) {
      isFullscreen = false;
    } else {
      isFullscreen = true;
    }
  }
);

var ratio;
var videoInitialWidth;
var videoInitialHeight;
var videoResize = function(containerSelector) {
  if (isFullscreen !== true) {
    var $videoContainer = $(containerSelector).find('.video_will_be_here');
    var $video = $videoContainer.find('#player');
    videoInitialWidth = _.isUndefined(videoInitialWidth)
      ? $video.outerWidth()
      : videoInitialWidth;
    videoInitialHeight = _.isUndefined(videoInitialHeight)
      ? $video.outerHeight()
      : videoInitialHeight;
    ratio = _.isUndefined(ratio)
      ? videoInitialWidth / videoInitialHeight
      : ratio;

    var width;
    var height;
    width = $videoContainer.outerWidth();
    height = width / ratio;

    $video.css('width', width);
    $video.css('height', height);
  }
};

// Update HTML nodes on the page
// with most recent values from
// the YouTube iFrame API

// Array to track all HTML nodes
var nodeList = ['duration', 'currentTime', 'percentage'];
var duration, currentTime;
function update(node) {
  switch (node) {
    // Update player reported changes
    case 'duration':
      duration = _.isUndefined(player.getDuration())
        ? 0
        : parseInt(player.getDuration());
      break;
    case 'currentTime':
      currentTime = _.isUndefined(player.getCurrentTime())
        ? 0
        : parseInt(player.getCurrentTime());
      break;
    case 'percentage':
      var percentage = parseInt(currentTime / duration * 100);
      progressBar(percentage);
      break;
  }
}
// Updates all HTML nodes
function updateAll() {
  for (var node in nodeList) {
    update(nodeList[node]);
  }
}

// Controls interval handlers to update page contens
// Array to track intervals
var activeIntervals = [];
function setIntervals() {
  activeIntervals[0] = setInterval(function() {
    update('currentTime');
  }, 500);
  activeIntervals[1] = setInterval(function() {
    update('duration');
  }, 500);
  activeIntervals[2] = setInterval(function() {
    update('percentage');
  }, 500);
}
function clearIntervals() {
  // Clears existing intervals to actively update page content
  for (var interval in activeIntervals) {
    clearInterval(interval);
  }
}

var progressBar = function(percentage) {
  var $bar = $('.progress-bar__inner');

  $bar.css('width', percentage + '%');
};

$(window).resize(function() {
  if ($('.video_will_be_here iframe').length > 0) {
    $('.video_will_be_here iframe').each(function() {
      var container = $(this).parents('.youtube-video__container');
      videoResize(container);
    });
  }
});
