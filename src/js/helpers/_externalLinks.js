$(document).ready(function() {
  $('a')
    .filter('[href^="http"], [href^="//"]')
    .not('[href*="' + window.location.host + '"]')
    .attr('rel', 'nofollow noopener noreferrer')
    .not('.trusted')
    .attr('target', '_blank');
});
