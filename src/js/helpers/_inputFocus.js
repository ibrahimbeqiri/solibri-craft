$(document).ready(function() {
  $('[data-input-focus]').on('focus', function() {
    $(this)
      .parent()
      .addClass('is-focused');
  });

  $('[data-input-focus]').on('blur', function() {
    $(this)
      .parent()
      .removeClass('is-focused');
  });
});
