var preventDefaultOnHash = function() {
  var $aWithHash = $('a[href="#"]');

  $aWithHash.on('click', function(event) {
    event.preventDefault();
  });
};

$(document).ready(function() {
  preventDefaultOnHash();
});
