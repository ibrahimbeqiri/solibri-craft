/* global Blazy */
//=require blazy/blazy.js

var lazyloadImages = function() {
  var bLazy = new Blazy({
    breakpoints: [
      {
        width: 425,
        src: 'data-src-small',
      },
      {
        width: 768,
        src: 'data-src-medium',
      },
    ],
    success: function(element) {
      $(element)
        .parent()
        .addClass('loaded');
    },
  });

  $(document).on('images:revalidate', function() {
    bLazy.revalidate();
  });
};

$(document).ready(function() {
  lazyloadImages();
});
