var resizingClass = function() {
  var resizeTimer;
  var $body = $('body');
  var resizingClass = 'resizing';

  $(window).on('resize', function(e) {
    if (!$body.hasClass(resizingClass)) {
      $body.addClass(resizingClass);
    }
    clearTimeout(resizeTimer);
    resizeTimer = setTimeout(function() {
      $body.removeClass(resizingClass);
      $(document).trigger('resized');
    }, 250);
  });
};

$(document).ready(function() {
  resizingClass();
});
