/* global Vue, VueChartJs */

//=require vue/dist/vue.min.js
//=require chart.js/dist/Chart.min.js
//=require vue-chartjs/dist/vue-chartjs.js
//=require vue-airbnb-style-datepicker/dist/vue-airbnb-style-datepicker.min.js

Vue.config.devtools = false;
Vue.config.debug = false;
Vue.config.silent = true;

var ajaxGet = function(url, callback) {
  var xmlhttp = new XMLHttpRequest();
  xmlhttp.onreadystatechange = function() {
    if (this.readyState === 4 && this.status === 200) {
      var response = JSON.parse(this.responseText);
      callback(response);
    }
  };
  xmlhttp.open('GET', url);
  xmlhttp.send();
};

var debounce = function(func, wait, immediate) {
  var timeout;
  return function() {
    var context = this;
    var args = arguments;
    var later = function() {
      timeout = null;
      if (!immediate) func.apply(context, args);
    };
    var callNow = immediate && !timeout;
    clearTimeout(timeout);
    timeout = setTimeout(later, wait);
    if (callNow) func.apply(context, args);
  };
};

if (document.getElementById('reports')) {
  // table columns
  var tableColumns = [
    {
      label: 'Date',
      field: 'dateCreated',
      sort: true,
    },
    {
      label: 'Area of business',
      field: 'field_areaOfBusiness',
      sort: true,
    },
    {
      label: 'Company',
      field: 'field_company',
      sort: true,
    },
    {
      label: 'Personnel',
      field: 'field_personnel',
      sort: true,
    },
    {
      label: 'City',
      field: 'field_city',
      sort: true,
    },
    {
      label: 'State',
      field: 'field_state',
      sort: true,
    },
    {
      label: 'Country',
      field: 'field_countries',
      sort: true,
    },
    {
      label: 'First Name',
      field: 'field_firstname',
      sort: true,
    },
    {
      label: 'Last Name',
      field: 'field_lastname',
      sort: true,
    },
    {
      label: 'Email',
      field: 'field_email',
      sort: true,
    },
    {
      label: 'Phone Number',
      field: 'field_phone',
      sort: true,
    },
    {
      label: 'Website',
      field: 'field_website1',
      sort: true,
    },
  ];

  // Date picker
  var datepickerOptions = {
    sundayFirst: true,
    colors: {
      selected: '#FFC800',
      inRange: '#3595f2',
      selectedText: '#fff',
      text: '#565a5c',
      inRangeBorder: '#3595f2',
      disabled: '#fff',
    },
  };

  Vue.use(window.AirbnbStyleDatepicker, datepickerOptions);

  // Data visualization

  Vue.component('piechart', {
    extends: VueChartJs.Pie,
    mixins: [VueChartJs.mixins.reactiveProp],
    props: ['options', 'title'],
    mounted: function() {
      this.renderChart(this.chartData, this.options);
    },
  });

  // Main vue app

  // Default options
  var defaultOptions = {
    formId: '153,2224,2226',
    countries: window.countries,
    referrer: window.referrer,
    startDate: null,
    endDate: null,
    sortField: 'dateCreated',
    sortType: 'desc',
    search: '',
  };

  // Create the vue instance
  // eslint-disable-next-line no-unused-vars
  var app = new Vue({
    el: '#reports',
    delimiters: ['<%', '%>'],
    data: {
      columns: tableColumns,
      sortValue: 'dateCreated, desc',
      rows: [],
      options: defaultOptions,
      loading: false,
      isTyping: false,
      pagination: {
        perPage: 25,
        total: 0,
        currentPage: 1,
        totalPages: 0,
      },
      businessAreaChartData: [],
      companySizeChartData: [],
      dateOpen: false,
    },
    created: function() {
      this.fetchRows();
    },
    watch: {
      'options.search': debounce(function() {
        this.isTyping = false;
      }, 1000),
      isTyping: function(value) {
        if (!value) {
          this.search();
        }
      },
    },
    computed: {
      sortedRows: function() {
        var $this = this;

        var output = $this.rows
          .sort(function(a, b) {
            var modifier = 1;
            if ($this.options.sortType === 'desc') {
              modifier = -1;
            }
            if (a[$this.options.sortField] < b[$this.options.sortField]) {
              return -1 * modifier;
            }
            if (a[$this.options.sortField] > b[$this.options.sortField]) {
              return 1 * modifier;
            }
            return 0;
          })
          .filter(function(row, index) {
            var currentPage = $this.pagination.currentPage;
            var perPage = $this.pagination.perPage;

            var start = (currentPage - 1) * perPage;
            var end = currentPage * perPage;

            if (index >= start && index < end) {
              return true;
            }
          });

        return output;
      },
    },
    methods: {
      // Fetch data from the api
      fetchRows: function() {
        // If we are loading, do nothing
        if (this.loading) {
          return false;
        }
        // Set loading status
        this.loading = true;

        // Create a url object
        var apiUrl = new URL(window.rowsApi);
        var options = this.options;

        // Add the options to the url
        Object.keys(options).forEach(function(key) {
          if (options[key]) {
            return apiUrl.searchParams.append(key, options[key]);
          }
        });

        var $this = this;
        // Fetch response
        ajaxGet(apiUrl.href, function(response) {
          // Set rows
          $this.rows = response.data;

          // Set pagination

          // Set dates
          $this.options.startDate = response.meta.startDate;
          $this.options.endDate = response.meta.endDate;

          // Set pagination details
          $this.pagination.total = response.meta.pagination.total;
          $this.pagination.currentPage = 1;
          $this.pagination.totalPages = Math.round(
            response.meta.pagination.total / $this.pagination.perPage
          );
          // Set pie chart data
          $this.setPieChartData(
            response.data,
            'field_areaOfBusiness',
            'businessAreaChartData'
          );
          $this.setPieChartData(
            response.data,
            'field_personnel',
            'companySizeChartData'
          );
          // Set loading to false
          $this.loading = false;
        });
      },

      setPieChartData: function(data, source, target) {
        this[target] = [];
        var dataFiltered = {};

        data.map(function(a) {
          var key = a[source];
          if (dataFiltered.hasOwnProperty(key)) {
            dataFiltered[key] = dataFiltered[key] + 1;
          } else {
            dataFiltered[key] = 1;
          }
        });

        var labels = Object.keys(dataFiltered);
        var chartData = Object.values(dataFiltered);

        this[target] = {
          labels: labels,
          datasets: [
            {
              data: chartData,
              backgroundColor: [
                '#FFC800',
                '#49BCFF',
                '#9D88F8',
                '#7CC576',
                '#F2687D',
                '#222E45',
                '#ffa902',
                '#2781F0',
                '#5F686F',
                '#ff00a1',
                '#54a700',
                '#dfff00',
              ],
            },
          ],
        };
      },
      sort: function(sortBy, available) {
        if (!available) {
          return false;
        }
        // If we are loading, do nothing
        if (this.loading) {
          return false;
        }
        // change order if sort is already selectef
        if (sortBy === this.options.sortField) {
          this.options.sortType =
            this.options.sortType === 'asc' ? 'desc' : 'asc';
        }
        // Set the new sort by
        this.options.sortField = sortBy;
      },
      sortBy: function() {
        if (this.loading) {
          return false;
        }

        var selectedValue = this.sortValue.split(', ');
        var sortBy = selectedValue[0];
        var direction = selectedValue[1];

        this.options.sortType = direction;
        this.options.sortField = sortBy;
      },
      // Next page
      nextPage: function() {
        // If we are loading, do nothing
        if (this.loading) {
          return false;
        }
        // if page is smaller than total pages, fetch next data
        if (this.pagination.currentPage < this.pagination.totalPages) {
          this.pagination.currentPage = this.pagination.currentPage + 1;
        } else {
          this.pagination.currentPage = this.pagination.totalPages;
        }
      },
      // Prev page
      prevPage: function() {
        // If we are loading, do nothing
        if (this.loading) {
          return false;
        }
        // if page is larger than 1, fetch prev data
        if (this.pagination.currentPage > 1) {
          this.pagination.currentPage = this.pagination.currentPage - 1;
        } else {
          this.pagination.currentPage = 1;
        }
      },
      // Change amount per page
      changeAmount: function() {
        // If we are loading, do nothing
        if (this.loading) {
          return false;
        }

        var totalPages = Math.round(
          this.pagination.total / this.pagination.perPage
        );

        // Calculate the totalPages again
        this.pagination.totalPages = totalPages > 1 ? totalPages : 1;

        // jump to the first page
        this.pagination.currentPage = 1;
      },
      // Change the dates
      changeDate: function(val, target) {
        // If we are loading, do nothing
        if (this.loading) {
          return false;
        }
        // set the start date
        this.options[target] = val;
      },
      searchByDate: function() {
        this.fetchRows();
        this.dateOpen = false;
      },
      openDate: function(event) {
        event.preventDefault();
        if (this.dateOpen) {
          return false;
        }
        this.dateOpen = true;
      },
      closeDate: function(event) {
        if (!this.dateOpen) {
          return false;
        }
        this.dateOpen = false;
      },
      search: function() {
        // If we are loading, do nothing
        if (this.loading) {
          return false;
        }
      },
      csvExport: function() {
        var $this = this;

        // Take our the column names in the same order as the table
        var columnsNames = $this.columns.map(function(col) {
          return col.label;
        });

        // Start the csv content with the column names
        var csv = columnsNames.join(',') + '\n';

        // Continue with the rows
        $this.rows.forEach(function(row) {
          var rowContent = '';
          $this.columns.forEach(function(col, idx, array) {
            if (row[col.field] !== null) {
              rowContent += '"' + row[col.field].replace(/"/g, "'") + '"';
            } else {
              rowContent += '';
            }

            if (idx !== array.length - 1) {
              rowContent += ',';
            }
          });

          csv += rowContent;
          csv += '\n';
        });

        var start = $this.options.startDate;
        var end = $this.options.endDate;

        var link = document.getElementById('download-csv-hidden');

        link.href = 'data:text/csv;charset=utf-8,' + encodeURI(csv);
        link.target = '_blank';
        link.download = 'solibri_report_' + start + '_' + end + '.csv';
        link.click();
      },
    },
  });
}
