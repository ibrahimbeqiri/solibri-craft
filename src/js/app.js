//---------------------------------------
// Require or Include the js files you want in the correct order
// They look like comments but are included by gulp
//---------------------------------------

//=require helpers/_lazyloadImages.js
//=require helpers/_resizingClass.js
//=require helpers/_youtubeAPI.js
//=require helpers/_detectMobile.js
//=require helpers/_preventDefaultOnHash.js
//=require helpers/_inputFocus.js
//=require helpers/_externalLinks.js

//=require components/_formInputs.js
//=require components/_cookieBanner.js
//=require components/_gallery.js
//=require components/_header.js
//=require components/_footer.js
//=require components/_trialDownload.js
//=require components/_featuresSection.js
//=require components/_officeSearchApp.js
//=require components/_cardsSection.js
//=require components/_subNavigation.js
//=require components/_careers.js
//=require components/_accordion.js
//=require components/_fileUpload.js
//=require components/_popupModal.js
//=require components/_scrollTo.js
//=require components/_hero.js
//=require components/_stylizedForm.js
//=require components/_banner.js
//=require components/_learnSearchApp.js
//=require components/_sectionAnim.js
//=require components/_alert.js
//=require components/_lockScreen.js
