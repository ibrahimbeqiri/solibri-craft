var toggleChildNav = function() {
  $('.toggle-child-nav').on('click', function() {
    $(this).toggleClass('is-active');
    $(this)
      .siblings('ul')
      .toggleClass('is-active');
  });
};

$(document).ready(function() {
  toggleChildNav();
});
