/* global Vue, _ */
//=require vue/dist/vue.min.js
//=require lodash/lodash.js

// Comment these three for local build.
Vue.config.devtools = false;
Vue.config.debug = false;
Vue.config.silent = true;

var query = window.location.search.substring(1);

// console.log(query);

function getQueryParam(param) {
  var params = query.split('&');
  for (var i = 0; i < params.length; i++) {
    var pair = params[i].split('=');
    if (decodeURIComponent(pair[0]) === param) {
      return decodeURIComponent(pair[1]);
    }
  }
  return false;
}

// Site search application
var learnSearchApp;

var typingTimer;
var doneTypingTimeout = 200;

var learnSearch = function(learnSearchForm) {
  var $learnSearchForm = $(learnSearchForm);

  learnSearchApp = new Vue({
    el: learnSearchForm,
    delimiters: ['<%', '%>'],
    // Here we can register any values or collections that hold data
    data: {
      learnSearch: '',
      autocomplete: {
        isOpen: false,
        isTyping: false,
        results: [],
        isLoading: false,
        arrowCounter: 0,
      },
      inputFocused: false,
    },
    created: function() {
      // Set search on page load
      var $setSearch = getQueryParam('q');
      if ($setSearch.length) {
        Vue.set(this, 'learnSearch', $setSearch);
      }
    },
    // Anything within the ready function will run when the application loads
    ready: function() {},
    watch: {
      learnSearch: function(val, oldVal) {
        var self = this;
        // Only do a search after stopped typing
        clearTimeout(typingTimer);

        setTimeout(function() {
          self.autocompleteFilterResults();
          self.autocomplete.isOpen = true;
          self.setHistory();
        }, doneTypingTimeout);
      },
    },
    methods: {
      setHistory: function() {
        var self = this;
        var params = '?';

        if (self.learnSearch.length) {
          params += 'q=' + self.learnSearch;
        }

        if (window.history && window.history.replaceState) {
          history.replaceState(null, null, params);

          return params;
        }
      },
      autocompleteFilterResults: function() {
        var self = this;
        var autocompleteItems = [];

        self.autocomplete.isLoading = true;

        if (self.learnSearch.length >= 3) {
          $.ajax({
            url: '/api/learn/search',
            type: 'get',
            dataType: 'json',
            async: true,
            data: { q: self.learnSearch },
            success: function(data) {
              _.each(data.data, function(itm) {
                autocompleteItems.push(itm);
              });

              Vue.set(self.autocomplete, 'results', autocompleteItems);

              self.autocomplete.isLoading = false;
            },
          });
        }
      },
      autocompleteSetResult: function(result) {
        this.learnSearch = result;
        this.autocomplete.isOpen = false;
      },
      autocompleteOnArrowDown: function(evt) {
        if (this.autocomplete.arrowCounter < this.autocomplete.results.length) {
          this.autocomplete.arrowCounter = this.autocomplete.arrowCounter + 1;
        }
      },
      autocompleteOnArrowUp: function() {
        if (this.autocomplete.arrowCounter > 0) {
          this.autocomplete.arrowCounter = this.autocomplete.arrowCounter - 1;
        }
      },
      autocompleteOnEnter: function() {
        var counter = this.autocomplete.arrowCounter;
        var selected = this.autocomplete.results[counter];

        if (selected) {
          window.location.href = selected.url;
        }

        this.autocomplete.isOpen = false;
        this.autocomplete.arrowCounter = -1;
      },
      autocompleteHandleClickOutside: function(evt) {
        if (!this.$refs.form.contains(evt.target)) {
          this.autocomplete.isOpen = false;
          this.autocomplete.arrowCounter = -1;
        }
      },
    },
    mounted: function mounted() {
      document.addEventListener('click', this.autocompleteHandleClickOutside);
    },
    destroyed: function destroyed() {
      document.removeEventListener(
        'click',
        this.autocompleteHandleClickOutside
      );
    },
  });
  learnSearchApp.$mount($learnSearchForm);
};

$(document).ready(function() {
  if ($('#learnSearchApp').length > 0) {
    learnSearch('#learnSearchApp');
  }
});
