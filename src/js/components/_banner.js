/* global parallax */
//=require scrollmonitor/scrollMonitor.js
//=require scrollmonitor-parallax/index.js

var bannerParallax = function() {
  if ($('.banner').length) {
    var parallaxRoot = parallax.create($('.banner').first());

    parallaxRoot.add(document.getElementById('banner__tower'), 0.2);
  }
};

$(document).ready(function() {
  bannerParallax();
});
