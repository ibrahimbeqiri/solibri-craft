//=require flickity/dist/flickity.pkgd.min.js

var cardsSectionCarousel = function() {
  if ($(window).width() <= 425) {
    var $carousel = $('.cardsSection-carousel');
    $carousel.flickity({
      // options
      setGallerySize: true,
      cellAlign: 'center',
      contain: true,
      wrapAround: true,
      autoPlay: 5000,
      pauseAutoPlayOnHover: true,
      accessibility: false,
      draggable: true,
      prevNextButtons: false,
      pageDots: false,
      groupCells: true,
    });
    checkTallest($carousel);
  }
};

var checkTallest = function(carousel) {
  var tallest = 0;
  carousel.find('.carousel-cell').each(function() {
    tallest = $(this).outerHeight() > tallest ? $(this).outerHeight() : tallest;
  });

  carousel.find('.carousel-cell').css('height', tallest);
};

$(window).on('resize', function() {
  if ($(window).width() <= 425) {
    $('.cardsSection-carousel')
      .find('.carousel-cell')
      .css('height', 'auto');
  }
});

$(document).on('resized', function() {
  if ($(window).width() <= 425) {
    var $carousel = $('.cardsSection-carousel');
    checkTallest($carousel);
  }
});

$(document).ready(function() {
  cardsSectionCarousel();
});
