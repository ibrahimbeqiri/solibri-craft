/* global parallax */

//=require scrollmonitor/scrollMonitor.js
//=require scrollmonitor-parallax/index.js

var formParallax = function() {
  if ($('.stylizedForm-section').length) {
    var parallaxRoot = parallax.create($('.stylizedForm-section').first());

    parallaxRoot.add(document.getElementById('stylizedForm-tower'), 0.5);
  }
};

$(document).ready(function() {
  formParallax();
});
