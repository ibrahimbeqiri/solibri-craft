/* global parallax */

// var featuresSection = function() {
//   var featureNav = $('.features-navigation');
//   var featureBlock = $('.feature-block');
//   var $window = $(window);
//   if (featureNav.length) {
//     var offsets = {};
//     var setOffsets = function() {
//       featureBlock.each(function() {
//         offsets[$(this).attr('id')] = {
//           top: $(this).offset().top - 72,
//           bottom: $(this).offset().top + $(this).height() + 72,
//         };
//       });
//     };

//     setOffsets();

//     var firstOffset = offsets[Object.keys(offsets)[0]];
//     var lastOffset =
//       offsets[Object.keys(offsets)[Object.keys(offsets).length - 1]];

//     $window.on('scroll', function() {
//       $.each(offsets, function(key, val) {
//         if (
//           $window.scrollTop() >= val.top &&
//           $window.scrollTop() <= val.bottom
//         ) {
//           featureNav.find('a[href="#' + key + '"]').addClass('is-active');
//         } else {
//           featureNav.find('a[href="#' + key + '"]').removeClass('is-active');
//         }
//       });

//       if (
//         $window.scrollTop() >= firstOffset.top &&
//         $window.scrollTop() <
//           lastOffset.top + $('.feature-block:last').outerHeight()
//       ) {
//         // We are between the first and last block
//         featureNav.addClass('is-sticky');
//         featureNav.css('top', '0');
//       } else if ($window.scrollTop() < firstOffset.top) {
//         // We are above the first feature block
//         featureNav.removeClass('is-sticky');
//         featureNav.css('top', '0');
//       } else if (
//         $window.scrollTop() >=
//         lastOffset.top + $('.feature-block:last').outerHeight()
//       ) {
//         // We are under the last feature block
//         featureNav.removeClass('is-sticky');
//         featureNav.css('top', lastOffset.top - firstOffset.top);
//       }

//       // svg colors
//     });
//   }
// };

var parallaxShadow = function() {
  $(window).on('scroll', function() {
    var windowTop = $(window).scrollTop();
    fadePanels(windowTop);
  });

  function fadePanels(windowTop) {
    var panels = $('.feature-block__overlay');

    var $window = $(window);
    var screenHeight = $window.height();
    var screenTop = $window.scrollTop();
    var screenBottom = screenTop + screenHeight;
    var screenHalf = screenTop + screenHeight / 2;

    for (var i = 0; i < panels.length; i++) {
      var panelHeight = $(panels[i]).height();
      var panelTop = $(panels[i]).offset().top;
      var panelBottom = panelTop + panelHeight;

      // Set active class for feature block icons list
      var section = $(panels[i]).parent();
      var $list = section.find('.feature-block__list');
      var listTop = $list.offset().top;

      if (screenBottom >= listTop + 100 && !section.hasClass('is-active')) {
        section.addClass('is-active');
      }

      // Check position for shadow transition
      var j = (screenHalf - panelTop) / (panelHeight / 2);

      if (screenHalf > panelTop && screenTop < panelBottom) {
        // console.log(j);
        $(panels[i])
          .parent()
          .addClass('is-active');
        $(panels[i]).css('opacity', 1 - j);
      } else {
        $(panels[i]).css('opacity', '1');
      }
    }
  }
};

var towerParallax = function() {
  var parallaxRoot = parallax.create($('.features-section').first());

  parallaxRoot.add(document.getElementById('feature-block-tower'), 0.5);
};

$(document).ready(function() {
  // featuresSection();
  parallaxShadow();
  towerParallax();
});
