var dropCookie = function(expiryDays) {
  setCookie('cookieControl', true, expiryDays);
  $('.cookie-banner').removeClass('is-shown');
};

var setCookie = function(name, value, expiryDays) {
  var d = new Date();
  d.setTime(d.getTime() + expiryDays * 24 * 60 * 60 * 1000);
  var expires = 'expires=' + d.toUTCString();
  document.cookie = name + '=' + value + ';' + expires + ';path=/';
  return getCookie(name);
};

var getCookie = function(name) {
  var cookieName = name + '=';
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(';');
  for (var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) === ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(cookieName) === 0) {
      return c.substring(cookieName.length, c.length);
    }
  }
  return false;
};

$(document).ready(function() {
  if (!getCookie('cookieControl')) {
    // Display cookie message on page
    $('.cookie-banner').addClass('is-shown');
    // // When accept button is clicked drop cookie
    $('body').on('click', '.accept-cookie', function() {
      dropCookie(30);
    });
  } else {
    dropCookie(30);
  }
});
