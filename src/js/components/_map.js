/* global google, assetsUrl */

var initMap = function() {
  var mapClass = '.map';
  if ($(mapClass).length) {
    var $map = $(mapClass);
    var address = $map.data('address');
    var createMap = function(latLng) {
      var mapOptions = {
        backgroundColor: '#66727a',
        zoom: 16,
        minZoom: 6,
        maxZoom: 18,
        scrollwheel: false,
        mapTypeControl: false,
        streetViewControl: false,
        panControl: false,
        overviewMapControl: false,
        rotateControl: false,
        scaleControl: false,
        zoomControl: true,
        center: latLng,
      };
      var map = new google.maps.Map($map[0], mapOptions);

      // eslint-disable-next-line no-unused-vars
      var marker = new google.maps.Marker({
        map: map,
        draggable: false,
        position: new google.maps.LatLng(latLng),
        icon: {
          url: assetsUrl + '/img/map-marker.svg',
          scaledSize: new google.maps.Size(45, 63),
        },
      });
    };

    $.getJSON({
      url: 'https://maps.googleapis.com/maps/api/geocode/json',
      data: {
        address: address,
        key: 'AIzaSyCY6ms_oWZkzOv_Cvvx5Z511fZHjSzLFtM',
      },
      success: function(data, textStatus) {
        // console.log('success response');
        // console.log(data.status);
        if (data.status === 'OK') {
          // console.log('status ok');
          var latLng = data.results[0].geometry.location;
          createMap(latLng);
        }
      },
    });
  }
};

$(document).ready(function() {
  initMap();
});
