/* global parallax */

//=require scrollmonitor/scrollMonitor.js
//=require scrollmonitor-parallax/index.js

var accordion = function() {
  var $question = $('.accordion-tab');

  $question.each(function() {
    var $this = $(this);

    $this.on('click', function() {
      // expand this answer
      $this.find('.accordion-tab__answer').slideToggle('fast');
      $this.toggleClass('is-open');

      // hide other answers
      $('.accordion-tab__answer')
        .not($this.find('.accordion-tab__answer'))
        .slideUp('fast');
    });
  });
};

var accordionParallax = function() {
  var parallaxRoot = parallax.create($('.accordion-section').first());

  parallaxRoot.add(document.getElementById('hero__tower--one'), 0.5);
  parallaxRoot.add(document.getElementById('hero__tower--two'), 0.3);
};

$(document).ready(function() {
  accordion();
  accordionParallax();
});
