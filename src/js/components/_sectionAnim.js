/* global scrollMonitor */

//=require scrollmonitor/scrollMonitor.js

var sectionAnim = function() {
  var $section = $('.section-animate');

  $section.each(function() {
    var $this = $(this);
    var offset = $this.outerHeight() / 2;
    var elementWatcher = scrollMonitor.create($this, -offset);

    elementWatcher.enterViewport(function() {
      if (!$this.hasClass('in-view')) {
        $this.addClass('in-view');
        setTimeout(function() {
          $this.addClass('in-view--done');
        }, 800);
        // console.log('in view');
      }
    });
  });
};

$(document).ready(function() {
  sectionAnim();
});
