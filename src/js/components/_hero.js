/* global parallax */

//=require scrollmonitor/scrollMonitor.js
//=require scrollmonitor-parallax/index.js

var heroParallax = function() {
  if ($('.hero').length) {
    var parallaxRoot = parallax.create($('.hero').first());

    // parallaxRoot.add(document.getElementById('hero__tower--one'), 0.5);
    // parallaxRoot.add(document.getElementById('hero__tower--'), 0.3);
    parallaxRoot.add($('.hero__content')[0], {
      start: {
        opacity: 1,
      },
      end: {
        opacity: 0.09,
      },
      easing: {
        opacity: 0,
      },
    });
    parallaxRoot.add(document.getElementById('hero__tower--one'), {
      start: {
        opacity: 0.3,
      },
      end: {
        x: 0,
        y: 150,
        z: 0,
        opacity: 0.09,
      },
      easing: {
        z: 0,
        opacity: 0,
      },
    });
    parallaxRoot.add(document.getElementById('hero__tower--two'), {
      start: {
        opacity: 0.3,
      },
      end: {
        x: 0,
        y: 130,
        z: 0,
        opacity: 0.09,
      },
      easing: {
        y: 0,
        opacity: 0,
      },
    });
  }
};

$(document).ready(function() {
  heroParallax();
});
