var currentScrollPosition = 0;
var lockScreen = function() {
  var isLocked = false;
  var $lockTrigger = $('.lockTrigger');

  $lockTrigger.on('click', function() {
    if (isLocked === false) {
      currentScrollPosition = $(window).scrollTop();
      setTimeout(function() {
        $('body').addClass('lock-screen');
      }, 500);
      isLocked = true;
    } else {
      $('body').removeClass('lock-screen');
      $('html, body').scrollTop(currentScrollPosition);
      isLocked = false;
    }
  });
};

$(document).ready(function() {
  lockScreen();
});
