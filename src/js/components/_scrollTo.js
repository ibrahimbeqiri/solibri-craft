var scrollToElement = function(elem, padding, speed, easing) {
  'use strict';
  elem = typeof elem !== 'undefined' ? elem : 'html, body';
  padding = typeof padding !== 'undefined' ? padding : 0;
  speed = typeof speed !== 'undefined' ? speed : 400;
  easing = typeof easing !== 'undefined' ? easing : 'swing';
  var mainElement = $('html, body');
  var $window = $(window);
  var removeScrollingClass = function() {
    $('html').removeClass('scrolling');
  };
  if ($(elem).length) {
    var elemOffset = $(elem).offset().top - $('body').offset().top;
    mainElement.animate(
      {
        scrollTop: elemOffset - padding + 'px',
      },
      speed,
      easing,
      removeScrollingClass
    );
  } else if (elem === '#top') {
    mainElement.animate(
      {
        scrollTop: 0,
      },
      speed,
      easing,
      removeScrollingClass
    );
  } else if (elem === '#bottom') {
    var wheight = $window.height();
    mainElement.animate(
      {
        scrollTop: wheight,
      },
      speed,
      easing,
      removeScrollingClass
    );
  } else if (elem === '#down') {
    mainElement.animate(
      {
        scrollTop: '' + ($window.scrollTop() + 300),
      },
      speed,
      easing,
      removeScrollingClass
    );
  }
};

var scrollLink = function() {
  'use strict';
  $('.scroll-to').on('click', function(e) {
    e.preventDefault();
    var $this = $(this);
    var target = null;
    var offset = 0;

    if (!$this.data('target')) {
      target = $this.attr('href');
    } else {
      target = $this.data('target');
    }
    if ($this.data('offset')) {
      offset = offset + $this.data('offset');
    }

    $('html').addClass('scrolling');
    scrollToElement(target, offset);

    return false;
  });
};

$(document).ready(function() {
  scrollLink();
});
