/* global Vue, _, getCookie, setCookie, customFileUpload */
//=require vue/dist/vue.min.js
//=require lodash/lodash.js

// Comment these three for local build.
Vue.config.devtools = false;
Vue.config.debug = false;
Vue.config.silent = true;

var query = window.location.search.substring(1);

// console.log(query);

function getQueryParam(param) {
  var params = query.split('&');
  for (var i = 0; i < params.length; i++) {
    var pair = params[i].split('=');
    if (decodeURIComponent(pair[0]) === param) {
      return decodeURIComponent(pair[1]);
    }
  }
  return false;
}

function getDefaultData() {
  return {
    items: [],
    results: [],
    officeSearch: '',
    hqOffice: null,
    isLoading: true,
    reversecompany: false,
    reversecountry: false,
    autocomplete: {
      isOpen: false,
      isTyping: false,
      items: [],
      results: [],
      isLoading: false,
      arrowCounter: 0,
    },
    modalIsOpen: false,
    currentScrollPosition: 0,
    form: {
      error: '',
      form_data: null,
    },
    form_submit: false,
    selectedOffice: {
      title: '',
      email: '',
    },
    error_message: '',
    inputFocused: false,
  };
}

// Define Craft Element API urls
var officeSearchApp;

var officeSearch = function(officeSearchForm) {
  var $officeSearchForm = $(officeSearchForm);

  officeSearchApp = new Vue({
    el: officeSearchForm,
    delimiters: ['<%', '%>'],
    // Here we can register any values or collections that hold data
    data: getDefaultData(),
    beforeCreate: function() {
      // Preparing the form so it will work dynamically work with Vue.
      /* Remove scripts in the Vue template because it will cause compile error. */
      var $scripts = $officeSearchForm.find('script');
      $scripts.each(function() {
        var $script = $(this);
        $script.remove();
      });

      var $officeTitle = $('input[type="hidden"].selectedOffice');
      var $officeEmail = $('input[type="hidden"].selectedOfficeEmail');

      $officeTitle.attr('v-model', 'selectedOffice.title');
      $officeEmail.attr('v-model', 'selectedOffice.email');

      $officeSearchForm
        .find('.submit button')
        .addClass('button is-expanded')
        .html('Submit the form to <% selectedOffice.title %>');

      $officeSearchForm
        .find('.contact-modal form')
        .attr('v-on:submit', 'submitForm');

      /* Put vue method tag to update the local storage on change on the form change */
      // Create an array of unique form fields
      var inputElements = [];
      var $inputs = $('[name*="fields["]'); // $appContainer.find
      _.each($inputs, function(el) {
        if ($(el).parents('.contact-modal').length > 0) {
          var $el = $(el);
          var matches = $el.attr('name').match(/\[(.*?)\]/);

          if (matches) {
            var submatch = matches[1];
            inputElements.push(submatch);
          }
          _.union(inputElements, submatch);
        }
      });

      inputElements = _.uniq(inputElements);

      // Putting vue method to each input field in the form
      _.each(inputElements, function(el) {
        var $el = $('.' + el);
        var type = $el.prop('type');

        switch (type) {
          case 'select-one':
            $el.attr('v-on:change', 'serializeForm()');
            break;
          case 'select-multiple':
            $el.attr('v-on:change', 'serializeForm()');
            break;
          case 'textarea':
            $el.attr('v-on:input', 'serializeForm()');
            break;
          // For inputs under groups.
          case undefined:
            if ($el.find('input[type="checkbox"]').length > 0) {
              var $checkboxes = $el.find('input[type="checkbox"]');
              $checkboxes.attr('v-on:change', 'serializeForm()');
            } else if ($el.find('input[type="radio"]').length > 0) {
              var $radios = $el.find('input[type="radio"]');
              $radios.attr('v-on:change', 'serializeForm()');
            } else {
              $el.attr('v-on:input', 'serializeForm()');
            }
            break;
          // Plain text like things
          default:
            $el.attr('v-on:input', 'serializeForm()');
        }
      });
    },
    created: function() {
      this.fetchData();

      // Set search on page load
      var $setSearch = getQueryParam('search');
      if ($setSearch.length) {
        Vue.set(this, 'officeSearch', $setSearch);
      }

      // Set cookie for officeSearch, and if there is data in storage, fetch it to the form
      this.setCookie();
      if (localStorage.officeSearch !== undefined) {
        var dataFromStorage = localStorage.getItem('officeSearch');

        var data = JSON.parse(dataFromStorage);
        if (data !== null) {
          this.form = data.form;
        }
      }
    },
    updated: function() {
      var $fileInput = $('input[type=file]');
      if (
        $fileInput.parents('#officeSearchApp').length > 0 &&
        $fileInput.parents('.file-upload-wrapper').length === 0
      ) {
        $fileInput.customFile();
      }
      if (this.modalIsOpen) {
        this.fetchFromStorage();
      }
    },
    // Anything within the ready function will run when the application loads
    ready: function() {},
    watch: {
      officeSearch: function(val, oldVal) {
        this.setHistory();
        this.updateResults();

        this.autocompleteFilterResults();
        this.autocomplete.isOpen = true;
      },
    },
    methods: {
      // Use local storage data if cookie exists, otherwise reset the local storage.
      setCookie: function() {
        // Set cookie and reset the local storage on created if it doesn't exist.
        if (!getCookie('officeSearch')) {
          this.reset();
          setCookie('officeSearch', true, 1);
        }
      },
      reset: function() {
        var def = getDefaultData();
        _.assign(this.$data, def);
        localStorage.setItem('officeSearch', JSON.stringify(def));
      },
      fetchData: function() {
        var self = this;
        var filteredItems = [];

        self.isLoading = true;

        $.ajax({
          url: '/api/offices',
          type: 'get',
          dataType: 'json',
          async: true,
          success: function(data) {
            _.each(data.data, function(itm) {
              filteredItems.push(itm);
            });
            Vue.set(self, 'items', filteredItems);
            self.updateResults();
            self.hqOffice = _.find(filteredItems, {'title': 'Solibri Inc'});
            self.isLoading = false;
            document
              .getElementById('officeSearchApp')
              .setAttribute('data-loading', 'false');
          },
        });

        var autocompleteItems = [];

        self.autocomplete.isLoading = true;

        $.ajax({
          url: '/api/offices/tags',
          type: 'get',
          dataType: 'json',
          async: true,
          success: function(data) {
            _.each(data.data, function(itm) {
              autocompleteItems.push(itm.title);
            });

            Vue.set(self.autocomplete, 'items', autocompleteItems);

            self.autocomplete.isLoading = false;
          },
        });
      },
      setHistory: function() {
        var self = this;
        var params = '?';

        if (self.officeSearch.length) {
          params += 'search=' + self.officeSearch;
        }

        if (window.history && window.history.replaceState) {
          history.replaceState(null, null, params);

          return params;
        }
      },
      // This method update the locale storage with the data
      updateStorage: function() {
        var dataJson = JSON.stringify(this.form.form_data);
        localStorage.setItem(
          'officeSearch-' + this.selectedOffice.title,
          dataJson
        );
      },
      fetchFromStorage: function() {
        /* Get values from the local storage if exists */
        // Getting data from the local storage
        var dataFromStorage = localStorage.getItem(
          'officeSearch-' + this.selectedOffice.title
        );

        var data = JSON.parse(dataFromStorage);
        var formObject = !_.isNull(data) ? data : null;

        // Filling in the form from the data from the local storage
        if (!_.isEmpty(formObject)) {
          $.each(formObject, function(key, value) {
            var $ele = $('[name="' + value.name + '"]');
            if (!_.isUndefined($ele)) {
              _.each($ele, function(el) {
                var $el = $(el);
                if ($el.parents('.contact-modal').length > 0) {
                  $el.attr('ref', value.name);
                  var type = $el.prop('type');

                  switch (type) {
                    case 'checkbox':
                      if ($el.val() === value.value) {
                        $el.attr('checked', 'checked');
                      }
                      break;
                    case 'radio':
                      $el
                        .filter('[value="' + value.value + '"]')
                        .attr('checked', 'checked');
                      break;
                    case 'select-one':
                      var $option = $el.find(
                        'option[value="' + value.value + '"]'
                      );
                      $option.attr('selected', 'selected');
                      if (!$el.hasClass('country-code')) {
                        $el.siblings('.holder').text($option.html());
                      }
                      break;
                    case 'select-multiple':
                      var $options = $el.find(
                        'option[value="' + value.value + '"]'
                      );
                      $options.attr('selected', 'selected');
                      break;
                    case 'textarea':
                      $el.attr('value', value.value);
                      $el.text(value.value);
                      break;
                    // Plain text like things
                    default:
                      var notPassing =
                        value.name === 'fields[selectedOfficeEmail]' ||
                        value.name === 'fields[selectedOffice]';

                      if (notPassing !== true) {
                        $el.attr('value', value.value);
                        $el.val(value.value);
                      }
                  }
                }
              });
            }
          });
        }
      },
      serializeForm: function() {
        var vueThis = this;

        var form = $('.contact-modal').find('form');
        var formData = $(form).serializeArray();

        vueThis.form.form_data = formData;
        vueThis.updateStorage();
      },
      fileUpload: function() {
        customFileUpload();
      },
      $last: function(item, list) {
        return item === list[list.length - 1];
      },
      onResultsPageChange: function(toPage, fromPage) {
        var self = this;
        self.setHistory();
      },
      updateResults: function() {
        var self = this;
        var filtereditems = [];
        var updatedResults = self.items;

        // Filter by search query
        if (self.officeSearch.length) {
          _.each(updatedResults, function(itm) {
            var searchElements =
              itm.title +
              ' ' +
              itm.country +
              ' ' +
              itm.streetAddress +
              ' ' +
              itm.tags;

            if (
              searchElements.search(new RegExp(self.officeSearch, 'i')) > -1
            ) {
              filtereditems.push(itm);
            }
          });
          updatedResults = filtereditems;
        }

        // set results as global
        Vue.set(self, 'results', updatedResults);
      },
      sortBy: function(name) {
        var self = this;
        var updatedResults = self.items;

        updatedResults = _.sortBy(updatedResults, function(itm) {
          if (name === 'country') {
            return itm.country;
          } else if (name === 'company') {
            return itm.title;
          }
        });

        var isReversed = 'reverse' + name;

        if (this[isReversed] === false) {
          this[isReversed] = true;

          updatedResults = _.reverse(updatedResults, function(itm) {
            if (name === 'country') {
              return itm.country;
            } else if (name === 'company') {
              return itm.title;
            }
          });
        } else {
          this[isReversed] = false;
        }

        Vue.set(self, 'results', updatedResults);
      },
      autocompleteFilterResults: function() {
        var self = this;

        this.autocomplete.results = this.autocomplete.items.filter(function(
          item
        ) {
          return (
            item.toLowerCase().indexOf(self.officeSearch.toLowerCase()) > -1
          );
        });
      },
      autocompleteSetResult: function(result) {
        this.officeSearch = result;
        this.autocomplete.isOpen = false;
      },
      autocompleteOnArrowDown: function(evt) {
        if (this.autocomplete.arrowCounter < this.autocomplete.results.length) {
          this.autocomplete.arrowCounter = this.autocomplete.arrowCounter + 1;
        }
      },
      autocompleteOnArrowUp: function() {
        if (this.autocomplete.arrowCounter > 0) {
          this.autocomplete.arrowCounter = this.autocomplete.arrowCounter - 1;
        }
      },
      autocompleteOnEnter: function() {
        var counter = this.autocomplete.arrowCounter;
        if (counter > -1) {
          this.officeSearch = this.autocomplete.results[counter];

          this.autocomplete.isOpen = false;
          this.autocomplete.arrowCounter = -1;
        }
      },
      autocompleteHandleClickOutside: function(evt) {
        if (!this.$refs.form.contains(evt.target)) {
          this.autocomplete.isOpen = false;
          this.autocomplete.arrowCounter = -1;
        }
      },
      preventDefault: function(event) {
        event.preventDefault();
      },
      openModal: function(officeTitle, supportEmail) {
        this.modalIsOpen = true;
        this.selectedOffice.title = officeTitle;
        this.selectedOffice.email = supportEmail;
        this.currentScrollPosition = $(window).scrollTop();
        // console.log(this.selectedOffice);
        // console.log(this.currentScrollPosition);
        // console.log(-this.currentScrollPosition);
        $('body').addClass('lock-screen');
        $('body').css('top', -this.currentScrollPosition);
        $('body').addClass('modal-is-active');
      },
      closeModal: function(event) {
        var closing = $(event.target).hasClass('closeModal');
        if (closing) {
          $('body').removeClass('modal-is-active');
          this.modalIsOpen = false;
          this.selectedOffice.name = '';
          this.selectedOffice.email = '';
          this.form.form_check = null;
          $('body').removeClass('lock-screen');
          $('body').css('top', 'auto');
          $('html, body').scrollTop(this.currentScrollPosition);
        }
      },
      submitForm: function() {
        this.form_submit = true;
        this.form.form_data = null;
        this.updateStorage();
        this.selectedOffice.name = '';
        this.selectedOffice.email = '';
      },
    },
    filters: {
      googleMaps: function(address) {
        return (
          '//www.google.com/maps/dir/Current+Location/' +
          encodeURIComponent(address.trim().replace(' ', '+'))
        );
      },
      websiteAddress: function(url) {
        var pattern = /^((http|https|ftp):\/\/)/;

        if (!pattern.test(url)) {
          url = '//' + url;
        }
        return url;
      },
    },
    mounted: function mounted() {
      document.addEventListener('click', this.autocompleteHandleClickOutside);
    },
    destroyed: function destroyed() {
      document.removeEventListener(
        'click',
        this.autocompleteHandleClickOutside
      );
    },
  });
  officeSearchApp.$mount($officeSearchForm);
};

$(document).ready(function() {
  if ($('#officeSearchApp').length > 0) {
    officeSearch('#officeSearchApp');
  }
  $('.officeSearchApp__form').on('submit', function(e) {
    e.preventDefault();

    return false;
  });
});
