/* global Vue, _, */
//=require vue/dist/vue.min.js
//=require lodash/lodash.js
//=require headroom.js/dist/headroom.min.js
//=require headroom.js/dist/jQuery.headroom.js

// Comment these three for local build.
Vue.config.devtools = false;
Vue.config.debug = false;
Vue.config.silent = true;

var headerNavLine = function() {
  var setLine = function($activeNav) {
    var activeNavWidth = $activeNav.width();
    var activeNavOffset =
      $activeNav.position().left +
      parseFloat($activeNav.css('padding-left'), 10);

    $('.primary-navigation .underline').css({
      width: activeNavWidth + 'px',
      left: activeNavOffset + 'px',
      height: '2px',
    });
  };

  if ($('.primary-navigation a.is-active').length) {
    setLine($('.primary-navigation a.is-active').first());
  }

  $(document).on('resized', function() {
    if ($('.primary-navigation a.is-active').length) {
      setLine($('.primary-navigation a.is-active').first());
    }
  });

  $('.primary-navigation a').on('mouseenter', function() {
    setLine($(this));
  });

  $('.primary-navigation ul').on('mouseleave', function() {
    if ($('.primary-navigation a.is-active').length) {
      setLine($('.primary-navigation a.is-active').first());
    } else {
      $('.primary-navigation .underline').css({
        height: '0px',
      });
    }
  });
};

var toggleSearch = function() {
  var $header = $('.site-header');
  var openClass = 'search-is-open';
  var closeSearch = function() {
    $header.removeClass(openClass);
    $('.site-search__input').blur();
  };
  var openSearch = function() {
    $header.addClass(openClass);
    $('.site-search__input').focus();
  };

  // Search open button
  $('.site-search-toggle__button').on('click', function(e) {
    e.preventDefault();

    if ($header.hasClass(openClass)) {
      closeSearch();
    } else {
      openSearch();
    }

    return false;
  });

  // Search close button
  $('.site-search__close').on('click', function() {
    closeSearch();
  });

  // if navigation is closed
  $('#site-navigation-status').on('change', function() {
    if ($(this).prop('checked') === false && $header.hasClass(openClass)) {
      closeSearch();
    }
  });

  $(document).on('search:open', openSearch);
  $(document).on('search:close', closeSearch);
};

// Site search application
var siteSearchApp;

var typingTimer;
var doneTypingTimeout = 200;

var siteSearch = function(siteSearchForm) {
  var $siteSearchForm = $(siteSearchForm);

  siteSearchApp = new Vue({
    el: siteSearchForm,
    delimiters: ['<%', '%>'],
    // Here we can register any values or collections that hold data
    data: {
      siteSearch: '',
      autocomplete: {
        isOpen: false,
        isTyping: false,
        results: [],
        isLoading: false,
        arrowCounter: 0,
      },
    },
    // Anything within the ready function will run when the application loads
    ready: function() {},
    watch: {
      siteSearch: function(val, oldVal) {
        var self = this;
        // Only do a search after stopped typing
        clearTimeout(typingTimer);

        setTimeout(function() {
          self.autocompleteFilterResults();
          self.autocomplete.isOpen = true;
        }, doneTypingTimeout);
      },
    },
    methods: {
      autocompleteFilterResults: function() {
        var self = this;
        var autocompleteItems = [];

        self.autocomplete.isLoading = true;

        if (self.siteSearch.length >= 3) {
          $.ajax({
            url: '/api/search',
            type: 'get',
            dataType: 'json',
            async: true,
            data: { q: self.siteSearch },
            success: function(data) {
              _.each(data.data, function(itm) {
                autocompleteItems.push(itm);
              });

              Vue.set(self.autocomplete, 'results', autocompleteItems);

              self.autocomplete.isLoading = false;
            },
          });
        }
      },
      autocompleteSetResult: function(result) {
        this.officeSearch = result;
        this.autocomplete.isOpen = false;
      },
      autocompleteOnArrowDown: function(evt) {
        if (this.autocomplete.arrowCounter < this.autocomplete.results.length) {
          this.autocomplete.arrowCounter = this.autocomplete.arrowCounter + 1;
        }
      },
      autocompleteOnArrowUp: function() {
        if (this.autocomplete.arrowCounter > 0) {
          this.autocomplete.arrowCounter = this.autocomplete.arrowCounter - 1;
        }
      },
      autocompleteOnEnter: function() {
        var counter = this.autocomplete.arrowCounter;
        var selected = this.autocomplete.results[counter];

        window.location.href = selected.url;

        this.autocomplete.isOpen = false;
        this.autocomplete.arrowCounter = -1;
      },
      closeSearch: function() {
        $(document).trigger('search:close');
      },
      openSearch: function() {
        $(document).trigger('search:open');
      },
    },
  });
  siteSearchApp.$mount($siteSearchForm);
};

$(document).ready(function() {
  headerNavLine();
  toggleSearch();
  if ($('#siteSearch').length > 0) {
    siteSearch('#siteSearch');
  }
});
