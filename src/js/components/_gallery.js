/* global PhotoSwipe, PhotoSwipeUI_Default */

//=require photoswipe/dist/photoswipe.js
//=require photoswipe/dist/photoswipe-ui-default.js

var gallery = function() {
  var elementSelector = '.gallery';
  var slideSelector = '[data-img]';
  var sliderHtml =
    '<div class="pswp" tabindex="-1" role="dialog" aria-hidden="true"><div class="pswp__bg"></div><div class="pswp__scroll-wrap"><div class="pswp__container"><div class="pswp__item"></div><div class="pswp__item"></div><div class="pswp__item"></div></div><div class="pswp__ui pswp__ui--hidden"><div class="pswp__top-bar"><div class="pswp__counter"></div><button class="pswp__button pswp__button--close" title="Close (Esc)"></button><div class="pswp__preloader"><div class="pswp__preloader__icn"><div class="pswp__preloader__cut"><div class="pswp__preloader__donut"></div></div></div></div></div><div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap"><div class="pswp__share-tooltip"></div></div><button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)"></button><button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)"></button><div class="pswp__caption"><div class="pswp__caption__center"></div></div></div></div></div>';

  var parseThumbnailElements = function($slides) {
    var items = [];

    $.each($slides, function(index, value) {
      var $this = $(this);
      var size = $this.attr('data-size').split('x');
      var item = {
        src: $this.attr('data-img'),
        w: parseInt(size[0], 10),
        h: parseInt(size[1], 10),
        title: $this.attr('data-desc'),
        msrc: $this.attr('data-thumb'),
        el: $this,
      };

      items.push(item);
    });

    return items;
  };
  var openGallery = function($slides, slideIndex) {
    var galleryElem = $('body').find('.pswp')[0];
    var items = parseThumbnailElements($slides);

    var options = {
      index: slideIndex,
      getThumbBoundsFn: function(index) {
        var thumbnail = items[index].el[0];
        var pageYScroll =
          window.pageYOffset || document.documentElement.scrollTop;
        var rect = thumbnail.getBoundingClientRect();

        return { x: rect.left, y: rect.top + pageYScroll, w: rect.width };
      },
      showHideOpacity: true,
      showAnimationDuration: 333,
      hideAnimationDuration: 333,
      bgOpacity: 0.9,
      spacing: 0.12,
      allowPanToNext: true,
      maxSpreadZoom: 2,
      loop: true,
      pinchToClose: true,
      closeOnScroll: false,
      closeOnVerticalDrag: true,
      mouseUsed: false,
      escKey: true,
      arrowKeys: true,
      history: false,
      errorMsg:
        '<div class="pswp__error-msg"><a href="%url%" target="_blank">The image</a> could not be loaded.</div>',
      focus: true,
      modal: true,
    };

    var gallery = new PhotoSwipe(
      galleryElem,
      PhotoSwipeUI_Default,
      items,
      options
    );

    gallery.init();
  };

  if ($(elementSelector).length) {
    $('body').append(sliderHtml);
    $(elementSelector)
      .find(slideSelector)
      .on('click', function(e) {
        e.preventDefault();

        var $thisSlide = $(this);
        var $galleryContainer = $thisSlide.parents(elementSelector);
        var $slides = $galleryContainer.find(slideSelector);
        var slideIndex = $slides.index($thisSlide);

        openGallery($slides, slideIndex);

        return false;
      });

    $(elementSelector)
      .find('.show-gallery')
      .on('click', function(e) {
        e.preventDefault();
        var $slides = $(this)
          .parents(elementSelector)
          .find(slideSelector);

        var slideIndex = 0;

        openGallery($slides, slideIndex);

        return false;
      });
  }
};

$(document).ready(function() {
  gallery();
});
