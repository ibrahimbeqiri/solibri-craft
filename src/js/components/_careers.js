// var toggleCareers = function() {
//   $(document).on('click', '.careers-navigation a', function(e) {
//     e.preventDefault();
//     var $this = $(this);
//     var activeClass = 'active';
//     var targetState = $this.attr('data-state');
//     var $container = $this.parents('.careers-section');
//     var $target = $container.find(
//       '.careers-tab[data-state="' + targetState + '"]'
//     );
//     var $slides = $container.find('.careers-tab');
//     var $navElements = $container.find('.careers-navigation a');

//     // Nav activestate
//     $navElements.removeClass(activeClass);
//     $this.addClass(activeClass);

//     $slides.removeClass(activeClass);
//     $target.addClass(activeClass);

//     return false;
//   });
// };

// var careersNavLine = function() {
//   var setLine = function($activeNav) {
//     var activeNavWidth = $activeNav.width();
//     var activeNavOffset = $activeNav.position().left;

//     $('.careers-navigation .underline').css({
//       width: activeNavWidth + 'px',
//       left: activeNavOffset + 'px',
//       height: '4px',
//     });
//   };

//   if ($('.careers-navigation a.active').length) {
//     setLine($('.careers-navigation a.is-active').first());
//   }

//   $('.careers-navigation  a').on('mouseenter', function() {
//     setLine($(this));
//   });

//   $('.careers-navigation  ul').on('mouseleave', function() {
//     if ($('.careers-navigation a.active').length) {
//       setLine($('.careers-navigation  a.active').first());
//     } else {
//       $('.careers-navigation  .underline').css({ height: '0px' });
//     }
//   });
// };

var careersScrollTo = function() {
  // Check if we're on the careers page
  if ($('.careers-template').length) {
    // Check if the careers section exists
    if ($('.careers-section').length) {
      $('.careers-section').attr('id', 'careers-section');
      // If the careers section exists check if the hero as a link and give it the correct id as href
      if ($('.hero__cta a').length) {
        $('.hero__cta a').attr('href', '#careers-section');
      }
    }
  }
};

$(document).ready(function() {
  // toggleCareers();
  // careersNavLine();
  careersScrollTo();
});
