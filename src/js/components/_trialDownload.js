/* global Vue, _, moment, styledSelect, styledCheckboxAndRadio, getCookie, setCookie, siteUrl, currentLocale, currentLang */
//=require vue/dist/vue.min.js
//=require lodash/lodash.js
//=require moment/moment.js

// Comment these three for local build.
// Vue.config.devtools = false;
// Vue.config.debug = false;
// Vue.config.silent = true;

var trialDownloadApp;

var translation = {
  en: {
    'This field is required': 'This field is required',
    'Check the email format': 'Check the email format',
    'Cannot connect to the module. Please refresh the page or try it later.':
      'Cannot connect to the module. Please refresh the page or try it later.',
    'Text message sent to': 'Text message sent to',
    'An error occurred please try again later':
      'An error occurred please try again later',
    'This field cannot be empty': 'This field cannot be empty',
    'Verification code is incorrect': 'Verification code is incorrect',
    'Choose an option': 'Choose an option',
    'Country code': 'Country code',
  },
  de: {
    'This field is required': 'Pflichtfeld',
    'Check the email format': 'Prüfen Sie die E-Mail',
    'Cannot connect to the module. Please refresh the page or try it later.':
      'Keine Verbindung möglich. Bitte aktualisieren Sie die Seite oder versuchen Sie es später erneut.',
    'Text message sent to': 'Textnachricht wird gesendet an',
    'An error occurred please try again later':
      'Es ist ein Fehler aufgetreten. Bitte versuchen Sie es später noch einmal',
    'This field cannot be empty': 'Dieses Feld muss ausgefüllt sein',
    'Verification code is incorrect': 'Der Verifizierungscode ist fehlerhaft',
    'Choose an option': 'Wählen Sie eine Option',
    'Country code': 'Landesvorwahl',
  },
  ja: {
    'This field is required': '必須入力項目です',
    'Check the email format': '有効なメール アドレスを入力してください',
    'Cannot connect to the module. Please refresh the page or try it later.':
      'モジュールに接続できません。ページを再読み込みするか、後ほどお試しください。',
    'Text message sent to': 'にテキストメッセージが送信されました',
    'An error occurred please try again later':
      'エラーが発生しました、後でやり直してください',
    'This field cannot be empty': 'このフィールドは必須です',
    'Verification code is incorrect': '確認コードが間違っています',
    'Choose an option': 'オプションの選択',
    'Country code': '国番号',
  },
};

var query = decodeURIComponent(window.location.search.substring(1));
var referer =
  query.indexOf('referer') > -1
    ? query
        .split('referer')[1]
        .substr(1)
        .slice(0, -1)
    : undefined;

function getQueryParam(param) {
  var params = query.split('&');
  for (var i = 0; i < params.length; i++) {
    var pair = params[i].split('=');
    if (decodeURIComponent(pair[0]) === param) {
      return decodeURIComponent(pair[1]);
    }
  }
  return false;
}

var trialDownload = function(containerId) {
  /** This is a function for trial download page
   */
  var value;
  var name;
  var code;
  var checkError;

  var countryValueInit = function() {
    if (_.isUndefined(code)) {
      var select = $('#trialDownload').find('select[name*=country]')[0];

      value = $(select).val();
      name = $(select)
        .find('option:selected')
        .text();
      code = '+' + name.split('+').pop(-1);
    }
  };

  function getDefaultData() {
    countryValueInit();
    return {
      step: 1,
      selectedCountry: '',
      contact: {
        country: {
          label: name, // Finland +358
          value: value, // FI
          code: code, // +358
        },
        phone_number: '',
      },
      form: {
        is_saved: false,
        error: '',
        message: '',
        form_data: null,
        contry: '',
      },
      error_message: '',
      additional_message: '',
      direction: 'slide-next',
      statusCode: '',
      is_loading: false,
      is_timeout: false,
      showing_lang: 'en',
      translation: translation['en'],
      windowWidth: window.innerWidth,
      headerHeight: 0,
    };
  }

  trialDownloadApp = new Vue({
    data: getDefaultData(),
    delimiters: ['<%', '%>'],
    beforeCreate: function() {
      var $appContainer = $(containerId);

      // Preparing the form so it will work dynamically work with Vue.
      /* Remove scripts in the Vue template because it will cause compile error. */
      var $scripts = $appContainer.find('script');
      $scripts.each(function() {
        var $script = $(this);
        $script.remove();
      });
      /* Remove submit button to prevent the form submission */
      var $formSubmitButton = $appContainer.find('button[type="submit"]');
      $formSubmitButton.remove();

      /* Put vue method tag to update the local storage on change on the form change */
      // Create an array of unique form fields
      var inputElements = [];
      var $inputs = $('[name*="fields["]'); // $appContainer.find
      _.each($inputs, function(el) {
        var $el = $(el);
        var matches = $el.attr('name').match(/\[(.*?)\]/);

        if (matches) {
          var submatch = matches[1];
          inputElements.push(submatch);
        }
        _.union(inputElements, submatch);
      });

      inputElements = _.uniq(inputElements);

      // Putting vue method to each input field in the form
      _.each(inputElements, function(el) {
        var $el = $('.' + el);
        var type = $el.prop('type');

        switch (type) {
          case 'select-one':
            $el.attr('v-on:change', 'serializeForm()');
            break;
          case 'select-multiple':
            $el.attr('v-on:change', 'serializeForm()');
            break;
          case 'textarea':
            $el.attr('v-on:input', 'serializeForm()');
            break;
          // For inputs under groups.
          case undefined:
            if ($el.find('input[type="checkbox"]').length > 0) {
              var $checkboxes = $el.find('input[type="checkbox"]');
              $checkboxes.attr('v-on:change', 'serializeForm()');
            } else if ($el.find('input[type="radio"]').length > 0) {
              var $radios = $el.find('input[type="radio"]');
              $radios.attr('v-on:change', 'serializeForm()');
            } else {
              $el.attr('v-on:input', 'serializeForm()');
            }
            break;
          // Plain text like things
          default:
            $el.attr('v-on:input', 'serializeForm()');
        }
      });

      /* Get phone number field from the form since it needs to be reused to step2, contact modal and interactive in step 2 and step 3 */
      var phoneFieldWrap = $('input[name*=phone]').parents('div.field')[0];
      var $select = $(phoneFieldWrap).find('select');
      var $phoneInput = $(phoneFieldWrap).find('input');

      $(phoneFieldWrap).addClass('phone-wrap');

      $select.addClass('country-code');
      $select.attr('v-model', 'contact.country.value');
      $select.attr('v-on:change', 'selectOnChange($event); serializeForm();');

      $select.addClass('styled');
      $select.wrap("<div class='select-wrapper'></div>");
      $select.after(
        "<span class='holder'><span class='label'><% contact.country.label %></span><span class='code'><% contact.country.code %></span></span>"
      );

      $phoneInput
        .attr('v-model', 'contact.phone_number')
        .attr('v-on:input', 'trimValue($event)')
        .wrapAll('<div class="input-wrapper"></div>');

      var $step2Heading = $('.step-2 .phone-wrap').find('.heading');
      var $step2Input = $('.step-2 .phone-wrap').find('input');
      $($step2Heading)
        .find('label')
        .clone()
        .appendTo($step2Heading);

      var countryCodeTranslation =
        translation[$('.showingLang').data('lang')]['Country code'];
        $($step2Heading)
        .find('label')
        .first()
        .html(countryCodeTranslation);

      $step2Input.before(
        '<div class="countrycode-text"><% contact.country.code %></div>'
      );

      // Add approveAll function to approve all checkbox
      var $approveAllWrap = $('.approveAll-field');
      var $approveAllTab = $approveAllWrap.parents('.tab');
      var $approveAll = $approveAllWrap.find('input[type="checkbox"]');

      $approveAll.attr('v-on:click', 'approve_all');
      $approveAllTab.find('.field').wrapAll('<div class="wrapped"></div>');

      /* Get values from the local storage if exists */
      // Getting data from the local storage
      var dataFromStorage = localStorage.getItem('trialDownload');

      var data = JSON.parse(dataFromStorage);
      var formObject =
        !_.isNull(data) && !_.isNull(data.form.form_data)
          ? data.form.form_data
          : null;

      // Filling in the form from the data from the local storage
      if (!_.isEmpty(formObject)) {
        $.each(formObject, function(key, value) {
          var $ele = $('[name="' + value.name + '"]');
          if (!_.isUndefined($ele)) {
            _.each($ele, function(el) {
              var $el = $(el);
              $el.attr('ref', value.name);
              var type = $el.prop('type');

              switch (type) {
                case 'checkbox':
                  if ($el.val() === value.value) {
                    $el.attr('checked', 'checked');
                  }
                  break;
                case 'radio':
                  $el
                    .filter('[value="' + value.value + '"]')
                    .attr('checked', 'checked');
                  break;
                case 'select-one':
                  var $option = $el.find('option[value="' + value.value + '"]');
                  $option.attr('selected', 'selected');
                  if (!$el.hasClass('country-code')) {
                    $el.siblings('.holder').text($option.html());
                  }
                  break;
                case 'select-multiple':
                  var $options = $el.find(
                    'option[value="' + value.value + '"]'
                  );
                  $options.attr('selected', 'selected');
                  break;
                case 'textarea':
                  $el.attr('value', value.value);
                  $el.text(value.value);
                  break;
                // Plain text like things
                default:
                  $el.attr('value', value.value);
                  $el.val(value.value);
              }
            });
          }
        });
      }

      var refererFromStorage = $('#fields-referrer').val();
      var referrer =
        getQueryParam('referer') !== false
          ? getQueryParam('referer')
          : getQueryParam('referrer') !== false
            ? getQueryParam('referrer')
            : !_.isUndefined(referer) && referer.length > 0
              ? referer
              : !_.isUndefined(refererFromStorage) &&
                refererFromStorage.length > 0
                ? refererFromStorage
                : '';
      $('#fields-referrer').val(referrer);
    },
    created: function() {
      this.setCookie();
      if (localStorage.trialDownload !== undefined) {
        var dataFromStorage = localStorage.getItem('trialDownload');

        var data = JSON.parse(dataFromStorage);
        if (data !== null) {
          this.step = data.step;
          this.contact = data.contact;
          this.form = data.form;
        }
      }
      this.showing_lang = $('.showingLang').data('lang');
      this.translation = translation[this.showing_lang];

      window.addEventListener('popstate', this.popstateListener);
      this.updateHistory(this.step, true);
    },
    mounted: function() {
      window.addEventListener('resize', this.handleWindowResize);
      this.methodForMountedAndupdated();
      this.getHeaderHeighgt();
      this.scrollup();
    },
    beforeDestroy: function() {
      window.removeEventListener('resize', this.handleWindowResize);
    },
    updated: function() {
      this.methodForMountedAndupdated();
    },
    destroyed: function() {
      window.removeEventListener('popstate', this.popstateListener);
    },
    computed: {
      video_next_step: function() {
        return this.video.is_finnished ? '' : 'is-disabled';
      },
      which_step: function() {
        return 'step-' + this.step;
      },
      current_step: function() {
        return 'step-' + this.step;
      },
      translated_error_message: function() {
        return !_.isUndefined(this.translation[this.error_message])
          ? this.translation[this.error_message]
          : this.error_message;
      },
    },
    filters: {
      format_seconds: function(sec) {
        return moment.utc(sec * 1000).format('HH:mm:ss');
      },
    },
    watch: {
      phone_number: function(val) {
        this.contact.phone_number = val;
      }
    },
    methods: {
      handleWindowResize: function(event) {
        // this.windowWidth = event.currentTarget.innerWidth;
        this.getHeaderHeighgt();
      },
      getHeaderHeighgt: function() {
        this.windowWidth = $(window).width();
        this.headerHeight = this.windowWidth < 769 ? 68 : 76;
      },
      // Use local storage data if cookie exists, otherwise reset the local storage.
      setCookie: function() {
        // Set cookie and reset the local storage on created if it doesn't exist.
        if (!getCookie('trialDownload')) {
          this.reset();
          setCookie('trialDownload', true, 1);
        }
      },
      popstateListener: function(evt) {
        if (!_.isUndefined(this.form) && this.form.is_saved !== true) {
          this.step = evt.state.trialDownload;
          this.updateStorage();
        }
      },
      methodForMountedAndupdated: function() {
        var vueThis = this;

        // Revalidate images in trial download
        // Set time out to make sure containers for images are loaded
        setTimeout(function() {
          $(document).trigger('images:revalidate');
          styledSelect();
          styledCheckboxAndRadio();
        }, 400);

        // Check if user is stuck in the loading page
        if (this.is_loading === true) {
          checkError = setTimeout(function() {
            vueThis.is_timeout = true;
          }, 30000);
        } else {
          // Call back for timeout text if loading went through after a long time
          clearTimeout(checkError);
          this.is_timeout = false;
        }

        // Load videos in the video section
        // This is step 2 and video src exists. In the page with video player.
        $('#trialDownload').addClass('is-ready');

        // Auto Focus on forms if the first field is empty
        if (
          $('.register-form #fields-firstname').length &&
          (!$('.register-form #fields-phone').is(':focus') &&
            !$('.register-form #fields-phone-country').is(':focus') &&
            !$('.register-form #fields-firstname').val().length > 0)
        ) {
          $('.step-2 input[type="text"]')
            .first()
            .focus();
        }

        this.preventDefault();
        this.stepNavigation();
      },
      trimValue: function(event) {
        var value = event.target.value;
        this.contact.phone_number = value.replace(/\s+/g, '');
      },
      updateForm: function() {
        /* Get values from the local storage if exists */
        // Getting data from the local storage
        var dataFromStorage = localStorage.getItem('trialDownload');

        var data = JSON.parse(dataFromStorage);
        var formObject =
          !_.isNull(data) && !_.isNull(data.form.form_data)
            ? data.form.form_data
            : null;

        // Filling in the form from the data from the local storage
        if (!_.isEmpty(formObject)) {
          $.each(formObject, function(key, value) {
            var $ele = $('[name="' + value.name + '"]');
            if (!_.isUndefined($ele)) {
              _.each($ele, function(el) {
                var $el = $(el);
                var type = $el.prop('type');

                switch (type) {
                  case 'checkbox':
                    if ($el.val() === value.value) {
                      $el.attr('checked', 'checked');
                    }
                    break;
                  case 'radio':
                    $el
                      .filter('[value="' + value.value + '"]')
                      .attr('checked', 'checked');
                    break;
                  case 'select-one':
                    var $option = $el.find(
                      'option[value="' + value.value + '"]'
                    );
                    $option.attr('selected', 'selected');
                    if (!$el.hasClass('country-code')) {
                      $el.siblings('.holder').text($option.html());
                    }
                    break;
                  case 'select-multiple':
                    var $options = $el.find(
                      'option[value="' + value.value + '"]'
                    );
                    $options.attr('selected', 'selected');
                    break;
                  case 'textarea':
                    $el.attr('value', value.value);
                    $el.text(value.value);
                    break;
                  // Plain text like things
                  default:
                    $el.attr('value', value.value);
                    $el.val(value.value);
                }
              });
            }
          });
        }
      },
      // This method update the locale storage with the data
      updateStorage: function() {
        var dataJson = JSON.stringify(this._self._data);
        localStorage.setItem('trialDownload', dataJson);
      },
      updateHistory: function(step, replaceState) {
        var lang = currentLang === 'en' ? '/' : '/' + currentLang + '/';
        if (_.isUndefined(step)) {
          step = this.step;
        }
        var stateObj = { trialDownload: step };
        if (replaceState === true) {
          history.replaceState(
            stateObj,
            currentLocale + ' - TrialDownload - ' + step,
            lang + 'download-solibri-trial/step=' + step
          );
        } else {
          history.pushState(
            stateObj,
            currentLocale + ' - TrialDownload - ' + step,
            lang + 'download-solibri-trial/step=' + step
          );
        }
      },
      preventDefault: function() {
        var $links = $('.content .trial-download-list__item a');

        $links.on('click', function(event) {
          event.preventDefault();
        });
      },
      stepNavigation: function() {
        var $steps = $('*[data-step]');
        var currentStep = this.step;
        var $selectedStep = currentStep === 3 ? $('*[data-step="thankyou"]') : $('*[data-step="' + currentStep + '"]');
        if (currentStep < 3) {
          $selectedStep.parents('ul').removeClass('thank-you');
          $steps.removeClass('is-active has-passed');
          _.each($steps, function(step) {
            var $step = $(step);
            var stepData = $step.data('step');
            if (stepData < currentStep) {
              $step.addClass('has-passed');
            }
          });
          if ($selectedStep.length > 0) {
            $selectedStep.addClass('is-active');
          } else {
            $('*[data-step="3"]').addClass('is-active');
          }
        } else {
          $selectedStep.parents('ul').addClass('thank-you');
          $selectedStep.addClass('is-active');
        }
      },
      approve_all: function() {
        var $approveAllWrap = $('.approveAll-field');
        var $approveAllTab = $approveAllWrap.parents('.tab');
        var $approveAll = $approveAllWrap.find('input[type="checkbox"]');
        var $checkboxes = $approveAllTab.find('.field.checkboxes input[type="checkbox"]');

        if($approveAll.is(':checked')) {
          _.each($checkboxes, function(checkbox) {
            $(checkbox).prop('checked', true);
          })
        }
      },
      form_check: function() {
        this.is_loading = true;
        if (this.form_validation()) {
          this.up();
          this.serializeForm();
          this.updateHistory();
          this.updateStorage();
          this.requestTrial();

        }
      },
      form_validation: function() {
        var vueThis = this;
        var isValid = true;

        $('.error-text').remove();
        if (!_.isNull(vueThis.$el.querySelectorAll('.is-error'))) {
          var errors = vueThis.$el.querySelectorAll('.is-error');
          _.each($(errors), function(el) {
            $(el).removeClass('is-error');
          });
        }

        /* Put vue method tag to update the local storage on change on the form change */
        // Create an array of unique form fields
        var inputs = vueThis.$el.querySelectorAll('[required="required"]');

        _.each($(inputs), function(el) {
          var $el = $(el);
          var type = $el.prop('type');
          var $elContainer = $el.parents('.field');
          switch (type) {
            case 'select-one':
              if ($el.find('option:selected').val().length === 0) {
                vueThis.display_error(
                  $elContainer,
                  vueThis.translation['Choose an option']
                );
                isValid = false;
              }
              if (
                $el.hasClass('countries') &&
                $el.find('option:selected').val() === '0'
              ) {
                vueThis.display_error(
                  $elContainer,
                  vueThis.translation['Choose an option']
                );
                isValid = false;
              }
              break;
            case 'select-multiple':
              if ($el.find('option:selected').length === 0) {
                vueThis.display_error(
                  $elContainer,
                  vueThis.translation['Choose an option']
                );
                isValid = false;
              }
              break;
            case 'email':
              // eslint-disable-next-line no-useless-escape
              var emailRegex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
              if ($el.val().length === 0) {
                vueThis.display_error(
                  $elContainer,
                  vueThis.translation['This field is required']
                );
                isValid = false;
              } else if (emailRegex.test($el.val()) === false) {
                vueThis.display_error(
                  $elContainer,
                  vueThis.translation['Check the email format']
                );
                isValid = false;
              }
              break;
            case 'tel':
              var phoneRegex = /^-?\d+\.?\d*$/;
              if ($el.val().length === 0) {
                vueThis.display_error(
                  $elContainer,
                  vueThis.translation['This field is required']
                );
                isValid = false;
              } else if (
                phoneRegex.test(vueThis.contact.phone_number) === false
              ) {
                vueThis.display_error(
                  $elContainer,
                  'Check the phone number format'
                );
                isValid = false;
              }
              break;
            // Plain text like things
            default:
              if ($el.val().length === 0) {
                vueThis.display_error(
                  $elContainer,
                  vueThis.translation['This field is required']
                );
                isValid = false;
              }
          }
        });
        var $required = vueThis.$el.querySelectorAll('fieldset.required');
        _.each($required, function(el) {
          var $el = $(el);
          var $lastInput = $el.find('input').last();

          var name = $lastInput.attr('name');
          var selectedItem = $('input[name="' + name + '"]:checked');
          if (selectedItem.length === 0) {
            vueThis.display_error($el, vueThis.translation['Choose an option']);
            isValid = false;
          }
        });

        if (isValid === true) {
          return true;
        } else {
          var el = $('.field.is-error')[0];
          var $text = $(el).find('.error-text');
          var offsetTop = $text.offset().top - 84;
          var offsetBottom = offsetTop + $text.outerHeight();

          var viewportTop = $(window).scrollTop() + this.headerHeight;
          var viewportBottom = viewportTop + $(window).height();

          if (offsetBottom < viewportTop || offsetTop > viewportBottom) {
            $('html, body').animate(
              {
                scrollTop: offsetTop,
              },
              150
            );
          }
          this.is_loading = false;
          return false;
        }
      },
      display_error: function(el, message) {
        var $el = $(el);
        var $errorContainer = null;
        $el.addClass('is-error');
        if ($el.children('.heading')) {
          $el.children('.heading').append('<p class="error-text"></p>');
          $errorContainer = $el.children('.heading').find('.error-text');
        }
        if (!_.isNull($errorContainer)) {
          $errorContainer.html(message);
        }
      },
      serializeForm: function() {
        var vueThis = this;

        var form = vueThis.$el.querySelector('form');
        var formData = $(form).serializeArray();

        vueThis.form.form_data = formData;

        var country = $('select#fields-countries')
          .find(':selected')
          .text();
        vueThis.form.contry = country;

        vueThis.updateStorage();
      },
      requestTrial: function() {
        var vueThis = this;
        var formData = vueThis.form.form_data;

        var email = _.find(formData, { name: 'fields[email]' });
        var firstname = _.find(formData, { name: 'fields[firstname]' });
        var lastname = _.find(formData, { name: 'fields[lastname]' });
        var company = _.find(formData, { name: 'fields[company]' });
        var country = vueThis.form.contry;

        $.ajax({
          method: 'GET',
          url: siteUrl + 'actions/solibri-solution-module/api/request-trial',
          data: {
            email: _.isObject(email) ? email.value : '',
            firstName: _.isObject(firstname) ? firstname.value : '',
            lastName: _.isObject(lastname) ? lastname.value : '',
            companyName: _.isObject(company) ? company.value : '',
            countryName: country,
            language: vueThis.showing_lang,
          },
          error: function(data) {
            vueThis.is_loading = false;
            var error = 'An error occurred please try again later';
            // console.log(data);
            // console.log('Trial download error');
            vueThis.error_message = error;
          },
        }).done(function(msg) {
          vueThis.is_loading = false;
          var data = JSON.parse(msg);
          // console.log(data);
          if (!_.isEmpty(data.body)) {
            if (data.body.statusCode === '0') {
              vueThis.updateStorage();
              vueThis.salesforce();
              vueThis.subscribe_newsletter();
              vueThis.save_form_entry();
            }
          } else {
            // console.log('Trial download error');
            // console.log('error');
            // console.log(data);
            var error = data.errors.statusMessage;
            vueThis.error_message = error;
            if (data.errors.statusCode === '1001') {
              vueThis.down();
              vueThis.statusCode = '1001';
            }
          }
        });
      },
      salesforce: function() {
        var vueThis = this;
        var formData = vueThis.form.form_data;

        var firstname = _.find(formData, { name: 'fields[firstname]' });
        var lastname = _.find(formData, { name: 'fields[lastname]' });
        var email = _.find(formData, { name: 'fields[email]' });
        var phone =
          vueThis.contact.country.code + ' ' + vueThis.contact.phone_number;
        var company = _.find(formData, { name: 'fields[company]' });
        var areaOfBusiness = _.find(formData, {
          name: 'fields[areaOfBusiness]',
        });
        var personnel = _.find(formData, { name: 'fields[personnel]' });
        var country = _.find(formData, { name: 'fields[countries]' });
        var city = _.find(formData, { name: 'fields[city]' });
        var website = _.find(formData, { name: 'fields[website1]' });
        var referrer = _.find(formData, { name: 'fields[referrer]' });
        var source = 'Trial: ' + vueThis.type;

        $.ajax({
          method: 'GET',
          url: siteUrl + 'actions/salesforce-module/api/send',
          data: {
            firstName: _.isObject(firstname) ? firstname.value : '',
            lastName: _.isObject(lastname) ? lastname.value : '',
            email: _.isObject(email) ? email.value : '',
            phone: phone,
            company: _.isObject(company) ? company.value : '',
            areaOfBusiness: _.isObject(areaOfBusiness)
              ? areaOfBusiness.value
              : '',
            personnel: _.isObject(personnel) ? personnel.value : '',
            country_code: _.isObject(country) ? country.value : '',
            city: _.isObject(city) ? city.value : '',
            website: _.isObject(website) ? website.value : '',
            source: source,
            referrer: _.isObject(referrer) ? referrer.value : '',
          },
          error: function(data) {
            vueThis.is_loading = false;
            // console.log(data);
            // console.log('salesforce error');
          },
        }).done(function(msg) {
          vueThis.is_loading = false;
          // var data = JSON.parse(msg);
          // console.log('salesforce done');
          // console.log(data);
        });
      },
      subscribe_newsletter: function() {
        var vueThis = this;
        var formData = vueThis.form.form_data;

        var firstname = _.find(formData, { name: 'fields[firstname]' });
        var lastname = _.find(formData, { name: 'fields[lastname]' });
        var email = _.find(formData, { name: 'fields[email]' });

        $.ajax({
          method: 'GET',
          url: siteUrl + 'actions/solibri-solution-module/api/mailchimp',
          data: {
            email: _.isObject(email) ? email.value : '',
            firstname: _.isObject(firstname) ? firstname.value : '',
            lastname: _.isObject(lastname) ? lastname.value : '',
          },
          error: function(data) {
            vueThis.is_loading = false;
            // console.log(data);
            // console.log('mailchimp error');
          },
        }).done(function(msg) {
          vueThis.is_loading = false;
          // var data = JSON.parse(msg);
          // console.log('mailchimp done');
          // console.log(data);
        });
      },
      save_form_entry: function() {
        var vueThis = this;
        // prepare some variables
        var postUrl = 'sproutForms/entries/saveEntry';

        // process the form
        $.ajax({
          type: 'POST',
          url: postUrl,
          data: vueThis.form.form_data,
          dataType: 'json',
          encode: true,
          error: function(data) {
            // console.log(data);
            vueThis.is_loading = false;
            var error = data.responseJSON.error;
            vueThis.error_message = error;
          },
        }).done(function(data) {
          vueThis.up();
          vueThis.is_loading = false;
          // console.log(data);
          if (data.success === true) {
            vueThis.form.is_saved = true;
            vueThis.step = 3;
            vueThis.updateStorage();
          } else {
            // console.log('error');
            // console.log(data);
            var error = data.errors;
            vueThis.error_message = error;
          }
        });
      },
      reset: function() {
        var def = getDefaultData();
        _.assign(this.$data, def);
        localStorage.setItem('trialDownload', JSON.stringify(def));
        this.stepNavigation();
      },
      selectOnChange: function(event) {
        var value = $(event.target).val();
        var name = $(event.target)
          .find('option:selected')
          .text();
        var code = '+' + name.split('+').pop(-1);

        trialDownloadApp.contact.country.label = name;
        trialDownloadApp.contact.country.value = value;
        trialDownloadApp.contact.country.code = code;
      },
      // For the transition
      prev: function() {
        this.direction = 'slide-prev';
        this.scrollup();
      },
      next: function() {
        this.direction = 'slide-next';
        this.scrollup();
      },
      // For the transition
      up: function() {
        this.direction = 'slide-up';
        this.scrollup();
      },
      down: function() {
        this.direction = 'slide-down';
        this.scrollup();
      },
      scrollup: function() {
        var top =
          this.step === 1
            ? this.headerHeight
            : this.step === 3
              ? this.headerHeight
              : this.headerHeight + 1;
        setTimeout(function() {
          $('html, body').animate(
            {
              scrollTop: top,
            },
            150
          );
        }, 100);
        return false;
      },
      stepNavLink: function(selectedStep) {
        if (selectedStep < this.step) {
          this.step = selectedStep;
          this.updateStorage();
          this.updateHistory();
          this.prev();
        }
      },
    },
  });

  trialDownloadApp.$mount(containerId);
};

$(window).on('load', function() {
  if ($('#trialDownload').length > 0) {
    trialDownload('#trialDownload');
  }
});
