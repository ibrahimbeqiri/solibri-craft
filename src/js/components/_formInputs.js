/* global _, siteUrl */
//=require lodash/lodash.js

var styledSelect = function() {
  $('select')
    .not('.phone')
    .each(function() {
      var $this = $(this);
      var selectedOption = $this.find(':selected').text();

      if (!$this.attr('multiple')) {
        $this.addClass('styled');

        if ($this.parent('.select-wrapper').length === 0) {
          $this.wrap("<div class='select-wrapper'></div>");
          $this.after("<span class='holder'>" + selectedOption + '</span>');
        } else {
          $this.next('.holder').text(selectedOption);
        }
      }
    });

  $(document).on('change', 'select.styled:not(".phone")', function() {
    var $this = $(this);
    var selectedOption = $this.find(':selected').text();

    $this.next('.holder').text(selectedOption);
    // fields-phone-country
  });
};

var styledCheckboxAndRadio = function() {
  var $inputs = $('form input[type="checkbox"], form input[type="radio"]');
  var $label = $inputs.parents('label');

  // `v` stands for label element
  $label.each(function(i, v) {
    // Get a length of label's children
    var length = $(v).contents().length;

    // Get outer HTML ot input element in the label
    var $input = $(v)
      .contents()
      .eq(0);

    // Get an array of element in the label execpt input
    // Mostly text and text-related tags (<a> <span>...)
    var elements = $(v)
      .contents()
      .slice(1, length);

    // Now we are creating dummy element combined with input + text
    var content = '';

    // Loop through the array of element
    // And add it to content string
    $(elements).each(function() {
      var $el = $(this);
      var line;
      if ($el[0].outerHTML) {
        line = $el[0].outerHTML;
      } else {
        line = $el[0].textContent;
      }
      $el.remove();
      content += line;
    });

    // Create an empty dummy span and add class and content string as inner HTML
    var labelSpan = document.createElement('span');
    labelSpan.innerHTML = content;
    labelSpan.className = 'label-text';

    // Replace inner html of label with new span
    $input.after(labelSpan);
  });

  var $labelText = $('.label-text');

  $labelText.each(function() {
    $(this).html(function(i, html) {
      return html.replace(/&nbsp;/, '');
    });
  });
  $inputs.parent('label').addClass('styled');
  $inputs.after('<span class="dummy"></span>');
};

var validationStyle = function() {
  var inputs = document.querySelectorAll('input, select, textarea');

  _.each(inputs, function(input) {
    input.addEventListener('invalid', isError, false);
    input.addEventListener('blur', isError, false);
  });

  // Validating for checkboxes
  var $required = document.querySelectorAll('fieldset.required');
  _.each($required, function(el) {
    var $formButton = $(el)
      .parents('form')
      .find('.submit');
    var $el = $(el);
    var $lastInput = $el.find('input').last();

    var name = $lastInput.attr('name');
    var selectedItems = document.querySelectorAll('input[name="' + name + '"]');

    _.each(selectedItems, function(selectedItem) {
      selectedItem.addEventListener('change', isCheckboxError, false);
    });

    $formButton.on('click', function(event) {
      _.each(selectedItems, function(selectedItem) {
        var validateCheckboxes = isCheckboxError(selectedItem);
        if (validateCheckboxes === false) {
          event.preventDefault();
        }
      });
    });
  });

  function isError(event) {
    if ($(event.target).is(':invalid')) {
      $(event.target)
        .parents('.field')
        .addClass('is-error');
    } else {
      $(event.target)
        .parents('.field')
        .removeClass('is-error');
    }
  }

  function isCheckboxError(event) {
    var el = event.target ? event.target : event;
    // Looking for twins
    var name = $(el).attr('name');
    var selectedItems = document.querySelectorAll('input[name="' + name + '"]');
    var noOptionChecked = true;
    _.each(selectedItems, function(selectedItem) {
      if ($(selectedItem).is(':checked')) {
        noOptionChecked = false;
      }
    });

    if (noOptionChecked) {
      $(el)
        .parents('.field.checkboxes')
        .addClass('is-error');

      return false;
    } else {
      $(el)
        .parents('.field.checkboxes')
        .removeClass('is-error');
    }
  }
};

var solibriLicenseFormHijack = function() {
  var $form = $('#getSolibriModelViewerLicense-form');
  var $formConatiner = $form.parents('.form-section__content');

  $form.on('submit', function(e) {
    e.preventDefault();

    var $loader = $form.parents('.article-wrapper').find('.loading-indicator');

    $loader.addClass('is-loading');
    if ($formConatiner.children('.error-text').length === 0) {
      $formConatiner.prepend('<p class="error-text"></p>');
      $formConatiner.append('<p class="error-text"></p>');
    }

    var rawData = $form.serializeArray();

    var parsedData = {
      fields: {},
    };

    for (var i = 0; i < rawData.length; i++) {
      var name = rawData[i]['name'];
      var val = rawData[i]['value'];

      if (_.includes(name, 'fields[')) {
        var subName = name.replace('fields[', '').replace(']', '');
        parsedData['fields'][subName] = val;
      } else {
        parsedData[name] = val;
      }
    }

    var sendData = parsedData.fields;

    sendData[window.csrfTokenName] = window.csrfTokenValue; // Append CSRF Token

    $.ajax({
      type: 'POST',
      url: siteUrl + 'actions/solibri-solution-module/api/request-smv-license',
      data: sendData,
      error: function(data) {
        $loader.removeClass('is-loading');
        var error = data.responseJSON;
        $formConatiner.children('.error-text').html(error);
      },
      success: function(response) {
        // Check if there are any errors
        var responseObj = JSON.parse(response);

        if ('statusMessage' in responseObj.errors) {
          $loader.removeClass('is-loading');

          $formConatiner
            .children('.error-text')
            .html(responseObj.errors.statusMessage);

          return false;
        }
        // No errors, we can proceed

        var action = parsedData.action;
        // process the form
        $.ajax({
          type: 'POST',
          url: action,
          data: $form.serialize(),
          dataType: 'json',
          encode: true,
          error: function(data) {
            $loader.removeClass('is-loading');
            var error = data.responseJSON;

            $formConatiner.children('.error-text').html(error);
          },
        }).done(function(data) {
          $loader.removeClass('is-loading');

          if (data.success !== true) {
            $formConatiner
              .children('.error-text')
              .html('Could not save the entry.');

            return false;
          }

          var wholeUrl = window.location.href;
          var newUrl =
            wholeUrl.replace(window.location.search, '') +
            "?ok=1&message=Solibri Model Viewer License is sent to your email address.<br> If you can't find the email, please make sure to check your Spam folder.";
          window.location.href = newUrl;
        });
      },
    });
  });
};

var $viewportMeta = $('meta[name="viewport"]');
$('input, select, textarea').bind('focus blur', function(event) {
  $viewportMeta.attr(
    'content',
    'width=device-width,initial-scale=1,' +
      (event.type === 'blur'
        ? 'shrink-to-fit=no'
        : 'maximum-scale=' + 1 + ',user-scalable=0')
  );
});

$(document).ready(function() {
  styledSelect();
  styledCheckboxAndRadio();
  validationStyle();
  solibriLicenseFormHijack();
});
