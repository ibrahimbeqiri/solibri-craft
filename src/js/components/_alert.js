// https://codepen.io/mpeutz2/pen/ixyJv
var Alert;
var _container;

(function(Alert) {
  var alert, error, info, success, warning;
  info = function(message, title, options) {
    return alert('info', message, title, 'icon-info-sign', options);
  };
  warning = function(message, title, options) {
    return alert('warning', message, title, 'icon-warning-sign', options);
  };
  error = function(message, title, options) {
    return alert('error', message, title, 'icon-minus-sign', options);
  };
  success = function(message, title, options) {
    return alert('success', message, title, '', options);
  };
  alert = function(type, message, title, icon, options) {
    var alertElem;
    var messageElem;
    var titleElem;
    var iconElem;
    var innerElem;
    var _wrap;
    if (typeof options === 'undefined') {
      options = {};
    }
    options = $.extend({}, Alert.defaults, options);
    if (!_wrap) {
      _wrap = $('#alerts');
      if (_wrap.length === 0) {
        _wrap = $('<div>')
          .attr('class', 'alerts--wrap')
          .appendTo($('body'));
      }
      if (!_container) {
        _container = $('#alerts');
        if (_container.length === 0) {
          _container = $('<div>')
            .attr('id', 'alerts')
            .appendTo($('.alerts--wrap'));
        }
      }
    }
    if (options.width) {
      _container.css({
        width: options.width,
      });
    }
    alertElem = $('<div>')
      .addClass('alert')
      .addClass('alert--' + type);
    setTimeout(function() {
      alertElem.addClass('open');
      $('.alerts--wrap').addClass('alert--open');
      $('body').addClass('modal-is-active');
    }, 1);
    if (icon) {
      iconElem = $('<i>').addClass(icon);
      alertElem.append(iconElem);
    }
    innerElem = $('<div>').addClass('alert--block');
    alertElem.append(innerElem);
    if (title) {
      titleElem = $('<div>')
        .addClass('alert--title')
        .append(title);
      innerElem.append(titleElem);
    }
    if (message) {
      messageElem = $('<div>')
        .addClass('alert--message')
        .append(message);
      innerElem.append(messageElem);
    }
    if (options.displayDuration > 0) {
      setTimeout(function() {
        leave();
      }, options.displayDuration);
    } else {
      // innerElem.append('<div class="alert--dismiss">Click to Dismiss</em>');
    }
    alertElem.on('click', function() {
      leave();
    });
    function leave() {
      alertElem.removeClass('open');
      $('.alerts--wrap').removeClass('alert--open');
      $('body').removeClass('modal-is-active');
      alertElem.one(
        'webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend',
        function() {
          return $('.alerts--wrap').remove();
        }
      );
    }
    return _container.prepend(alertElem);
  };
  Alert.defaults = {
    width: '',
    icon: '',
    displayDuration: 3000,
    pos: '',
  };
  Alert.info = info;
  Alert.warning = warning;
  Alert.error = error;
  Alert.success = success;
  return (_container = void 0);
})(Alert || (Alert = {}));

this.Alert = Alert;

// $('#test').on('click', function() {
//   Alert.info('Message');
// });
