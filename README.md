## This needs to be written

## How to set up

* preferably use laravel Valet instead of MAMP
* set correct variables for .env file (see .env.example file, font delete this ever)
* set up database, import database dump
* run `composer install`
* run `yarn install`
* run `yarn start`

## How to Install Craft 3

See the Craft 3 documentation for [installation](https://docs.craftcms.com/v3/installation.html) and [updating](https://docs.craftcms.com/v3/updating.html) instructions.

## Resources

#### Official Resources

* [Craft 3 Documentation](https://docs.craftcms.com/v3/)
* [Craft 3 Class Reference](https://docs.craftcms.com/api/v3/)
* [Craft 3 Plugins](https://plugins.craftcms.com)
* [Demo site](https://demo.craftcms.com/)
* [Craft Slack](https://craftcms.com/community#slack)
* [Craft CMS Stack Exchange](http://craftcms.stackexchange.com/)

#### Community Resources

* [Mijingo](https://mijingo.com/craft) – Video courses and other learning resources
* [Envato Tuts+](https://webdesign.tutsplus.com/categories/craft-cms/courses) – Video courses
* [Straight Up Craft](http://straightupcraft.com/) – Articles, tutorials, and more
* [pluginfactory.io](https://pluginfactory.io/) – Craft plugin scaffold generator
