<?php
/**
 * Salesforce module for Craft CMS 3.x
 *
 * Salesforce api
 *
 * @link      http://agencyleroy.com
 * @copyright Copyright (c) 2018 Agency Leroy
 */

/**
 * @author    Agency Leroy
 * @package   SalesforceModule
 * @since     1.0.0
 */
return [
    'Salesforce plugin loaded' => 'Salesforce plugin loaded',
];
