<?php
/**
 * Salesforce module for Craft CMS 3.x
 *
 * Salesforce api
 *
 * @link      http://agencyleroy.com
 * @copyright Copyright (c) 2018 Agency Leroy
 */

namespace modules\salesforcemodule;

use modules\salesforcemodule\services\SalesforceModuleService as SalesforceModuleServiceService;
use modules\salesforcemodule\variables\SalesforceModuleVariable;

use Craft;
use craft\i18n\PhpMessageSource;
use craft\web\UrlManager;
use craft\web\twig\variables\CraftVariable;
use craft\events\RegisterUrlRulesEvent;

use yii\base\Event;
use yii\base\InvalidConfigException;
use yii\base\Module;

/**
 * Class SalesforceModule
 *
 * @author    Agency Leroy
 * @package   SalesforceModule
 * @since     1.0.0
 *
 * @property  SalesforceModuleServiceService $salesforceModuleService
 */
class SalesforceModule extends Module
{
    // Static Properties
    // =========================================================================

    /**
     * @var SalesforceModule
     */
    public static $instance;

    // Public Methods
    // =========================================================================

    /**
     * @inheritdoc
     */
    public function __construct($id, $parent = null, array $config = [])
    {
        Craft::setAlias('@modules/salesforcemodule', $this->getBasePath());
        $this->controllerNamespace = 'modules\salesforcemodule\controllers';

        // Translation category
        $i18n = Craft::$app->getI18n();
        /** @noinspection UnSafeIsSetOverArrayInspection */
        if (!isset($i18n->translations[$id]) && !isset($i18n->translations[$id.'*'])) {
            $i18n->translations[$id] = [
                'class' => PhpMessageSource::class,
                'sourceLanguage' => 'en-US',
                'basePath' => '@modules/salesforcemodule/translations',
                'forceTranslation' => true,
                'allowOverrides' => true,
            ];
        }

        // Set this as the global instance of this module class
        static::setInstance($this);

        parent::__construct($id, $parent, $config);
    }

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        self::$instance = $this;

        Event::on(
            UrlManager::class,
            UrlManager::EVENT_REGISTER_CP_URL_RULES,
            function (RegisterUrlRulesEvent $event) {
                $event->rules['send'] = 'modules/salesforce-module/api/send';
            }
        );

        Event::on(
            CraftVariable::class,
            CraftVariable::EVENT_INIT,
            function (Event $event) {
                /** @var CraftVariable $variable */
                $variable = $event->sender;
                $variable->set('salesforceModule', SalesforceModuleVariable::class);
            }
        );

        Craft::info(
            Craft::t(
                'salesforce-module',
                '{name} module loaded',
                ['name' => 'Salesforce']
            ),
            __METHOD__
        );
    }

    // Protected Methods
    // =========================================================================
}
