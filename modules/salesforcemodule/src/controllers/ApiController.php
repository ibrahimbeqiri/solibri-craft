<?php
/**
 * Salesforce module for Craft CMS 3.x
 *
 * Salesforce api
 *
 * @link      http://agencyleroy.com
 * @copyright Copyright (c) 2018 Agency Leroy
 */

namespace modules\salesforcemodule\controllers;

use modules\salesforcemodule\SalesforceModule;

use Craft;
use craft\web\Controller;

/**
 * @author    Agency Leroy
 * @package   SalesforceModule
 * @since     1.0.0
 */
class ApiController extends Controller
{

    // Protected Properties
    // =========================================================================

    /**
     * @var    bool|array Allows anonymous access to this controller's actions.
     *         The actions must be in 'kebab-case'
     * @access protected
     */
    protected $allowAnonymous = ['send'];

    // Public Methods
    // =========================================================================

    /**
     * @return json response from api
     */
    public function actionSend()
    {

      $request = Craft::$app->getRequest();
      $form_data = $request->getQueryParams();

      $response = SalesforceModule::$instance->salesforceModuleService->send($form_data);

      return $response;
    }
}
