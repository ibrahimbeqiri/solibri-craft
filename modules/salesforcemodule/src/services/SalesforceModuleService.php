<?php
/**
 * Salesforce module for Craft CMS 3.x
 *
 * Salesforce api
 *
 * @link      http://agencyleroy.com
 * @copyright Copyright (c) 2018 Agency Leroy
 */

namespace modules\salesforcemodule\services;

use modules\salesforcemodule\SalesforceModule;

use Craft;
use craft\base\Component;

/**
 * @author    Agency Leroy
 * @package   SalesforceModule
 * @since     1.0.0
 */
class SalesforceModuleService extends Component
{

  protected $rest;
  protected $api_url;
  protected $default_options;
  protected $oid;

  /**
   * Constructor.
   */

  public function __construct()
  {

    $oid = getenv('SALESFORCE_OID');
    $api_url = 'https://webto.salesforce.com';

    $client_opts = array(
      'base_uri'      => $api_url,
      'http_errors'   => false,
      'timeout' => 10
    );

    $this->oid = $oid;
    $this->rest = new \GuzzleHttp\Client($client_opts);
    $this->api_url = $api_url;
    $this->default_options = array();
  }

  /**
   * Sales force
   * @param object $form_data (required)
   *
   * @return server response
   */

  public function send($form_data)
  {

    if (!isset($form_data['firstName'])) {
      $error = array(
        'errors' => "'firstName' is required",
      );

      return json_encode($error);
    }
    if (!isset($form_data['lastName'])) {
      $error = array(
        'errors' => "'lastName' is required",
      );

      return json_encode($error);
    }
    if (!isset($form_data['email'])) {
      $error = array(
        'errors' => "'email' is required",
      );

      return json_encode($error);
    }
    if (!isset($form_data['phone'])) {
      $error = array(
        'errors' => "'phone' is required",
      );

      return json_encode($error);
    }
    if (!isset($form_data['company'])) {
      $error = array(
        'errors' => "'company' is required",
      );

      return json_encode($error);
    }
    if (!isset($form_data['areaOfBusiness'])) {
      $error = array(
        'errors' => "'areaOfBusiness' is required",
      );

      return json_encode($error);
    }
    if (!isset($form_data['personnel'])) {
      $error = array(
        'errors' => "'personnel' is required",
      );

      return json_encode($error);
    }
    if (!isset($form_data['country_code'])) {
      $error = array(
        'errors' => "'country_code' is required",
      );

      return json_encode($error);
    }
    if (!isset($form_data['city'])) {
      $error = array(
        'errors' => "'city' is required",
      );

      return json_encode($error);
    }
    if (!isset($form_data['website'])) {
      $error = array(
        'errors' => "'website' is required",
      );

      return json_encode($error);
    }
    if (!isset($form_data['source'])) {
      $error = array(
        'errors' => "'source' is required",
      );

      return json_encode($error);
    }
    if (!isset($form_data['referrer'])) {
      $form_data["referrer"] = Craft::$app->getRequest()->getReferrer();
    }

    $query = array(
      'oid' => $this->oid,
      'retUrl' => Craft::$app->sites->currentSite->baseUrl,
      'first_name' => $form_data['firstName'],
      'last_name' => $form_data['lastName'],
      'email' => $form_data['email'],
      'phone' => $form_data['phone'],
      'company' => $form_data['company'],
      'industry' => $form_data['areaOfBusiness'],
      'employees' => $form_data['personnel'],
      'country_code' => $form_data['country_code'],
      'city' => $form_data['city'],
      'URL' => $form_data['website'],
      'lead_source' => $form_data['source'],
      'referrer' => $form_data['referrer']
    );

    if (Craft::$app->getConfig()->general->devMode) {
      $query['debug'] = 1;
      $query['debugEmail'] = 'hyein@agencyleroy.com';
    }

    $response = $this->rest->post('/servlet/servlet.WebToLead?encoding=UTF-8', array_merge(
      $this->default_options,
      array('form_params' => $query)
    ));

    return $this->parseResponse($response);
  }

  /**
   * parse response
   *
   * @param string $raw_data
   *
   * @return server response
   */

  public function parseResponse($raw_response) {

    $body = (! isset($raw_response->body)) ? json_decode($raw_response->getBody()) : $raw_response->body;
    $errors = new \stdClass();
    // Handle errors
    if (isset($body->errors)) {
        $errors = $body->errors; // when response is {errors: {}}
        unset($body->errors);
    } elseif ($raw_response->getStatusCode() == 400) {
        $errors = $body; // body here is a stdClass
        $body = new \stdClass();
    } elseif ($raw_response->getStatusCode() != 200 && gettype($body) == 'string') {
        // the response was an error so put the body as an error
        $errors = (object) array("error" => $body);
        $body = new \stdClass();
    }

    if ($raw_response->getStatusCode() == 200) {
      $body = (object) array(
        "statusCode" => $raw_response->getStatusCode(),
        "statusMessage" => $raw_response->getReasonPhrase(),
        "statusBody" => (string) $raw_response->getBody(),
      );
    }

    $response = array(
      'body' => $body,
      'errors' => $errors,
    );

    return json_encode($response);
  }

}
