<?php
/**
 * Salesforce module for Craft CMS 3.x
 *
 * Salesforce api
 *
 * @link      http://agencyleroy.com
 * @copyright Copyright (c) 2018 Agency Leroy
 */

namespace modules\salesforcemodule\variables;

use modules\salesforcemodule\SalesforceModule;

use Craft;

/**
 * @author    Agency Leroy
 * @package   SalesforceModule
 * @since     1.0.0
 */
class SalesforceModuleVariable
{
    // Public Methods
    // =========================================================================

    /**
     * @param $form_data the form data to send to salesforce
     * @return string
     *
     * How you use in twig templates
     * {{ craft.salesforceModule.send($form_data) }}
     */
    public function send($form_data)
    {

      $response = SalesforceModule::$instance->salesforceModuleService->send($form_data);

      return $response;
    }
}
