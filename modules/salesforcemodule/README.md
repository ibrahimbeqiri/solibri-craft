# Salesforce module for Craft CMS 3.x

Salesforce api

## Requirements

This module requires Craft CMS 3.0.0-RC1 or later.

## Installation

To install the module, follow these instructions.

First, you'll need to add the contents of the `app.php` file to your `config/app.php` (or just copy it there if it does not exist). This ensures that your module will get loaded for each request. The file might look something like this:

```
return [
    'modules' => [
        'salesforce-module' => [
            'class' => \modules\salesforcemodule\SalesforceModule::class,
            'components' => [
                'salesforceModuleService' => [
                    'class' => 'modules\salesforcemodule\services\SalesforceModuleService',
                ],
            ],
        ],
    ],
    'bootstrap' => ['salesforce-module'],
];
```

You'll also need to make sure that you add the following to your project's `composer.json` file so that Composer can find your module:

    "autoload": {
        "psr-4": {
          "modules\\salesforcemodule\\": "modules/salesforcemodule/src/"
        }
    },

After you have added this, you will need to do:

    composer dump-autoload

…from the project’s root directory, to rebuild the Composer autoload map. This will happen automatically any time you do a `composer install`.

## Salesforce Overview

This is a simple module to connect to the Salesforce API.

## Using Salesforce

You need to set the SALESFORCE_OID in your .env file.

Example:
SALESFORCE_OID="your_oid"

Use directly in twig template (Variables):

Sending form to salesforce:
{{ craft.salesforceModule.send($form_data) }}

Ajax request urls (Controller actions):

Sending a verification code as sms:
http://solibri-craft.test/actions/salesforce-module/api/send?firstName=Martin&lastName=Stigzelius&email=martin@agencyleroy.com&phone=0505429605&company=Agency Leroy&areaOfBusiness=Consulting&personnel=24&country_code=FI&city=Helsinki&website=agencyleroy.com&source=Trial: Option 1

## Salesforce Roadmap

Some things to do, and ideas for potential features:

* Release it

Brought to you by [Agency Leroy](http://agencyleroy.com)
