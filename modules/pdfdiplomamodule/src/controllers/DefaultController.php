<?php
/**
 * pdf-diploma module for Craft CMS 3.x
 *
 * Create PDF diploma
 *
 * @link      agencyleroy.com
 * @copyright Copyright (c) 2019 Agency Leroy
 */

namespace modules\pdfdiplomamodule\controllers;

use modules\pdfdiplomamodule\PdfdiplomaModule;

use Craft;
use craft\web\Controller;

/**
 * Default Controller
 *
 * Generally speaking, controllers are the middlemen between the front end of
 * the CP/website and your module’s services. They contain action methods which
 * handle individual tasks.
 *
 * A common pattern used throughout Craft involves a controller action gathering
 * post data, saving it on a model, passing the model off to a service, and then
 * responding to the request appropriately depending on the service method’s response.
 *
 * Action methods begin with the prefix “action”, followed by a description of what
 * the method does (for example, actionSaveIngredient()).
 *
 * https://craftcms.com/docs/plugins/controllers
 *
 * @author    Agency Leroy
 * @package   PdfdiplomaModule
 * @since     1.01
 */

class DefaultController extends Controller
{

    // Protected Properties
    // =========================================================================

    /**
     * @var    bool|array Allows anonymous access to this controller's actions.
     *         The actions must be in 'kebab-case'
     * @access protected
     */
    protected $allowAnonymous = ['index', 'do-something'];

    // Public Methods
    // =========================================================================

    /**
     * Handle a request going to our module's index action URL,
     * e.g.: actions/pdf-diploma-module/default
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $name = $_GET['name'];
        $today = date("j.n.Y");
        $mpdf = new \Mpdf\Mpdf(['orientation' => 'L', 'mode' => 'utf-8']);

        function add_custom_fonts_to_mpdf($mpdf, $fonts_list)
        {
            // Logic from line 1146 mpdf.pdf - $this->available_unifonts = array()...       
            foreach ($fonts_list as $f => $fs) {
                // add to fontdata array
                $mpdf->fontdata[$f] = $fs;

                // add to available fonts array
                if (isset($fs['R']) && $fs['R']) {
                    $mpdf->available_unifonts[] = $f;
                }
                if (isset($fs['B']) && $fs['B']) {
                    $mpdf->available_unifonts[] = $f.'B'; 
                }
                if (isset($fs['I']) && $fs['I']) {
                    $mpdf->available_unifonts[] = $f.'I'; 
                }
                if (isset($fs['BI']) && $fs['BI']) {
                    $mpdf->available_unifonts[] = $f.'BI' ; 
                }
            }
            $mpdf->default_available_fonts = $mpdf->available_unifonts;
        }
        $custom_fontdata = array(
            'scout' => array(
                'R' => '../../../../web/Scout-Black.ttf',
                'B' => '../../../../web/Scout-Bold.ttf'
            ),
        );
        add_custom_fonts_to_mpdf($mpdf, $custom_fontdata);
        $mpdf->SetImportUse();
        $mpdf->SetDocTemplate($_SERVER['DOCUMENT_ROOT'] . '/Solibri_Certificate_2019_template.pdf');
        $mpdf->SetTitle('Solibri Certificate');
        $mpdf->WriteHTML(
            '<div style="padding-top: 297px; width: 100%; margin-left: auto; margin-right: auto; text-align: center; text-transform: uppercase">
                <h1 style="font-size: 27pt; letter-spacing: 0.030em; font-family: scout; color: #033e62;">'
                . $name .
                '</h1>
                <h1 style="font-weight: normal; letter-spacing: 0.080em; padding-top: 92px; font-family: scout; color: #033e62; font-size: 10pt;">'
                . $today .
                '</h1>
            </div>'
        );
        echo $mpdf->Output('Solibri Certificate.pdf', 'I');
    }

    /**
     * Handle a request going to our module's actionDoSomething URL,
     * e.g.: actions/pdf-diploma-module/default/do-something
     *
     * @return mixed
     */
    public function actionDoSomething()
    {
        $result = 'Welcome to the DefaultController actionDoSomething() method';

        return $result;
    }
}
