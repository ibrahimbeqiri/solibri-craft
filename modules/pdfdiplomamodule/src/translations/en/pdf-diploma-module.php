<?php
/**
 * pdf-diploma module for Craft CMS 3.x
 *
 * Create PDF diploma
 *
 * @link      agencyleroy.com
 * @copyright Copyright (c) 2019 Agency Leroy
 */

/**
 * pdf-diploma en Translation
 *
 * Returns an array with the string to be translated (as passed to `Craft::t('pdf-diploma-module', '...')`) as
 * the key, and the translation as the value.
 *
 * http://www.yiiframework.com/doc-2.0/guide-tutorial-i18n.html
 *
 * @author    Agency Leroy
 * @package   PdfdiplomaModule
 * @since     1.0.0
 */
return [
    'pdf-diploma plugin loaded' => 'pdf-diploma plugin loaded',
];
