<?php
/**
 * Sprout Forms Custom Report module for Craft CMS 3.x
 *
 * Custom reporting in the frontend for sprout forms
 *
 * @link      http://agencyleroy.com
 * @copyright Copyright (c) 2018 Agency Leroy
 */

/**
 * @author    Agency Leroy
 * @package   SproutFormsCustomReportModule
 * @since     1.0.0
 */
return [
    'Sprout Forms Custom Report plugin loaded' => 'Sprout Forms Custom Report plugin loaded',
];
