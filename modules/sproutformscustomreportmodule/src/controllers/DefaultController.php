<?php
/**
 * Sprout Forms Custom Report module for Craft CMS 3.x
 *
 * Custom reporting in the frontend for sprout forms
 *
 * @link      http://agencyleroy.com
 * @copyright Copyright (c) 2018 Agency Leroy
 */

namespace modules\sproutformscustomreportmodule\controllers;

use modules\sproutformscustomreportmodule\SproutFormsCustomReportModule;

use Craft;
use craft\web\Controller;
use craft\db\Query;
use craft\elements\db\ElementQuery;
use craft\helpers\Db;

use craft\helpers\DateTimeHelper;

use barrelstrength\sproutforms\elements\Form as FormElement;

/**
 * @author    Agency Leroy
 * @package   SproutFormsCustomReportModule
 * @since     1.0.0
 */
class DefaultController extends Controller
{

    // Protected Properties
    // =========================================================================

    /**
     * @var    bool|array Allows anonymous access to this controller's actions.
     *         The actions must be in 'kebab-case'
     * @access protected
     */
    protected $allowAnonymous = ['get-form-entries'];

    // Public Methods
    // =========================================================================

    /**
     * @return mixed
     */
    public function actionGetFormEntries()
    {

        // Get the request
        $request = Craft::$app->getRequest();
        $queryParams = $request->getQueryParams();


        // Get the form id's
        $formIds = $queryParams['formId'];


        // Set default dates if not defined
        $today = DateTimeHelper::toDateTime(date('Y-m-d'));
        $todayDate = $today->format("Y-m-d");
        $monthAgoDate = $today->modify("-1 month")->format("Y-m-d");

        $startDate = (isset($queryParams['startDate']) && !empty($queryParams['startDate'])) ? $queryParams['startDate'] : $monthAgoDate;
        $endDate = (isset($queryParams['endDate']) && !empty($queryParams['endDate'])) ? $queryParams['endDate'] : $todayDate;


        // Countries parameter
        $countries = (isset($queryParams['countries']) && !empty($queryParams['countries'])) ? $queryParams['countries'] : null;

        $referrer = (isset($queryParams['referrer']) && !empty($queryParams['referrer'])) ? $queryParams['referrer'] : null;


        ///////////////////

        // Find the correct forms
        $forms = FormElement::find();
        $forms->id($formIds);


        $queries = [];

        // for each form found, build the db query
        foreach($forms->all() as $form) {
            $formQuery = new Query;
            $formQuery = $formQuery
                ->select('`dateCreated`, `field_countries`, `field_firstname`,`field_lastname`, `field_email`, `field_phone`, `field_company`, `field_areaOfBusiness`, `field_personnel`, `field_state`, `field_city`, `field_website1`, `field_referrer`')
                ->from($form->contentTable);


            // Search by dates
            $formQuery->where("dateCreated > '{$startDate}'");
            $formQuery->andWhere("dateCreated <= '{$endDate}'");

            // If countries excist, add them to the query
            if ($countries) {
                $countriesArray = explode(',', $countries);

                $parsedCountries = "";

                foreach($countriesArray as $key => $value) {
                    $parsedCountries = "{$parsedCountries}'{$value}'";
                    if ($key !== count($countriesArray) - 1) {
                        $parsedCountries = "{$parsedCountries},";
                    }
                }

                $formQuery->andWhere("field_countries IN ({$parsedCountries})");
            }

            if ($referrer) {
                $formQuery->andWhere(["field_referrer" => "{$referrer}"]);
            }

            $queries[] = $formQuery;
        }


        // Amount of queryies created
        $queriesAmount = count($queries);


        //If multiple queries we need to combine them with UNION

        if ($queriesAmount == 1) {
            // If only one query
            $query = $queries[0];
        } elseif ($queriesAmount == 2)  {
            // If only one two queries
            $query = $queries[0]->union($queries[1], true);
            $query = $query->union($queries[2], true);
        } else {
            // If more than 2 queries, start with the first one and connect the other ones
            $query = $queries[0]->union($queries[1], true);
            $i=0;
            foreach($queries as $q) {
                if ($i > 1 ) {
                    $query->union($queries[$i], true);
                }
                $i++;
            }
        }

        // Total result
        $total = $query->count();

        // Map the result
        $data = array_map(function($entry) {

            //output correct areaOfBusiness
            switch ($entry['field_areaOfBusiness']) {
                case "architecture":
                  $areaOfBusiness = "Architecture";
                break;
                case "buildingOwner":
                  $areaOfBusiness = "Building owner";
                break;
                case "constructionCompany":
                  $areaOfBusiness = "Construction company";
                break;
                case "consulting":
                  $areaOfBusiness = "Consulting";
                break;
                case "education":
                  $areaOfBusiness = "Education";
                break;
                case "engineering":
                  $areaOfBusiness = "Engineering";
                break;
                case "facilitiesManagement":
                  $areaOfBusiness = "Facilities management";
                break;
                case "research":
                  $areaOfBusiness = "Research";
                break;
                case "software":
                  $areaOfBusiness = "Software";
                break;
                case "other":
                  $areaOfBusiness = "Other";
                break;
                default:
                  $areaOfBusiness = $entry['field_areaOfBusiness'];
                break;
            };

            //output correct personnel
            switch ($entry['field_personnel']) {
                case "19":
                  $personnel = "1-9";
                break;
                case "1019":
                  $personnel = "10-19";
                break;
                case "2049":
                  $personnel = "20-49";
                break;
                case "5099":
                  $personnel = "50-99";
                break;
                case "100499":
                  $personnel = "100-499";
                break;
                case "500orMore":
                  $personnel = "500 or more";
                break;
                default:
                  $personnel = $entry['field_personnel'];
                break;
            };
            return [
                'dateCreated' => $entry['dateCreated'],
                'field_firstname' => $entry['field_firstname'],
                'field_lastname' => $entry['field_lastname'],
                'field_email' => $entry['field_email'],
                'field_phone' => json_decode($entry['field_phone'])->phone,
                'field_company' => $entry['field_company'],
                'field_areaOfBusiness' => $areaOfBusiness,
                'field_personnel' => $personnel,
                'field_countries' => $entry['field_countries'],
                'field_state' => $entry['field_state'],
                'field_city' => $entry['field_city'],
                'field_website1' => $entry['field_website1'],
                'field_referrer' => $entry['field_referrer'],
            ];
        }, $query->all());

        // Build the response
        $response = [
            'data' => $data,
            'meta' => [
                'startDate' => $startDate,
                'endDate' => $endDate,
                'pagination' => [
                    'total' => $total,
                ]
            ]
        ];

        // Return json
        return json_encode($response);
    }
}
