<?php
/**
 * Sprout Forms Custom Report module for Craft CMS 3.x
 *
 * Custom reporting in the frontend for sprout forms
 *
 * @link      http://agencyleroy.com
 * @copyright Copyright (c) 2018 Agency Leroy
 */

namespace modules\sproutformscustomreportmodule\services;

use modules\sproutformscustomreportmodule\SproutFormsCustomReportModule;

use Craft;
use craft\base\Component;

/**
 * @author    Agency Leroy
 * @package   SproutFormsCustomReportModule
 * @since     1.0.0
 */
class SproutFormsCustomReportModuleService extends Component
{
    // Public Methods
    // =========================================================================

    /*
     * @return mixed
     */
    public function exampleService()
    {
        $result = 'something';

        return $result;
    }
}
