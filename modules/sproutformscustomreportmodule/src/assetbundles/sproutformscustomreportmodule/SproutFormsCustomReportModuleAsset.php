<?php
/**
 * Sprout Forms Custom Report module for Craft CMS 3.x
 *
 * Custom reporting in the frontend for sprout forms
 *
 * @link      http://agencyleroy.com
 * @copyright Copyright (c) 2018 Agency Leroy
 */

namespace modules\sproutformscustomreportmodule\assetbundles\SproutFormsCustomReportModule;

use Craft;
use craft\web\AssetBundle;
use craft\web\assets\cp\CpAsset;

/**
 * @author    Agency Leroy
 * @package   SproutFormsCustomReportModule
 * @since     1.0.0
 */
class SproutFormsCustomReportModuleAsset extends AssetBundle
{
    // Public Methods
    // =========================================================================

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->sourcePath = "@modules/sproutformscustomreportmodule/assetbundles/sproutformscustomreportmodule/dist";

        $this->depends = [
            CpAsset::class,
        ];

        $this->js = [
            'js/SproutFormsCustomReportModule.js',
        ];

        $this->css = [
            'css/SproutFormsCustomReportModule.css',
        ];

        parent::init();
    }
}
