/**
 * Sprout Forms Custom Report module for Craft CMS
 *
 * Sprout Forms Custom Report JS
 *
 * @author    Agency Leroy
 * @copyright Copyright (c) 2018 Agency Leroy
 * @link      http://agencyleroy.com
 * @package   SproutFormsCustomReportModule
 * @since     1.0.0
 */
