<?php
/**
 * Sprout Forms Custom Report module for Craft CMS 3.x
 *
 * Custom reporting in the frontend for sprout forms
 *
 * @link      http://agencyleroy.com
 * @copyright Copyright (c) 2018 Agency Leroy
 */

namespace modules\sproutformscustomreportmodule;

use modules\sproutformscustomreportmodule\services\SproutFormsCustomReportModuleService as SproutFormsCustomReportModuleServiceService;
use modules\sproutformscustomreportmodule\variables\SproutFormsCustomReportModuleVariable;

use Craft;
use craft\i18n\PhpMessageSource;
use craft\web\UrlManager;
use craft\web\twig\variables\CraftVariable;
use craft\events\RegisterUrlRulesEvent;

use yii\base\Event;
use yii\base\InvalidConfigException;
use yii\base\Module;

/**
 * Class SproutFormsCustomReportModule
 *
 * @author    Agency Leroy
 * @package   SproutFormsCustomReportModule
 * @since     1.0.0
 *
 * @property  SproutFormsCustomReportModuleServiceService $sproutFormsCustomReportModuleService
 */
class SproutFormsCustomReportModule extends Module
{
    // Static Properties
    // =========================================================================

    /**
     * @var SproutFormsCustomReportModule
     */
    public static $instance;

    // Public Methods
    // =========================================================================

    /**
     * @inheritdoc
     */
    public function __construct($id, $parent = null, array $config = [])
    {
        Craft::setAlias('@modules/sproutformscustomreportmodule', $this->getBasePath());
        $this->controllerNamespace = 'modules\sproutformscustomreportmodule\controllers';

        // Translation category
        $i18n = Craft::$app->getI18n();
        /** @noinspection UnSafeIsSetOverArrayInspection */
        if (!isset($i18n->translations[$id]) && !isset($i18n->translations[$id.'*'])) {
            $i18n->translations[$id] = [
                'class' => PhpMessageSource::class,
                'sourceLanguage' => 'en-US',
                'basePath' => '@modules/sproutformscustomreportmodule/translations',
                'forceTranslation' => true,
                'allowOverrides' => true,
            ];
        }

        // Set this as the global instance of this module class
        static::setInstance($this);

        parent::__construct($id, $parent, $config);
    }

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        self::$instance = $this;

        Event::on(
            UrlManager::class,
            UrlManager::EVENT_REGISTER_SITE_URL_RULES,
            function (RegisterUrlRulesEvent $event) {
                $event->rules['getFormEntries'] = 'modules/sprout-forms-custom-report-module/default/get-form-entries';
            }
        );

        Event::on(
            CraftVariable::class,
            CraftVariable::EVENT_INIT,
            function (Event $event) {
                /** @var CraftVariable $variable */
                $variable = $event->sender;
                $variable->set('sproutFormsCustomReportModule', SproutFormsCustomReportModuleVariable::class);
            }
        );

        Craft::info(
            Craft::t(
                'sprout-forms-custom-report-module',
                '{name} module loaded',
                ['name' => 'Sprout Forms Custom Report']
            ),
            __METHOD__
        );
    }

    // Protected Methods
    // =========================================================================
}
