<?php
/**
 * Sprout Forms Custom Report module for Craft CMS 3.x
 *
 * Custom reporting in the frontend for sprout forms
 *
 * @link      http://agencyleroy.com
 * @copyright Copyright (c) 2018 Agency Leroy
 */

namespace modules\sproutformscustomreportmodule\variables;

use modules\sproutformscustomreportmodule\SproutFormsCustomReportModule;

use Craft;

/**
 * @author    Agency Leroy
 * @package   SproutFormsCustomReportModule
 * @since     1.0.0
 */
class SproutFormsCustomReportModuleVariable
{
    // Public Methods
    // =========================================================================

    /**
     * @param null $optional
     * @return string
     */
    public function exampleVariable($optional = null)
    {
        $result = "And away we go to the Twig template...";
        if ($optional) {
            $result = "I'm feeling optional today...";
        }
        return $result;
    }
}
