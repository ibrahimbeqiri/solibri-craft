# Sprout Forms Custom Report module for Craft CMS 3.x

Custom reporting in the frontend for sprout forms

## Requirements

This module requires Craft CMS 3.0.0-RC1 or later.

## Installation

To install the module, follow these instructions.

First, you'll need to add the contents of the `app.php` file to your `config/app.php` (or just copy it there if it does not exist). This ensures that your module will get loaded for each request. The file might look something like this:
```
return [
    'modules' => [
        'sprout-forms-custom-report-module' => [
            'class' => \modules\sproutformscustomreportmodule\SproutFormsCustomReportModule::class,
            'components' => [
                'sproutFormsCustomReportModuleService' => [
                    'class' => 'modules\sproutformscustomreportmodule\services\SproutFormsCustomReportModuleService',
                ],
            ],
        ],
    ],
    'bootstrap' => ['sprout-forms-custom-report-module'],
];
```
You'll also need to make sure that you add the following to your project's `composer.json` file so that Composer can find your module:

    "autoload": {
        "psr-4": {
          "modules\\sproutformscustomreportmodule\\": "modules/sproutformscustomreportmodule/src/"
        }
    },

After you have added this, you will need to do:

    composer dump-autoload
 
 …from the project’s root directory, to rebuild the Composer autoload map. This will happen automatically any time you do a `composer install` or `composer update` as well.

## Sprout Forms Custom Report Overview

-Insert text here-

## Using Sprout Forms Custom Report

-Insert text here-

## Sprout Forms Custom Report Roadmap

Some things to do, and ideas for potential features:

* Release it

Brought to you by [Agency Leroy](http://agencyleroy.com)
