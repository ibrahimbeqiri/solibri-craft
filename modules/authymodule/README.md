# Authy module for Craft CMS 3.x

Authy Api Endpoints

## Requirements

This module requires Craft CMS 3.0.0-RC1 or later.

## Installation

To install the module, follow these instructions.

First, you'll need to add the contents of the `app.php` file to your `config/app.php` (or just copy it there if it does not exist). This ensures that your module will get loaded for each request. The file might look something like this:

```
return [
    'modules' => [
        'authy-module' => [
            'class' => \modules\authymodule\AuthyModule::class,
            'components' => [
                'authyModuleService' => [
                    'class' => 'modules\authymodule\services\AuthyModuleService',
                ],
            ],
        ],
    ],
    'bootstrap' => ['authy-module'],
];
```

You'll also need to make sure that you add the following to your project's `composer.json` file so that Composer can find your module:

    "autoload": {
        "psr-4": {
          "modules\\authymodule\\": "modules/authymodule/src/"
        }
    },

After you have added this, you will need to do:

    composer dump-autoload

…from the project’s root directory, to rebuild the Composer autoload map. This will happen automatically any time you do a `composer install`.

## Authy Overview

This is a simple module to connect to the Authy API.

## Using Authy

You need to set the AUTHY_API_KEY in your .env file.

Example:
AUTHY_API_KEY="1234567891011121314"

Use directly in twig template (Variables):

Sending a verification code as sms:
{{ craft.authyModule.phoneVerificationStart("+358505429605", "358") }}

Validation verification code:
{{ craft.authyModule.phoneVerificationCheck("+358505429605", "358", "7110") }}

Ajax request urls (Controller actions):

Sending a verification code as sms:
http://your-site.com/actions/authy-module/api/phone-verification-start?phone_number=+358505429605&country_code=358

Validation verification code:
http://your-site.com/actions/authy-module/api/phone-verification-check?phone_number=+358505429605&country_code=358&verification_code=1234

## Authy Roadmap

Some things to do, and ideas for potential features:

* Release it

Brought to you by [Agency Leroy](http://agencyleroy.com)
