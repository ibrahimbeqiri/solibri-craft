<?php
/**
 * Authy module for Craft CMS 3.x
 *
 * Authy Api Endpoints
 *
 * @link      http://agencyleroy.com
 * @copyright Copyright (c) 2018 Agency Leroy
 */

namespace modules\authymodule\services;

use modules\authymodule\AuthyModule;

use Craft;
use craft\base\Component;

/**
 * @author    Agency Leroy
 * @package   AuthyModule
 * @since     1.0.0
 */
class AuthyModuleService extends Component
{

  protected $rest;
  protected $api_url;
  protected $default_options;

  /**
   * Constructor.
   */

  public function __construct()
  {

    $api_key = getenv('AUTHY_API_KEY');
    $api_url = 'https://api.authy.com';

    $client_opts = array(
      'base_uri'      => "{$api_url}/protected/json/",
      'headers'       => array('X-Authy-API-Key' => $api_key),
      'http_errors'   => false
    );

    $this->rest = new \GuzzleHttp\Client($client_opts);
    $this->api_url = $api_url;
    $this->default_options = array('curl' => [CURLOPT_IPRESOLVE => CURL_IPRESOLVE_V4]);
  }


  /**
   * Starts phone verification. (Sends token to user via sms or call).
   *
   * @param string $phone_number User's phone_number stored in your database
   * @param string $country_code User's phone country code stored in your database
   * @param string $via The method the token will be sent to user (sms or call)
   * @param int $code_length The length of the verifcation code to be sent to the user
   *
   * @return server response
   */

  public function phoneVerificationStart($phone_number, $country_code, $via='sms', $code_length=4, $locale=null)
  {

    if (!$phone_number) {

      $phone_validation = array(
        'errors' => "'phone_number' is required",
      );

      return json_encode($phone_validation);
    }

    if (!$country_code) {
      $country_code_validation = array(
        'errors' => "'country_code' is required",
      );

      return json_encode($country_code_validation);
    }

    $query = array(
      'phone_number' => $phone_number,
      'country_code' => $country_code,
      'via'          => $via,
      'code_length'  => $code_length
    );


    if ($locale != null) {
      $query["locale"] = $locale;
    }

    $response = $this->rest->post('phones/verification/start', array_merge(
      $this->default_options,
      array('query' => $query)
    ));

    return $this->parseResponse($response);
  }


  /**
   * Phone verification check. (Checks whether the token entered by the user is valid or not).
   *
   * @param string $phone_number User's phone_number stored in your database
   * @param string $country_code User's phone country code stored in your database
   * @param string $verification_code The verification code entered by the user to be checked
   *
   * @return server response
   */

  public function phoneVerificationCheck($phone_number, $country_code, $verification_code)
  {

    if (!$phone_number) {
      $error = array(
        'errors' => "'phone_number' is required",
      );

      return json_encode($error);
    }

    if (!$country_code) {
      $error = array(
        'errors' => "'country_code' is required",
      );

      return json_encode($error);
    }

    if (!$verification_code) {
      $error = array(
        'errors' => "'verification_code' is required",
      );

      return json_encode($error);
    }

    $response = $this->rest->get('phones/verification/check', array_merge(
      $this->default_options,
      array(
        'query' => array(
          'phone_number'      => $phone_number,
          'country_code'      => $country_code,
          'verification_code' => $verification_code
        )
      )
    ));

    return $this->parseResponse($response);
  }


  /**
   * Parse response
   *
   * @param string $raw_response, the response from the api
   *
   * @return parsed server response
   */

  public function parseResponse($raw_response) {

    $body = (! isset($raw_response->body)) ? json_decode($raw_response->getBody()) : $raw_response->body;
    $errors = new \stdClass();
    // Handle errors
    if (isset($body->errors)) {
        $errors = $body->errors; // when response is {errors: {}}
        unset($body->errors);
    } elseif ($raw_response->getStatusCode() == 400) {
        $errors = $body; // body here is a stdClass
        $body = new \stdClass();
    } elseif ($raw_response->getStatusCode() != 200 && gettype($body) == 'string') {
        // the response was an error so put the body as an error
        $errors = (object) array("error" => $body);
        $body = new \stdClass();
    }

    $response = array(
      'body' => $body,
      'errors' => $errors,
    );

    return json_encode($response);
  }

}
