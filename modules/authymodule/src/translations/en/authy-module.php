<?php
/**
 * Authy module for Craft CMS 3.x
 *
 * Authy Api Endpoints
 *
 * @link      http://agencyleroy.com
 * @copyright Copyright (c) 2018 Agency Leroy
 */

/**
 * @author    Agency Leroy
 * @package   AuthyModule
 * @since     1.0.0
 */
return [
    'Authy plugin loaded' => 'Authy plugin loaded',
];
