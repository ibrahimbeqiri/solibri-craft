<?php
/**
 * Authy module for Craft CMS 3.x
 *
 * Authy Api Endpoints
 *
 * @link      http://agencyleroy.com
 * @copyright Copyright (c) 2018 Agency Leroy
 */

namespace modules\authymodule\variables;

use modules\authymodule\AuthyModule;

use Craft;

/**
 * @author    Agency Leroy
 * @package   AuthyModule
 * @since     1.0.0
 */
class AuthyModuleVariable
{

   /**
     * @param string $phone_number (required)
     * @param string $country_code (required)
     * @param string $via (optional)
     * @param int $code_length (optional)
     * @param string $locale (optional)
     * @return object
     *
     * How you use in twig templates
     * {{ craft.authyModule.phoneVerificationStart("+358505429605", "358") }}
     */
    public function phoneVerificationStart($phone_number, $country_code, $via='sms', $code_length=4, $locale=null)
    {

      $response = AuthyModule::$instance->authyModuleService->phoneVerificationStart(
        $phone_number,
        $country_code,
        $via,
        $code_length,
        $locale
      );

      return $response;
    }

   /**
     * @param string $phone_number (required)
     * @param string $country_code (required)
     * @param string $verification_code (required)
     * @return object
     *
     * How you use in twig templates
     * {{ craft.authyModule.phoneVerificationCheck("+358505429605", "358", "7110") }}
     */
    public function phoneVerificationCheck($phone_number, $country_code, $verification_code)
    {

      $response = AuthyModule::$instance->authyModuleService->phoneVerificationCheck(
        $phone_number,
        $country_code,
        $verification_code
      );

      return $response;
    }
}
