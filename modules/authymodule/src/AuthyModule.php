<?php
/**
 * Authy module for Craft CMS 3.x
 *
 * Authy Api Endpoints
 *
 * @link      http://agencyleroy.com
 * @copyright Copyright (c) 2018 Agency Leroy
 */

namespace modules\authymodule;

use modules\authymodule\services\AuthyModuleService as AuthyModuleServiceService;
use modules\authymodule\variables\AuthyModuleVariable;

use Craft;
use craft\i18n\PhpMessageSource;
use craft\web\UrlManager;
use craft\web\twig\variables\CraftVariable;
use craft\events\RegisterUrlRulesEvent;

use yii\base\Event;
use yii\base\InvalidConfigException;
use yii\base\Module;

/**
 * Class AuthyModule
 *
 * @author    Agency Leroy
 * @package   AuthyModule
 * @since     1.0.0
 *
 * @property  AuthyModuleServiceService $authyModuleService
 */
class AuthyModule extends Module
{
    // Static Properties
    // =========================================================================

    /**
     * @var AuthyModule
     */
    public static $instance;

    // Public Methods
    // =========================================================================

    /**
     * @inheritdoc
     */
    public function __construct($id, $parent = null, array $config = [])
    {
        Craft::setAlias('@modules/authymodule', $this->getBasePath());
        $this->controllerNamespace = 'modules\authymodule\controllers';

        // Translation category
        $i18n = Craft::$app->getI18n();
        /** @noinspection UnSafeIsSetOverArrayInspection */
        if (!isset($i18n->translations[$id]) && !isset($i18n->translations[$id.'*'])) {
            $i18n->translations[$id] = [
                'class' => PhpMessageSource::class,
                'sourceLanguage' => 'en-US',
                'basePath' => '@modules/authymodule/translations',
                'forceTranslation' => true,
                'allowOverrides' => true,
            ];
        }

        // Set this as the global instance of this module class
        static::setInstance($this);

        parent::__construct($id, $parent, $config);
    }

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        self::$instance = $this;

        Event::on(
            UrlManager::class,
            UrlManager::EVENT_REGISTER_SITE_URL_RULES,
            function (RegisterUrlRulesEvent $event) {
                $event->rules['phoneVerificationStart'] = 'modules/authy-module/api/phone-verification-start';
                $event->rules['phoneVerificationCheck'] = 'modules/authy-module/api/phone-verification-check';
            }
        );

        Event::on(
            CraftVariable::class,
            CraftVariable::EVENT_INIT,
            function (Event $event) {
                /** @var CraftVariable $variable */
                $variable = $event->sender;
                $variable->set('authyModule', AuthyModuleVariable::class);
            }
        );

        Craft::info(
            Craft::t(
                'authy-module',
                '{name} module loaded',
                ['name' => 'Authy']
            ),
            __METHOD__
        );
    }

    // Protected Methods
    // =========================================================================
}
