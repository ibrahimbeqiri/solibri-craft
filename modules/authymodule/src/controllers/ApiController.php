<?php
/**
 * Authy module for Craft CMS 3.x
 *
 * Authy Api Endpoints
 *
 * @link      http://agencyleroy.com
 * @copyright Copyright (c) 2018 Agency Leroy
 */

namespace modules\authymodule\controllers;

use modules\authymodule\AuthyModule;

use Craft;
use craft\web\Controller;

/**
 * @author    Agency Leroy
 * @package   AuthyModule
 * @since     1.0.0
 */
class ApiController extends Controller
{

    // Protected Properties
    // =========================================================================

    /**
     * @var    bool|array Allows anonymous access to this controller's actions.
     *         The actions must be in 'kebab-case'
     * @access protected
     */
    protected $allowAnonymous = ['phone-verification-start', 'phone-verification-check'];

    // Public Methods
    // =========================================================================

    /**
     * @return json response from api
     */
    public function actionPhoneVerificationStart()
    {

      $request = Craft::$app->getRequest();

      $phone_number = $request->getParam('phone_number');
      $country_code = $request->getParam('country_code');
      $via = ($request->getParam('via') !== null) ? $request->getParam('via') : 'sms';
      $code_length = ($request->getParam('code_length') !== null) ? $request->getParam('code_length') : 4;
      $locale = $request->getParam('locale');

      $response = AuthyModule::$instance->authyModuleService->phoneVerificationStart(
        $phone_number,
        $country_code,
        $via,
        $code_length,
        $locale
      );

      return $response;
    }

    /**
     * @return json response from api
     */
    public function actionPhoneVerificationCheck()
    {

      $request = Craft::$app->getRequest();

      $phone_number = $request->getParam('phone_number');
      $country_code = $request->getParam('country_code');
      $verification_code = $request->getParam('verification_code');


      $response = AuthyModule::$instance->authyModuleService->phoneVerificationCheck(
        $phone_number,
        $country_code,
        $verification_code
      );

      return $response;
    }
  }
