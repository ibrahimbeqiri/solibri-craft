# Solibri Solution module for Craft CMS 3.x

Solibri Solution API endpoints

## Requirements

This module requires Craft CMS 3.0.0-RC1 or later.

## Installation

To install the module, follow these instructions.

First, you'll need to add the contents of the `app.php` file to your `config/app.php` (or just copy it there if it does not exist). This ensures that your module will get loaded for each request. The file might look something like this:

```
return [
    'modules' => [
        'solibri-solution-module' => [
            'class' => \modules\solibrisolutionmodule\SolibriSolutionModule::class,
            'components' => [
                'solibriSolutionModuleService' => [
                    'class' => 'modules\solibrisolutionmodule\services\SolibriSolutionModuleService',
                ],
            ],
        ],
    ],
    'bootstrap' => ['solibri-solution-module'],
];
```

You'll also need to make sure that you add the following to your project's `composer.json` file so that Composer can find your module:

    "autoload": {
        "psr-4": {
          "modules\\solibrisolutionmodule\\": "modules/solibrisolutionmodule/src/"
        }
    },

After you have added this, you will need to do:

    composer dump-autoload

…from the project’s root directory, to rebuild the Composer autoload map. This will happen automatically any time you do a `composer install`.

## Solibri Solution Overview

This is a simple module to connect to the Solibri Solution API.

## Using Solibri Solution

CHANGE ME
You need to set the SSC_API_AUTH in your .env file.

Example:
SSC_API_AUTH="your_token"

Use directly in twig template (Variables):

Requesting a trial download:
{{ craft.solibriSolutionModule.requestTrial($form_data) }}

Requesting a SMV license:
{{ craft.solibriSolutionModule.requestSMVLicense($form_data) }}

Ajax request urls (Controller actions):

Requesting a trial download:
http://solibri-craft.test/actions/solibri-solution-module/api/request-trial?firstName=Martin&lastName=Stigzelius&email=martin@leroy-creative.com&companyName=Agency Leroy&countryName=Finland
Requesting a SMV license:
http://solibri-craft.test/actions/solibri-solution-module/api/request-smv-license?firstName=Martin&lastName=Stigzelius&email=martin@agencyleroy.com&company=Agency Leroy&areaOfBusiness=Creative&personnel=24&country=Finland&data=feci51UzDkRwEGNtB6FLmc80WCzyd0uaQeIjkeHXhZ_U2FhtkWxSnVUJl8c656h_0LVWyOdqbL6bYeK7AGfS1JqGEq1Pj9hq1IkDtLXq-fBFE0XT_75TgQXJVe6gwqA4UQsWEAcr7rpZ_d-RmKTavdFQb7pn3rGNVamjxathBVY&serviceVersion=2

## Solibri Solution Roadmap

Some things to do, and ideas for potential features:

* Release it

Brought to you by [Agency Leroy](http://agencyleroy.com)
