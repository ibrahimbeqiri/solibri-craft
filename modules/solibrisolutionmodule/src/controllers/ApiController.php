<?php
/**
 * Solibri Solution module for Craft CMS 3.x
 *
 * Solibri Solution API endpoints
 *
 * @link      http://agencyleroy.com
 * @copyright Copyright (c) 2018 Agency Leroy
 */

namespace modules\solibrisolutionmodule\controllers;

use modules\solibrisolutionmodule\SolibriSolutionModule;

use Craft;
use craft\web\Controller;

/**
 * @author    Agency Leroy
 * @package   SolibriSolutionModule
 * @since     1.0.0
 */
class ApiController extends Controller
{

  // Protected Properties
  // =========================================================================

  /**
   * @var    bool|array Allows anonymous access to this controller's actions.
   *         The actions must be in 'kebab-case'
   * @access protected
   */
  protected $allowAnonymous = ['request-trial', 'request-smv-license', 'mailchimp'];

  // Public Methods
  // =========================================================================

  /**
   * @return json response from api
   */
  public function actionRequestTrial()
  {

    $request = Craft::$app->getRequest();
    $form_data = $request->getQueryParams();

    $response = SolibriSolutionModule::$instance->solibriSolutionModuleService->requestTrial($form_data);

    return $response;
  }

  /**
   * @return json response from api
   */
  public function actionRequestSmvLicense()
  {

    $this->requirePostRequest();

    $request = Craft::$app->getRequest();
    $form_data = Craft::$app->request->getBodyParams();

    $response = SolibriSolutionModule::$instance->solibriSolutionModuleService->requestSMVLicense($form_data);

    return $response;
  }

  /**
   * @return json response from api
   */
  public function actionMailchimp()
  {

    $request = Craft::$app->getRequest();
    $form_data = $request->getQueryParams();

    $response = SolibriSolutionModule::$instance->solibriSolutionModuleService->mailchimp($form_data);

    return $response;
  }
}
