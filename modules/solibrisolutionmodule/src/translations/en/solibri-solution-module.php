<?php
/**
 * Solibri Solution module for Craft CMS 3.x
 *
 * Solibri Solution API endpoints
 *
 * @link      http://agencyleroy.com
 * @copyright Copyright (c) 2018 Agency Leroy
 */

/**
 * @author    Agency Leroy
 * @package   SolibriSolutionModule
 * @since     1.0.0
 */
return [
    'Solibri Solution plugin loaded' => 'Solibri Solution plugin loaded',
];
