<?php
/**
 * Solibri Solution module for Craft CMS 3.x
 *
 * Solibri Solution API endpoints
 *
 * @link      http://agencyleroy.com
 * @copyright Copyright (c) 2018 Agency Leroy
 */

namespace modules\solibrisolutionmodule\services;

use modules\solibrisolutionmodule\SolibriSolutionModule;

use Craft;
use craft\base\Component;
use craft\mail\Message;

/**
 * @author    Agency Leroy
 * @package   SolibriSolutionModule
 * @since     1.0.0
 */
class SolibriSolutionModuleService extends Component
{

  protected $rest;
  protected $api_url;
  protected $default_options;

  /**
   * Constructor.
   */

  public function __construct()
  {

    $api_url = Craft::$app->config->general->sscApiUrl;
    
    $api_key = getenv('SSC_API_AUTH');

    $client_opts = array(
      'base_uri'      => $api_url,
      'headers'       => array('Authorization' => $api_key),
      'http_errors'   => false
    );

    $this->rest = new \GuzzleHttp\Client($client_opts);
    $this->api_url = $api_url;
    $this->default_options = array('curl' => [CURLOPT_IPRESOLVE => CURL_IPRESOLVE_V4]);
  }

  /**
   * Request SSC Token
   *
   * @return server response
   */

  public function requestSSCToken()
  {


    $response = $this->rest->get('/services/public/secure/token', $this->default_options);

    return $this->parseResponse($response);
  }

  /**
   * Request Trial
   * @param object $form_data (required)
   *
   * @return server response
   */

  public function requestTrial($form_data)
  {

    $getToken = $this->requestSSCToken();

    if ($getToken->body->statusCode !== '0') {
      return $getToken;
    }

    $token = $getToken->body->token;

    if (!isset($form_data['email'])) {
      $error = array(
        'errors' => "'email' is required",
      );

      return json_encode($error);
    }

    if (!isset($form_data['firstName'])) {
      $error = array(
        'errors' => "'firstName' is required",
      );

      return json_encode($error);
    }

    if (!isset($form_data['lastName'])) {
      $error = array(
        'errors' => "'lastName' is required",
      );

      return json_encode($error);
    }

    if (!isset($form_data['companyName'])) {
      $error = array(
        'errors' => "'companyName' is required",
      );

      return json_encode($error);
    }

    if (!isset($form_data['countryName'])) {
      $error = array(
        'errors' => "'countryName' is required",
      );

      return json_encode($error);
    }

    $query = array(
      'email' => $form_data['email'],
      'firstName' => $form_data['firstName'],
      'lastName' => $form_data['lastName'],
      'companyName' => $form_data['companyName'],
      'countryName' => $form_data['countryName']
    );

    $response = $this->rest->post("/services/public/trial/user/v2/token/{$token}", [
      'headers' => ['Content-type' => 'application/json'],
      'body' => json_encode($query)
    ]);

    $parsedResponse = $this->parseResponse($response, false);
    $hasPasswordUrl = false;

    if (!empty($parsedResponse->body->passwordUrl)) {
      $hasPasswordUrl = true;
    }

    if ($hasPasswordUrl) {
      $link = $parsedResponse->body->passwordUrl;
      $language = $form_data['language'];
      $firstname = $form_data['firstName'];
      $message = $this->buildTrialDownloadMessage($firstname, $link, $language);
      $email = $form_data['email'];
      $subject = 'Thank you for your interest in Solibri Model Checker';

      $mail = $this->sendMail($email, $message, $subject);
    }

    return json_encode($parsedResponse);
  }


  /**
   * Request SMV License
   * @param object $form_data (required)
   *
   * @return server response
   */

  public function requestSMVLicense($form_data)
  {


    $getToken = $this->requestSSCToken();

    if ($getToken->body->statusCode !== '0') {
      return $getToken;
    }

    $token = $getToken->body->token;

    //Required
    if (!isset($form_data['firstName'])) {
      $error = array(
        'errors' => "'firstName' is required",
      );

      return json_encode($error);
    }

    //Required
    if (!isset($form_data['lastName'])) {
      $error = array(
        'errors' => "'lastName' is required",
      );

      return json_encode($error);
    }

    //Required
    if (!isset($form_data['email'])) {
      $error = array(
        'errors' => "'email' is required",
      );

      return json_encode($error);
    }

    //Optional
    if (!isset($form_data['phone'])) {
      $form_data["phone"] = '';
    }

    //Required
    if (!isset($form_data['company'])) {
      $error = array(
        'errors' => "'company' is required",
      );

      return json_encode($error);
    }

    //Required
    if (!isset($form_data['areaOfBusiness'])) {
      $error = array(
        'errors' => "'areaOfBusiness' is required",
      );

      return json_encode($error);
    }

    //Required
    if (!isset($form_data['personnel'])) {
      $error = array(
        'errors' => "'personnel' is required",
      );

      return json_encode($error);
    }

    //Required
    if (!isset($form_data['country'])) {
      $error = array(
        'errors' => "'country' is required",
      );

      return json_encode($error);
    }

    //Optional
    if (!isset($form_data['state'])) {
      $form_data["state"] = '';
    }

    //Optional
    if (!isset($form_data['city'])) {
      $form_data["city"] = '';
    }

    //Optional
    if (!isset($form_data['website'])) {
      $form_data["website"] = '';
    }

    //Optional
    if (!isset($form_data['newsletter'])) {
      $form_data["newsletter"] = 'false';
    }

    //Required
    if (!isset($form_data['data'])) {
      $error = array(
        'errors' => "'data' is required",
      );

      return json_encode($error);
    }


    $query = array(
      'firstName' => $form_data['firstName'],
      'lastName' => $form_data['lastName'],
      'email' => $form_data['email'],
      'phone' => $form_data['phone'],
      'company' => $form_data['company'],
      'areaOfBusiness' => $form_data['areaOfBusiness'],
      'personnel' => $form_data['personnel'],
      'country' => $form_data['country'],
      'state' => $form_data['state'],
      'city' => $form_data['city'],
      'website' => $form_data['website'],
      'application' => "SMV",
      'data' => $form_data['data']
    );

    if (isset($form_data['serviceVersion'])) {
      $url = '/services/public/client/registration/safe/v2/token/';
    } else {
      $url = '/services/public/client/registration/safe/token/';
    }


    $response = $this->rest->post("{$url}{$token}", [
      'headers' => ['Content-type' => 'application/json'],
      'body' => json_encode($query)
    ]);

    $parsedResponse = $this->parseResponse($response, false);

    if (isset($parsedResponse->body) && !empty($parsedResponse->body)) {
      if (isset($parsedResponse->body->licenseKey) && !empty($parsedResponse->body->licenseKey)) {

        $licenseKey = $parsedResponse->body->licenseKey;
        $message = $this->buildLicenseMessage($licenseKey);
        $email = $form_data['email'];
        $subject = 'Thank you for your interest in Solibri Model Viewer';

        $mail = $this->sendMail($email, $message, $subject);

      }

    }

    return json_encode($parsedResponse);
  }


  /**
   * Request Trial
   * @param object $form_data (required)
   *
   * @return server response
   */

  public function mailchimp($form_data)
  {

      //Required
      if (!isset($form_data['email'])) {
        $error = array(
          'errors' => "'email' is required",
        );

        return json_encode($error);
      }
      //Optional
      if (!isset($form_data['firstname'])) {
        $form_data["firstname"] = 'false';
      }
      //Optional
      if (!isset($form_data['lastname'])) {
        $form_data["lastname"] = 'false';
      }

      $query = (object) [
        'email_address' => $form_data['email'],
        'merge_fields' => [
          'FNAME' => $form_data['firstname'],
          'LNAME' => $form_data['lastname']
        ],
        'status' => 'subscribed',
      ];

      $curl = curl_init();

      curl_setopt_array($curl, array(
        CURLOPT_URL => "https://us9.api.mailchimp.com/3.0/lists/3ab6fd6cbb/members/",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => json_encode($query),
        CURLOPT_HTTPHEADER => array(
          "Authorization: Basic YW55c3RyaW5nOmVmMzgzNjQ2NmVkY2RkZTU2ZjYwOWQ3ZjA4OTQ5OGYxLXVzOQ==",
          "Cache-Control: no-cache",
          "Content-Type: application/json",
          "Postman-Token: fe6fe1a1-899c-494b-9f6c-5356f1d42574"
        ),
      ));

      $response = curl_exec($curl);
      $err = curl_error($curl);

      curl_close($curl);

      if ($err) {
        return "cURL Error #:" . $err;
      } else {
        return $response;
      }
  }

  /**
   * Parse response
   *
   * @param string $raw_data
   *
   * @return server response
   */

  public function parseResponse($raw_response, $string = false) {

    $body = (! isset($raw_response->body)) ? json_decode($raw_response->getBody()) : $raw_response->body;
    $errors = new \stdClass();

    // Handle errors
    if (isset($body->errors)) {
        $errors = $body->errors; // when response is {errors: {}}
        unset($body->errors);

    } elseif ($raw_response->getStatusCode() == 400) {
        $errors = $body; // body here is a stdClass
        $body = new \stdClass();

    } elseif ($raw_response->getStatusCode() != 200 && gettype($body) == 'string') {
        // the response was an error so put the body as an error
        $errors = $body;
        $body = new \stdClass();

    } elseif ($raw_response->getStatusCode() != 200 && gettype($body) != 'string') {
      $errors = (object) array(
        "statusCode" => $raw_response->getStatusCode(),
        "statusMessage" => $raw_response->getReasonPhrase()
      );
      $body = new \stdClass();
    } elseif (isset($body->statusCode) && $body->statusCode != 0) {
      $errors = $body;
      $body = new \stdClass();
    }

    $response = (object) array(
      'body' => $body,
      'errors' => $errors,
    );

    if ($string) {
      $response = json_encode($response);
    }

    return $response;
  }

  /**
   * @param $html
   * @param $subject
   * @param null $mail
   * @param array $attachments
   * @return bool
   */
  private function sendMail($email, $html, $subject): bool
  {
      $settings = Craft::$app->systemSettings->getSettings('email');
      $message = new Message();

      $message->setFrom([$settings['fromEmail'] => $settings['fromName']]);
      $message->setTo($email);
      $message->setSubject($subject);
      $message->setHtmlBody($html);

      return Craft::$app->mailer->send($message);
  }

  public function buildLicenseMessage($smvLicenseKey) {
  $year = date("Y");

  $message = <<<EOD
  <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
   <html xmlns="http://www.w3.org/1999/xhtml">
       <head>
       <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
       <title>Thank You</title>
       <style type="text/css">
               /* ///////// CLIENT-SPECIFIC STYLES ///////// */
               #outlook a{padding:0;} /* Force Outlook to provide a "view in browser" message */
               .ReadMsgBody{width:100%;} .ExternalClass{ width:100%; } /* Force Hotmail to display emails at full width */
               .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;} /* Force Hotmail to display normal line spacing */
               body, table, td, p, a, li, blockquote{-webkit-text-size-adjust:100%; -ms-text-size-adjust:100%;} /* Prevent WebKit and Windows mobile changing default text sizes */
               table, td{mso-table-lspace:0pt; mso-table-rspace:0pt;} /* Remove spacing between tables in Outlook 2007 and up */
               img{-ms-interpolation-mode:bicubic;} /* Allow smoother rendering of resized image in Internet Explorer */
               /* ///////// RESET STYLES ///////// */
               body{margin:0; padding:0;background: #f2f2f2;}
               img{border:0; height:auto; line-height:100%; outline:none; text-decoration:none;}
               table{border-collapse:collapse !important;position: relative;}
               body, #bodyTable, #bodyCell{ margin:0; padding:0; width:100% !important; }
               /* ///////// TEMPLATE STYLES ///////// */
               /* ========== Page Styles ========== */

         center {
           display: none !important;
         }

         .helveticaRegular {
           font-family: 'Helvetica Neue', helvetica, arial, sans-serif;
           font-weight: normal;
         }

         .helveticaBold {
           font-family: 'Helvetica Neue', helvetica, arial, sans-serif;
           font-weight: bold;
         }

         .container {
           width: 600px;
           margin: auto;
         }
               #bodyTable{
                   background: #f2f2f2;

               }
               /* ========== Header Styles ========== */

         #templateHeaderBorder{
           border-top: 10px solid #000000;
           width: 600px;
         }

         #templateHeader {
           width: 100%;
           margin: 10px 0 10px 0;
           text-align: center;
         }

         .preheader_content_left {
           text-align: left;
         }
         #logo {
           width: 80px;
           padding: 0 10px 0 20px;
         }

         .preheader_content_center {
           text-align: center;
         }
         .preheader_content_center p {
           font-family: 'Helvetica Neue', helvetica, arial, sans-serif;
           font-weight: normal;
           color: #a8a8a8;
           font-size: 10px;
           padding: 0px 20px;
           margin: 0;
         }
         .preheader_content_center img {
           width: 150px;
           margin: auto;
         }
           .preheader_content_left {
             text-align: left;
             font-family: 'Helvetica Neue', helvetica, arial, sans-serif;
               font-weight: normal;
               color: #a8a8a8;
               font-size: 10px;
         }
         .preheader_content_right {
           text-align: right;
           font-family: 'Helvetica Neue', helvetica, arial, sans-serif;
           font-weight: normal;
           color: #a8a8a8;
           font-size: 10px;
         }
         .preheader_content_right a {
           color: #a8a8a8;
         }

               /* ========== Body Styles ========== */

         .sectionContainer {
           padding: 0px;
           width: 100%;
         }

         h1 {
           font-family: 'Helvetica Neue', helvetica, arial, sans-serif;
           font-weight: normal;
           color: #929292;
           font-size: 40px;
           margin: 50px 0 30px 0;
         }

         p {
           font-family: 'Helvetica Neue', helvetica, arial, sans-serif;
           font-weight: normal;
           font-size: 14px;
           line-height: 1.8em;
           color: #929292;
           padding: 10px 0;
         }

         .button {
           width: 100%;
           text-align: center;
         }
         .button a {
           text-decoration: none;
           font-family: 'Helvetica Neue', helvetica, arial, sans-serif;
           font-weight: bold;
           color: #363D40;
           text-transform: uppercase;
           font-size: 12px;
         }

               /* ========== Column Styles ========== */


               /* ========== Footer Styles ========== */

               #templateFooter{
           width: 100%;
                   background: #ffffff;
               }
         #templateFooterSocial {
           width: 100%;
           background: #ffffff;
           text-align: center;
         }
         #templateFooterSocial a {
           padding: 10px 15px;
         }
         #templateFooterSocial img {
           width: 30px;
         }
         .footerText {
           font-family: 'Helvetica Neue', helvetica, arial, sans-serif;
           font-weight: normal;
           color: #808080;
           font-size: 10px;
         }
         .footerText a {
           color: #808080;
         }

         /* Outlook First */

         body.outlook p {
           display: inline !important;
         }

               /* ///////// MOBILE STYLES ///////// */

         @media only screen and (max-width: 640px){
                   /* /////// CLIENT-SPECIFIC MOBILE STYLES /////// */
                   body, table, td, p, a, li, blockquote{-webkit-text-size-adjust:none !important;} /* Prevent Webkit platforms from changing default text sizes */
           body{width:100% !important; min-width:100% !important; max-width: 100% !important;} /* Prevent iOS Mail from adding padding to the body */

                   /* /////// MOBILE TEMPLATE STYLES /////// */

                   /* ======== Page Styles ======== */

           .container {
             width: 100% !important;
           }
           #templateHeader {
             margin: 20px 0 40px 0 !important;
           }
           #templateHeaderBorder{
             width: 100% !important;
           }

           #templateBody {
             margin-top: 0px !important;
           }

           .preheader_content_right {
             text-align: right !important;
           }

           .preheader_content_left {
             text-align: left !important;
             padding: 0px !important;
             width: 100% !important;
           }

           #logo {
             width: 120px !important;
             margin: auto !important;
           }

           h1 {
             font-size: 36px !important;
             margin: 30px 0 30px 0 !important;
           }

           p {
             font-size: 15px !important;
           }

           .sectionLabel {
             font-size: 15px !important;
           }

           .sectionImage {
             width: 100% !important;
           }

           .column {
             display: block !important;
             width: 100% !important;
             text-align: center !important;
           }
           .columnImage {
             width: 100% !important;
             margin: auto;
           }

           .responsive {
             width: 100% !important;
           }

           #templateFooterSocial a {
             padding: 10px 5px !important;
           }

           #templateFooterSocial img {
             width: 30px !important;
           }
               }
           </style>

   <style type="text/css">

   </style>
   </head>
       <body leftmargin="0" marginwidth="0" align="center" topmargin="0" marginheight="0" offset="0" width="100%" style="background: #FFFFFF;width: 100%;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;margin: 0;padding: 0;">
         <!-- BEGIN TEMPLATE // -->
         <table align="center" valign="top" border="0" cellpadding="0" cellspacing="0" width="100%" id="bodyTable" style="background: #ffffff;width: 100%;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;position: relative;margin: 0;padding: 0;border-collapse: collapse !important;">
           <tr>
             <td align="center" valign="top" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
               <!-- BEGIN PREHEADER // -->
               <!--
        <table border="0" cellpadding="10" cellspacing="0" id="templateHeader" width="100%" align="left" style="width: 100%;background: #ffffff;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;position: relative;margin: 10px 0 10px 0;text-align: center;border-collapse: collapse !important;">
                 <tr>
                   <td align="left" valign="top" width="60%" class="column preheader_content_left responsive preheaderContent" mc:edit="preheader_content00" style="color: #a8a8a8;font-family: 'Helvetica Neue', helvetica, arial, sans-serif;font-weight: normal;font-size: 10px;padding: 10px 0 10px 20px;margin: 0;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;text-align: left;">
                   Thank you for registering
                   </td>
                   <td align="right" valign="top" width="40%" style="color: #a8a8a8;font-family: 'Helvetica Neue', helvetica, arial, sans-serif;font-weight: normal;font-size: 10px;padding: 10px 20px 10px 0;margin: 0;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;text-align: right;" class="preheader_content_right column" mc:edit="preheader_content01">
                   </td>
                 </tr>
               </table>
        -->
               <!-- // END PREHEADER -->
             </td>
           </tr>
           <tr>
             <td align="center" valign="top" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
             <!-- BEGIN BODY // -->
               <table border="0" cellpadding="0" cellspacing="10" id="templateBody" width="100%" align="center" style="background: #FFFFFF;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;position: relative;border-collapse: collapse !important;">
                   <tr>
                     <td align="center" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                       <table border="0" cellpadding="20" cellspacing="0" width="100" style="width: 100px;margin: auto;background: #FFFFFF;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;position: relative;border-collapse: collapse !important;">
                         <tr>
                           <td align="center" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                             <img class="sectionImage responsive" width="100" style="width: 100px;margin: auto;-ms-interpolation-mode: bicubic;border: 0;height: auto;line-height: 100%;outline: none;text-decoration: none;" src="https://solibri-assets.s3.amazonaws.com/old-site/newsletter_images/logo.jpg" alt="Section Label">
                           </td>
                         </tr>
                       </table>
                     </td>
                   </tr>
                 <tr>
                   <td align="center" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                     <div mc:repeatable="section" mc:variant="Section Normal">
                       <table border="0" cellpadding="0" cellspacing="0" width="100%" class="sectionContainer" style="width: 100%;background: #ffffff;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;position: relative;padding: 0px;border-collapse: collapse !important;">
                         <tr>
                           <td align="center" style="padding: 0px 0px 30px 0px;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                             <table border="0" cellpadding="0" cellspacing="0" width="600" align="center" class="section container responsive" style="width: 600px;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;position: relative;margin: auto;border-collapse: collapse !important;">
                               <tr>
                                 <td mc:edit="sectionBody" class="sectionBody responsive" width="600" style="width: 600px;text-align: center;padding: 10px 20px 0 20px;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;" align="center">
                                   <p style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: 'Helvetica Neue', helvetica, arial, sans-serif;font-weight: normal;font-size: 14px;line-height: 1.8em;color: #929292;padding: 10px 0 0 0;"><span style="color: #2B2C2C;">Thank you for registering Solibri Model Viewer.</span><br>Enter the license key below and get ready to enjoy your new product without delay.<br><br>Your license key is:</p>
                                 </td>
                               </tr>
                               <tr>
                                 <td align="center" style="font-family: monospace;padding: 15px 30px;border-radius: 2px;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt; background-color: whitesmoke; font-size: 14px; color: #2B2C2C;">
                                  $smvLicenseKey
                                 </td>
                               </tr>
                             </table>
                           </td>
                         </tr>
                       </table>
                     </div>
                   </td>
                  </tr>
               </table>
             <!-- // END BODY -->
             </td>
           </tr>
           <tr>
             <td align="center" valign="middle" width="100%" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
               <!-- BEGIN FOOTER // -->
               <table border="0" cellpadding="0" width="100%" cellspacing="0" id="templateFooterSocial" style="width: 100%;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;position: relative;background: #ffffff;text-align: center;border-collapse: collapse !important;">
                 <tr>
                   <td valign="middle" align="center" width="100%" style="text-align: center;padding: 20px 0;width: 100%;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                     <div mc:edit="footer_content00">
                       <a href="https://www.facebook.com/solibri" style="padding: 10px 15px;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;"><img src="https://solibri-assets.s3.amazonaws.com/old-site/newsletter_images/facebook.jpg" style="width: 30px;-ms-interpolation-mode: bicubic;border: 0;height: auto;line-height: 100%;outline: none;text-decoration: none;" width="30" height="30" alt="Facebook"></a>
                       <a href="https://twitter.com/solibri" style="padding: 10px 15px;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;"><img src="https://solibri-assets.s3.amazonaws.com/old-site/newsletter_images/twitter.jpg" style="width: 30px;-ms-interpolation-mode: bicubic;border: 0;height: auto;line-height: 100%;outline: none;text-decoration: none;" width="30" height="30" alt="Twitter"></a>
                       <a href="mailto:info@solibri.com" style="padding: 10px 15px;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;"><img src="https://solibri-assets.s3.amazonaws.com/old-site/newsletter_images/mail.jpg" style="width: 30px;-ms-interpolation-mode: bicubic;border: 0;height: auto;line-height: 100%;outline: none;text-decoration: none;" width="30" height="30" alt="Mail"></a>
                     </div>
                   </td>
                 </tr>
               </table>
               <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateFooter" style="width: 100%;background-color: #666666;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;position: relative;background: #ffffff;border-collapse: collapse !important;">
                 <tr>
                   <td valign="top" align="center" width="100%" style="text-align: center;padding: 15px 0;width: 100%;color: #CBCBCB;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;font-family: 'Helvetica Neue', helvetica, arial, sans-serif;font-weight: normal;font-size: 10px;" class="footerText">
                     <div mc:edit="footer_content01">
                     <em>Copyright © <a href="http://www.solibri.com/" style="color: #CBCBCB;">$year Solibri Inc.</a>, All rights reserved.</em>                     </div>
                   </td>
                 </tr>
               </table>
               <!-- // END FOOTER -->
             </td>
           </tr>
         </table>
         <!-- // END TEMPLATE -->
       </body>
   </html>
EOD;

  return trim($message);

}

function buildTrialDownloadMessage($firstname, $link, $language) {

  if ($language == "de") {
    $message = <<<EOD
<!DOCTYPE html>
 <html>
     <head>
         <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
   <style type="text/css">
    p { font-family:arial; }
   </style>
     </head>
     <body>
         <p>Sehr geehrte(r) $firstname,</p>
   <p>Sie haben sich als Testbenutzer am Solibri Solution Center registriert. Ihr Konto steht Ihnen jetzt zur Verwendung bereit. Klicken Sie auf den folgenden Link (oder kopieren Sie ihn in Ihren Browser), um Ihr Kennwort festzulegen: <a href="$link">$link</a>.</p>
   <p>Sobald Sie Ihr Kennwort festgelegt haben, können Sie verfügbare Testversionen aus dem Solibri Solution Center herunterladen.</p>
   <p>Mit freundlichen Grüßen</p>
   <p>Ihr Solibri Solution Center Team</p>
     </body>
 </html>
EOD;

  } else if ($language == "ja") {
    $message = <<<EOD
<!DOCTYPE html>
 <html>
     <head>
         <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
   <style type="text/css">
    p { font-family:arial; }
   </style>
     </head>
     <body>
         <p>$firstname 様</p>
   <p>お客様はSolibri Solution Centerにトライアルユーザとして登録されました。お客様のアカウントは現在ご利用可能です。リンク <a href="$link">$link</a> をクリック（またはブラウザのウィンドウにコピー&ペースト）してパスワードを設定してください。</p>
   <p>パスワードを設定後、Solibri Solution Centerからトライアル版のソフトウェアをダウンロードできます。</p>
   <p>敬具</p>
   <p>Solibri Solution Centerチーム</p>
     </body>
 </html>
EOD;

  } else {

  $message = <<<EOD
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
 <html xmlns="http://www.w3.org/1999/xhtml">
     <head>
     <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
     <title>Thank You</title>
     <style type="text/css">
             /* ///////// CLIENT-SPECIFIC STYLES ///////// */
             #outlook a{padding:0;} /* Force Outlook to provide a "view in browser" message */
             .ReadMsgBody{width:100%;} .ExternalClass{ width:100%; } /* Force Hotmail to display emails at full width */
             .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;} /* Force Hotmail to display normal line spacing */
             body, table, td, p, a, li, blockquote{-webkit-text-size-adjust:100%; -ms-text-size-adjust:100%;} /* Prevent WebKit and Windows mobile changing default text sizes */
             table, td{mso-table-lspace:0pt; mso-table-rspace:0pt;} /* Remove spacing between tables in Outlook 2007 and up */
             img{-ms-interpolation-mode:bicubic;} /* Allow smoother rendering of resized image in Internet Explorer */
             /* ///////// RESET STYLES ///////// */
             body{margin:0; padding:0;background: #f2f2f2;}
             img{border:0; height:auto; line-height:100%; outline:none; text-decoration:none;}
             table{border-collapse:collapse !important;position: relative;}
             body, #bodyTable, #bodyCell{ margin:0; padding:0; width:100% !important; }
             /* ///////// TEMPLATE STYLES ///////// */
             /* ========== Page Styles ========== */

       center {
         display: none !important;
       }

       .helveticaRegular {
         font-family: 'Helvetica Neue', helvetica, arial, sans-serif;
         font-weight: normal;
       }

       .helveticaBold {
         font-family: 'Helvetica Neue', helvetica, arial, sans-serif;
         font-weight: bold;
       }

       .container {
         width: 600px;
         margin: auto;
       }
             #bodyTable{
                 background: #f2f2f2;

             }
             /* ========== Header Styles ========== */

       #templateHeaderBorder{
         border-top: 10px solid #000000;
         width: 600px;
       }

       #templateHeader {
         width: 100%;
         margin: 10px 0 10px 0;
         text-align: center;
       }

       .preheader_content_left {
         text-align: left;
       }
       #logo {
         width: 80px;
         padding: 0 10px 0 20px;
       }

       .preheader_content_center {
         text-align: center;
       }
       .preheader_content_center p {
         font-family: 'Helvetica Neue', helvetica, arial, sans-serif;
         font-weight: normal;
         color: #a8a8a8;
         font-size: 10px;
         padding: 0px 20px;
         margin: 0;
       }
       .preheader_content_center img {
         width: 150px;
         margin: auto;
       }
         .preheader_content_left {
           text-align: left;
           font-family: 'Helvetica Neue', helvetica, arial, sans-serif;
             font-weight: normal;
             color: #a8a8a8;
             font-size: 10px;
       }
       .preheader_content_right {
         text-align: right;
         font-family: 'Helvetica Neue', helvetica, arial, sans-serif;
         font-weight: normal;
         color: #a8a8a8;
         font-size: 10px;
       }
       .preheader_content_right a {
         color: #a8a8a8;
       }

             /* ========== Body Styles ========== */

       .sectionContainer {
         padding: 0px;
         width: 100%;
       }

       h1 {
         font-family: 'Helvetica Neue', helvetica, arial, sans-serif;
         font-weight: normal;
         color: #929292;
         font-size: 40px;
         margin: 50px 0 30px 0;
       }

       p {
         font-family: 'Helvetica Neue', helvetica, arial, sans-serif;
         font-weight: normal;
         font-size: 14px;
         line-height: 1.8em;
         color: #929292;
         padding: 10px 0;
       }

       .button {
         width: 100%;
         text-align: center;
       }
       .button a {
         text-decoration: none;
         font-family: 'Helvetica Neue', helvetica, arial, sans-serif;
         font-weight: bold;
         color: #363D40;
         text-transform: uppercase;
         font-size: 12px;
       }

             /* ========== Column Styles ========== */


             /* ========== Footer Styles ========== */

             #templateFooter{
         width: 100%;
                 background: #ffffff;
             }
       #templateFooterSocial {
         width: 100%;
         background: #ffffff;
         text-align: center;
       }
       #templateFooterSocial a {
         padding: 10px 15px;
       }
       #templateFooterSocial img {
         width: 30px;
       }
       .footerText {
         font-family: 'Helvetica Neue', helvetica, arial, sans-serif;
         font-weight: normal;
         color: #808080;
         font-size: 10px;
       }
       .footerText a {
         color: #808080;
       }

       /* Outlook First */

       body.outlook p {
         display: inline !important;
       }

             /* ///////// MOBILE STYLES ///////// */

       @media only screen and (max-width: 640px){
                 /* /////// CLIENT-SPECIFIC MOBILE STYLES /////// */
                 body, table, td, p, a, li, blockquote{-webkit-text-size-adjust:none !important;} /* Prevent Webkit platforms from changing default text sizes */
         body{width:100% !important; min-width:100% !important; max-width: 100% !important;} /* Prevent iOS Mail from adding padding to the body */

                 /* /////// MOBILE TEMPLATE STYLES /////// */

                 /* ======== Page Styles ======== */

         .container {
           width: 100% !important;
         }
         #templateHeader {
           margin: 20px 0 40px 0 !important;
         }
         #templateHeaderBorder{
           width: 100% !important;
         }

         #templateBody {
           margin-top: 0px !important;
         }

         .preheader_content_right {
           text-align: right !important;
         }

         .preheader_content_left {
           text-align: left !important;
           padding: 0px !important;
           width: 100% !important;
         }

         #logo {
           width: 120px !important;
           margin: auto !important;
         }

         h1 {
           font-size: 36px !important;
           margin: 30px 0 30px 0 !important;
         }

         p {
           font-size: 15px !important;
         }

         .sectionLabel {
           font-size: 15px !important;
         }

         .sectionImage {
           width: 100% !important;
         }

         .column {
           display: block !important;
           width: 100% !important;
           text-align: center !important;
         }
         .columnImage {
           width: 100% !important;
           margin: auto;
         }

         .responsive {
           width: 100% !important;
         }

         #templateFooterSocial a {
           padding: 10px 5px !important;
         }

         #templateFooterSocial img {
           width: 30px !important;
         }
             }
         </style>

 <style type="text/css">

 </style>
 </head>
     <body leftmargin="0" marginwidth="0" align="center" topmargin="0" marginheight="0" offset="0" width="100%" style="background: #FFFFFF;width: 100%;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;margin: 0;padding: 0;">
       <!-- BEGIN TEMPLATE // -->
       <table align="center" valign="top" border="0" cellpadding="0" cellspacing="0" width="100%" id="bodyTable" style="background: #ffffff;width: 100%;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;position: relative;margin: 0;padding: 0;border-collapse: collapse !important;">
         <tr>
           <td align="center" valign="top" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
             <!-- BEGIN PREHEADER // -->
             <!--
      <table border="0" cellpadding="10" cellspacing="0" id="templateHeader" width="100%" align="left" style="width: 100%;background: #ffffff;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;position: relative;margin: 10px 0 10px 0;text-align: center;border-collapse: collapse !important;">
               <tr>
                 <td align="left" valign="top" width="60%" class="column preheader_content_left responsive preheaderContent" mc:edit="preheader_content00" style="color: #a8a8a8;font-family: 'Helvetica Neue', helvetica, arial, sans-serif;font-weight: normal;font-size: 10px;padding: 10px 0 10px 20px;margin: 0;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;text-align: left;">
                   Thank you for downloading the trial version of Solibri Model Checker (SMC). To help you discover its incredible potential, we’ve put together the perfect support package. No-nonsense product information. Short instructional films on software features. Useful customer insight. Free weekly training webinars.
                 </td>
                 <td align="right" valign="top" width="40%" style="color: #a8a8a8;font-family: 'Helvetica Neue', helvetica, arial, sans-serif;font-weight: normal;font-size: 10px;padding: 10px 20px 10px 0;margin: 0;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;text-align: right;" class="preheader_content_right column" mc:edit="preheader_content01">
                 </td>
               </tr>
             </table>
      -->
             <!-- // END PREHEADER -->
           </td>
         </tr>
         <tr>
           <td align="center" valign="top" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
           <!-- BEGIN BODY // -->
             <table border="0" cellpadding="0" cellspacing="10" id="templateBody" width="100%" align="center" style="background: #FFFFFF;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;position: relative;border-collapse: collapse !important;">
                 <tr>
                   <td align="center" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                     <table border="0" cellpadding="20" cellspacing="0" width="100" style="width: 100px;margin: auto;background: #FFFFFF;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;position: relative;border-collapse: collapse !important;">
                       <tr>
                         <td align="center" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                           <img class="sectionImage responsive" width="100" style="width: 100px;margin: auto;-ms-interpolation-mode: bicubic;border: 0;height: auto;line-height: 100%;outline: none;text-decoration: none;" src="https://solibri-assets.s3.amazonaws.com/old-site/newsletter_images/logo.jpg" alt="Section Label">
                         </td>
                       </tr>
                     </table>
                   </td>
                 </tr>
               <tr>
                 <td align="center" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                   <div mc:repeatable="section" mc:variant="Section Normal">
                     <table border="0" cellpadding="0" cellspacing="0" width="100%" class="sectionContainer" style="width: 100%;background: #ffffff;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;position: relative;padding: 0px;border-collapse: collapse !important;">
                       <tr>
                         <td align="center" style="padding: 0px 0px 30px 0px;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                           <table border="0" cellpadding="0" cellspacing="0" width="600" align="center" class="section container responsive" style="width: 600px;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;position: relative;margin: auto;border-collapse: collapse !important;">
                             <tr>
                               <td align="center" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                                 <img mc:edit="sectionImage" class="sectionImage responsive" width="600" style="width: 400px;margin: 20px auto;-ms-interpolation-mode: bicubic;border: 0;height: auto;line-height: 100%;outline: none;text-decoration: none;" src="https://solibri-assets.s3.amazonaws.com/old-site/newsletter_images/thank_you_solibri.png" alt="Section Label">
                               </td>
                             </tr>
                             <tr>
                               <td mc:edit="sectionBody" class="sectionBody responsive" width="600" style="width: 600px;text-align: center;padding: 10px 20px;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;" align="center">
                                 <p style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: 'Helvetica Neue', helvetica, arial, sans-serif;font-weight: normal;font-size: 14px;line-height: 1.8em;color: #929292;padding: 10px 0;">Thank you for registering for the trial version of Solibri Model Checker.<br><br>Click on the below button to enter Solibri Solution Center and create your password. You will then be able to download your Solibri Model Checker trial.*<br><br><small>*Confirm your account details by choosing a new password. You can then download Solibri Model Checker. (The password must be 8 characters in length and contain at least one uppercase and lowercase letter as well as one number</small>)</p>
                                 <p style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: 'Helvetica Neue', helvetica, arial, sans-serif;font-weight: normal;font-size: 14px;line-height: 1.8em;color: #929292;padding: 10px 0;">The SSC set password screen is pictured below:</p>
                                 <a href="$link"><img style="width: 300px;margin: 0 auto;border: 1px solid grey;" src="https://solibri-assets.s3.amazonaws.com/old-site/newsletter_images/solibripassword.png"></a><br><br>
                               </td>
                             </tr>
                             <tr>
                               <td align="center" class="button responsive" width="600" style="width: 600px;margin: auto;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;text-align: center;">
                                 <table align="center" border="0" width="200" cellpadding="0" cellspacing="0" style="width: 200px;margin: auto;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;position: relative;border-collapse: collapse !important;">
                                   <tr>
                                     <td align="center" style="font-family: 'Helvetica Neue', helvetica, arial, sans-serif;font-weight: bold;background: #FEC615;color: #333333;padding: 15px 30px;border-radius: 2px;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                                       <div mc:edit="buttonGrey">
                                         <a href="$link" style="text-decoration: none;font-family: 'Helvetica Neue', helvetica, arial, sans-serif;font-weight: bold;color: #363D40;text-transform: uppercase;font-size: 12px;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;">Set password</a>
                                       </div>
                                     </td>
                                   </tr>
                                 </table>
                               </td>
                             </tr>
                             <tr>
                               <td mc:edit="sectionBody" class="sectionBody responsive" width="600" style="width: 600px;text-align: center;padding: 10px 20px;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;" align="center">
                                 <p style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-family: 'Helvetica Neue', helvetica, arial, sans-serif;font-weight: normal;font-size: 14px;line-height: 1.8em;color: #929292;padding: 10px 0;"><br>Once you have <a href="$link" style="color: #333333">set your password</a> you can download trial software from<br>Solibri Solution Center.</p>
                               </td>
                             </tr>

                           </table>
                         </td>
                       </tr>
                     </table>
                   </div>



   </div>

             </table>
           <!-- // END BODY -->
           </td>
         </tr>
         <tr>
           <td align="center" valign="middle" width="100%" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
             <!-- BEGIN FOOTER // -->
             <table border="0" cellpadding="0" width="100%" cellspacing="0" id="templateFooterSocial" style="width: 100%;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;position: relative;background: #ffffff;text-align: center;border-collapse: collapse !important;">
               <tr>
                 <td valign="middle" align="center" width="100%" style="text-align: center;padding: 20px 0;width: 100%;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                   <div mc:edit="footer_content00">
                     <a href="https://www.facebook.com/solibri" style="padding: 10px 15px;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;"><img src="https://solibri-assets.s3.amazonaws.com/old-site/newsletter_images/facebook.jpg" style="width: 30px;-ms-interpolation-mode: bicubic;border: 0;height: auto;line-height: 100%;outline: none;text-decoration: none;" width="30" height="30" alt="Facebook"></a>
                     <a href="https://twitter.com/solibri" style="padding: 10px 15px;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;"><img src="https://solibri-assets.s3.amazonaws.com/old-site/newsletter_images/twitter.jpg" style="width: 30px;-ms-interpolation-mode: bicubic;border: 0;height: auto;line-height: 100%;outline: none;text-decoration: none;" width="30" height="30" alt="Twitter"></a>
                     <a href="mailto:info@solibri.com" style="padding: 10px 15px;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;"><img src="https://solibri-assets.s3.amazonaws.com/old-site/newsletter_images/mail.jpg" style="width: 30px;-ms-interpolation-mode: bicubic;border: 0;height: auto;line-height: 100%;outline: none;text-decoration: none;" width="30" height="30" alt="Mail"></a>
                   </div>
                 </td>
               </tr>
             </table>
             <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateFooter" style="width: 100%;background-color: #666666;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;position: relative;background: #ffffff;border-collapse: collapse !important;">
               <tr>
                 <td valign="top" align="center" width="100%" style="text-align: center;padding: 15px 0;width: 100%;color: #CBCBCB;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;font-family: 'Helvetica Neue', helvetica, arial, sans-serif;font-weight: normal;font-size: 10px;" class="footerText">
                   <div mc:edit="footer_content01">
                     <em>Copyright © <a href="http://www.solibri.com/" style="color: #CBCBCB;">2018 Solibri Inc.</a>, All rights reserved.</em>
                   </div>
                 </td>
               </tr>
             </table>
             <!-- // END FOOTER -->
           </td>
         </tr>
       </table>
       <!-- // END TEMPLATE -->
     </body>
 </html>
EOD;

  } /*end language if/else */

  return trim($message);

}


}
