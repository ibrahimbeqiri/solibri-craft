<?php
/**
 * Solibri Solution module for Craft CMS 3.x
 *
 * Solibri Solution API endpoints
 *
 * @link      http://agencyleroy.com
 * @copyright Copyright (c) 2018 Agency Leroy
 */

namespace modules\solibrisolutionmodule;

use modules\solibrisolutionmodule\services\SolibriSolutionModuleService as SolibriSolutionModuleServiceService;
use modules\solibrisolutionmodule\variables\SolibriSolutionModuleVariable;

use Craft;
use craft\i18n\PhpMessageSource;
use craft\web\UrlManager;
use craft\web\twig\variables\CraftVariable;
use craft\events\RegisterUrlRulesEvent;

use yii\base\Event;
use yii\base\InvalidConfigException;
use yii\base\Module;

/**
 * Class SolibriSolutionModule
 *
 * @author    Agency Leroy
 * @package   SolibriSolutionModule
 * @since     1.0.0
 *
 * @property  SolibriSolutionModuleServiceService $solibriSolutionModuleService
 */
class SolibriSolutionModule extends Module
{
    // Static Properties
    // =========================================================================

    /**
     * @var SolibriSolutionModule
     */
    public static $instance;

    // Public Methods
    // =========================================================================

    /**
     * @inheritdoc
     */
    public function __construct($id, $parent = null, array $config = [])
    {
        Craft::setAlias('@modules/solibrisolutionmodule', $this->getBasePath());
        $this->controllerNamespace = 'modules\solibrisolutionmodule\controllers';

        // Translation category
        $i18n = Craft::$app->getI18n();
        /** @noinspection UnSafeIsSetOverArrayInspection */
        if (!isset($i18n->translations[$id]) && !isset($i18n->translations[$id.'*'])) {
            $i18n->translations[$id] = [
                'class' => PhpMessageSource::class,
                'sourceLanguage' => 'en-US',
                'basePath' => '@modules/solibrisolutionmodule/translations',
                'forceTranslation' => true,
                'allowOverrides' => true,
            ];
        }

        // Set this as the global instance of this module class
        static::setInstance($this);

        parent::__construct($id, $parent, $config);
    }

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        self::$instance = $this;

        Event::on(
            UrlManager::class,
            UrlManager::EVENT_REGISTER_SITE_URL_RULES,
            function (RegisterUrlRulesEvent $event) {
                $event->rules['requestTrial'] = 'modules/solibri-solution-module/api/requestTrial';
                $event->rules['mailchimp'] = 'modules/solibri-solution-module/api/mailchimp';
                $event->rules['requestSMVLicense'] = 'modules/solibri-solution-module/api/requestSMVLicense';
            }
        );

        Event::on(
            CraftVariable::class,
            CraftVariable::EVENT_INIT,
            function (Event $event) {
                /** @var CraftVariable $variable */
                $variable = $event->sender;
                $variable->set('solibriSolutionModule', SolibriSolutionModuleVariable::class);
            }
        );

        Craft::info(
            Craft::t(
                'solibri-solution-module',
                '{name} module loaded',
                ['name' => 'Solibri Solution']
            ),
            __METHOD__
        );
    }

    // Protected Methods
    // =========================================================================
}
