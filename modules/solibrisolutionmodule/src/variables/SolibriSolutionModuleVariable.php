<?php
/**
 * Solibri Solution module for Craft CMS 3.x
 *
 * Solibri Solution API endpoints
 *
 * @link      http://agencyleroy.com
 * @copyright Copyright (c) 2018 Agency Leroy
 */

namespace modules\solibrisolutionmodule\variables;

use modules\solibrisolutionmodule\SolibriSolutionModule;

use Craft;

/**
 * @author    Agency Leroy
 * @package   SolibriSolutionModule
 * @since     1.0.0
 */
class SolibriSolutionModuleVariable
{
  /**
   * @param object $form_data (required)
   *
   * @return object
   *
   * How you use in twig templates
   * {{ craft.solibriSolutionModule.requestTrial($form_data) }}
   */
  public function requestTrial($form_data)
  {

    $response = SolibriSolutionModule::$instance->solibriSolutionModuleService->requestTrial($form_data);

    return $response;
  }

  /**
   * @param object $form_data (required)
   *
   * @return object
   *
   * How you use in twig templates
   * {{ craft.solibriSolutionModule.requestSMVLicense($form_data) }}
   */
  public function requestSMVLicense($form_data)
  {

    $response = SolibriSolutionModule::$instance->solibriSolutionModuleService->requestSMVLicense($form_data);

    return $response;
  }
}
