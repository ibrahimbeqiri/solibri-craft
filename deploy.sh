environment=$1

current_release=$2
keep_releases=5
last_release=$(expr $current_release - $keep_releases)

case $environment in
staging)
  server='ubuntu@staging.solibri.com'
  ;;
production)
  server='ubuntu@34.255.177.75'
  ;;
esac

echo "===== deploying release $current_release to $environment on $server"

echo "===== copying files to $server:/home/ubuntu/solibri/releases/$current_release"
rsync --recursive --copy-links --exclude 'node_modules' ./ "$server:/home/ubuntu/solibri/releases/$current_release"

echo "===== linking folders and files"
ssh -A "$server" "
  ln -fsn /home/ubuntu/solibri/releases/$current_release /home/ubuntu/solibri/current
  ln -fsn /home/ubuntu/solibri/shared/web/upload /home/ubuntu/solibri/current/web/uploads
  ln -fsn /home/ubuntu/solibri/shared/storage /home/ubuntu/solibri/current/storage
  ln -fsn /home/ubuntu/solibri/shared/.env /home/ubuntu/solibri/current/
  ln -fsn /home/ubuntu/solibri/shared/robots.txt /home/ubuntu/solibri/current/web/
  "

echo "===== restarting fpm"
ssh -A "$server" "sudo /etc/init.d/php7.0-fpm restart"

echo "===== removing releases older than $last_release"
ssh -A "$server" "
  for ((i=0; i<$last_release; i++)); do rm -rf /home/ubuntu/solibri/releases/\$i; done;
  "

echo "===== finished deploying $environment to $server ######"
