<?php
/**
 * sprout-forms-countries plugin for Craft CMS 3.x
 *
 * Countries Field for Sprout Forms
 *
 * @link      http://agencyleroy.com
 * @copyright Copyright (c) 2018 Agency Leroy
 */

/**
 * sprout-forms-countries en Translation
 *
 * Returns an array with the string to be translated (as passed to `Craft::t('sprout-forms-countries', '...')`) as
 * the key, and the translation as the value.
 *
 * http://www.yiiframework.com/doc-2.0/guide-tutorial-i18n.html
 *
 * @author    Agency Leroy
 * @package   Sproutformscountries
 * @since     1.0.0
 */
return [
    'sprout-forms-countries plugin loaded' => 'sprout-forms-countries plugin loaded',
];
