<?php

namespace sproutformscountries\integrations\sproutforms\fields;

use Craft;
use craft\helpers\Template as TemplateHelper;
use craft\base\ElementInterface;
use craft\base\PreviewableFieldInterface;
use CommerceGuys\Addressing\Repository\CountryRepository;

use barrelstrength\sproutforms\base\FormField;

/**
 * Class Countries
 *
 * @package Craft
 */
class Countries extends FormField implements PreviewableFieldInterface
{
    /**
     * @var string
     */
    public $cssClasses;

    /**
     * @var string|null Default Country
     */
    public $defaultCountry;

    /**
     * @var mixed All Countries
     */
    public $options;

    public function init()
    {
        if (is_null($this->options)){
            $this->options = $this->getOptions();
        }
        parent::init();
    }

    /**
     * @inheritdoc
     */
    public static function displayName(): string
    {
        return Craft::t('sprout-forms-countries', 'Countries');
    }

    /**
     * @return string
     */
    public function getSvgIconPath()
    {
        return '@sproutformscountriesicons/map.svg';
    }

    /**
     * @inheritdoc
     *
     * @throws \Twig_Error_Loader
     * @throws \yii\base\Exception
     */
    public function getSettingsHtml()
    {
        $rendered = Craft::$app->getView()->renderTemplate(
            'sprout-forms-countries/_integrations/sproutforms/formtemplates/fields/countries/settings',
            [
                'field' => $this,
                'options' => $this->options
            ]
        );

        return $rendered;
    }

    /**
     * @inheritdoc
     *
     * @throws \Twig_Error_Loader
     * @throws \yii\base\Exception
     */
    public function getInputHtml($value, ElementInterface $element = null): string
    {
        return Craft::$app->getView()->renderTemplate('_includes/forms/select',
            [
                'name' => $this->handle,
                'value' => $value ?? $this->defaultCountry,
                'options' => $this->options
            ]
        );
    }

    /**
     * @inheritdoc
     *
     * @throws \Twig_Error_Loader
     * @throws \yii\base\Exception
     */
    public function getExampleInputHtml()
    {
        $options = $this->getOptions();

        return Craft::$app->getView()->renderTemplate('sprout-forms-countries/_integrations/sproutforms/formtemplates/fields/countries/example',
            [
                'field' => $this,
                'options' => $this->options
            ]
        );
    }

    /**
     * @inheritdoc
     *
     * @throws \Twig_Error_Loader
     * @throws \yii\base\Exception
     */
    public function getFrontEndInputHtml($value, array $renderingOptions = null): string
    {
        $rendered = Craft::$app->getView()->renderTemplate(
            'countries/input',
            [
                'name' => $this->handle,
                'value' => $value ?? $this->defaultCountry,
                'field' => $this,
                'options' => $this->options,
                'renderingOptions' => $renderingOptions
            ]
        );

        return TemplateHelper::raw($rendered);
    }

    /**
     * @inheritdoc
     */
    public function getTemplatesPath()
    {
        return Craft::getAlias('@sproutformscountries/templates/_integrations/sproutforms/formtemplates/fields/');
    }

    /**
     * Return countries as options for select field
     *
     * @return array
     */
    private function getOptions()
    {
        $options = [
          "0"=> Craft::t('sprout-forms-countries', 'Select...'),
          "AF"=> "Afghanistan",
          "AL"=> "Albania",
          "DZ"=> "Algeria",
          "AS"=> "American Samoa",
          "AD"=> "Andorra",
          "AO"=> "Angola",
          "AI"=> "Anguilla",
          "AQ"=> "Antarctica",
          "AG"=> "Antigua and Barbuda",
          "AR"=> "Argentina",
          "AM"=> "Armenia",
          "AW"=> "Aruba",
          "AU"=> "Australia",
          "AT"=> "Austria",
          "AZ"=> "Azerbaijan",
          "BS"=> "Bahamas",
          "BH"=> "Bahrain",
          "BD"=> "Bangladesh",
          "BB"=> "Barbados",
          "BY"=> "Belarus",
          "BE"=> "Belgium",
          "BZ"=> "Belize",
          "BJ"=> "Benin",
          "BM"=> "Bermuda",
          "BT"=> "Bhutan",
          "BO"=> "Bolivia",
          "BA"=> "Bosnia and Herzegovina",
          "BW"=> "Botswana",
          "BR"=> "Brazil",
          "BN"=> "Brunei",
          "BG"=> "Bulgaria",
          "BF"=> "Burkina Faso",
          "BI"=> "Burundi",
          "KH"=> "Cambodia",
          "CM"=> "Cameroon",
          "CA"=> "Canada",
          "CV"=> "Cape Verde",
          "KY"=> "Cayman Islands",
          "CF"=> "Central African Republic",
          "TD"=> "Chad",
          "CL"=> "Chile",
          "CN"=> "China",
          "CX"=> "Christmas Island",
          "CC"=> "Cocos (Keeling) Islands",
          "CO"=> "Colombia",
          "KM"=> "Comoros",
          "CD"=> "Congo",
          "CK"=> "Cook Islands",
          "CR"=> "Costa Rica",
          "CI"=> "Cote D'ivoire",
          "HR"=> "Croatia",
          "CU"=> "Cuba",
          "CY"=> "Cyprus",
          "CZ"=> "Czech Republic",
          "DK"=> "Denmark",
          "DG"=> "Diego Garcia",
          "DJ"=> "Djibouti",
          "DM"=> "Dominica",
          "DO"=> "Dominican Republic",
          "EC"=> "Ecuador",
          "EG"=> "Egypt",
          "SV"=> "El Salvador",
          "GQ"=> "Equatorial Guinea",
          "ER"=> "Eritrea",
          "EE"=> "Estonia",
          "ET"=> "Ethiopia",
          "FO"=> "Faroe Islands",
          "FJ"=> "Fiji",
          "FI"=> "Finland",
          "FR"=> "France",
          "GF"=> "French Guiana",
          "PF"=> "French Polynesia",
          "GA"=> "Gabon",
          "GM"=> "Gambia",
          "GE"=> "Georgia",
          "DE"=> "Germany",
          "GH"=> "Ghana",
          "GI"=> "Gibraltar",
          "GR"=> "Greece",
          "GL"=> "Greenland",
          "GD"=> "Grenada",
          "GP"=> "Guadeloupe",
          "GU"=> "Guam",
          "GT"=> "Guatemala",
          "GN"=> "Guinea",
          "GW"=> "Guinea-Bissau",
          "GY"=> "Guyana",
          "HT"=> "Haiti",
          "HN"=> "Honduras",
          "HK"=> "Hong Kong",
          "HU"=> "Hungary",
          "IS"=> "Iceland",
          "IN"=> "India",
          "ID"=> "Indonesia",
          "IR"=> "Iran",
          "IQ"=> "Iraq",
          "IE"=> "Ireland",
          "IL"=> "Israel",
          "IT"=> "Italy",
          "JM"=> "Jamaica",
          "JP"=> "Japan",
          "JO"=> "Jordan",
          "KZ"=> "Kazakhstan",
          "KE"=> "Kenya",
          "KI"=> "Kiribati",
          "KW"=> "Kuwait",
          "KG"=> "Kyrgyzstan",
          "LV"=> "Latvia",
          "LB"=> "Lebanon",
          "LS"=> "Lesotho",
          "LR"=> "Liberia",
          "LY"=> "Libyan Arab Jamahiriya",
          "LI"=> "Liechtenstein",
          "LT"=> "Lithuania",
          "LU"=> "Luxembourg",
          "MO"=> "Macao",
          "MK"=> "Macedonia",
          "MG"=> "Madagascar",
          "MW"=> "Malawi",
          "MY"=> "Malaysia",
          "MV"=> "Maldives",
          "ML"=> "Mali",
          "MT"=> "Malta",
          "MH"=> "Marshall Islands",
          "MQ"=> "Martinique",
          "MR"=> "Mauritania",
          "MU"=> "Mauritius",
          "YT"=> "Mayotte",
          "MX"=> "Mexico",
          "MD"=> "Moldova, Republic of",
          "MC"=> "Monaco",
          "MN"=> "Mongolia",
          "MS"=> "Montserrat",
          "MA"=> "Morocco",
          "MZ"=> "Mozambique",
          "MM"=> "Myanmar",
          "NA"=> "Namibia",
          "NR"=> "Nauru",
          "NP"=> "Nepal",
          "NL"=> "Netherlands",
          "AN"=> "Netherlands Antilles",
          "NC"=> "New Caledonia",
          "NZ"=> "New Zealand",
          "NI"=> "Nicaragua",
          "NE"=> "Niger",
          "NG"=> "Nigeria",
          "NU"=> "Niue",
          "NF"=> "Norfolk Island",
          "KP"=> "North Korea",
          "NO"=> "Norway",
          "OM"=> "Oman",
          "PK"=> "Pakistan",
          "PW"=> "Palau",
          "PA"=> "Panama",
          "PG"=> "Papua New Guinea",
          "PY"=> "Paraguay",
          "PE"=> "Peru",
          "PH"=> "Philippines",
          "PN"=> "Pitcairn",
          "PL"=> "Poland",
          "PT"=> "Portugal",
          "PR"=> "Puerto Rico",
          "QA"=> "Qatar",
          "RE"=> "Reunion",
          "RO"=> "Romania",
          "RU"=> "Russia",
          "RW"=> "Rwanda",
          "SH"=> "St. Helena",
          "LC"=> "St. Lucia",
          "WS"=> "Samoa",
          "SM"=> "San Marino",
          "SA"=> "Saudi Arabia",
          "SN"=> "Senegal",
          "CS"=> "Serbia and Montenegro",
          "SC"=> "Seychelles",
          "SL"=> "Sierra Leone",
          "SG"=> "Singapore",
          "SK"=> "Slovakia",
          "SI"=> "Slovenia",
          "SB"=> "Solomon Islands",
          "SO"=> "Somalia",
          "ZA"=> "South Africa",
          "KR"=> "South Korea",
          "ES"=> "Spain",
          "LK"=> "Sri Lanka",
          "SD"=> "Sudan",
          "SR"=> "Suriname",
          "SZ"=> "Swaziland",
          "SE"=> "Sweden",
          "CH"=> "Switzerland",
          "SY"=> "Syria",
          "TW"=> "Taiwan",
          "TJ"=> "Tajikistan",
          "TZ"=> "Tanzania",
          "TH"=> "Thailand",
          "TL"=> "Timor-Leste",
          "TG"=> "Togo",
          "TK"=> "Tokelau",
          "TO"=> "Tonga",
          "TT"=> "Trinidad and Tobago",
          "TN"=> "Tunisia",
          "TR"=> "Turkey",
          "TM"=> "Turkmenistan",
          "TV"=> "Tuvalu",
          "UG"=> "Uganda",
          "UA"=> "Ukraine",
          "AE"=> "United Arab Emirates",
          "GB"=> "United Kingdom",
          "US"=> "United States",
          "UY"=> "Uruguay",
          "UZ"=> "Uzbekistan",
          "VU"=> "Vanuatu",
          "VE"=> "Venezuela",
          "VN"=> "Vietnam",
          "WF"=> "Wallis and Futuna",
          "EH"=> "Western Sahara",
          "YE"=> "Yemen",
          "ZM"=> "Zambia",
          "ZW"=> "Zimbabwe"
        ];

        return $options;
    }
}
