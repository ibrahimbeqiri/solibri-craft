<?php
/**
 * General Configuration
 *
 * All of your system's general configuration settings go in here. You can see a
 * list of the available settings in vendor/craftcms/cms/src/config/GeneralConfig.php.
 *
 * @see craft\config\GeneralConfig
 */

define('URI_SCHEME',  ( isset($_SERVER['HTTPS'] ) ) ? "https://" : "http://" ); // Ensure our urls have the right scheme

define('SITE_URL',    URI_SCHEME . $_SERVER['SERVER_NAME'] . '/'); // The site url

define('BASEPATH',    dirname(__DIR__) . '/'); // The site basepath

define('BROWSER_LANG', $_SERVER['HTTP_ACCEPT_LANGUAGE']);

return [
    // Global settings
    '*' => [
        // Strip special characters from files uploaded to craft
        'convertFilenamesToAscii' => true,

        // Convert slugs to slugs withouth special characters
        'limitAutoSlugsToAscii' => true,

        // Set default language in craft cp
        'defaultCpLanguage' => 'en-US',

        // Default Week Start Day (0 = Sunday, 1 = Monday...)
        'defaultWeekStartDay' => 1,

        // Enable CSRF Protection (recommended)
        'enableCsrfProtection' => true,

        // Whether generated URLs should omit "index.php"
        'omitScriptNameInUrls' => true,

        // Pagination url, triggers pagination
        'pageTrigger' => 'page-',

        'defaultImageQuality' => 82,

        // Control Panel trigger word
        'cpTrigger' => 'admin',

        // The secure key Craft will use for hashing and encrypting data
        'securityKey' => getenv('SECURITY_KEY'),

        'aliases' => [
          '@assetBaseUrl' => SITE_URL.'uploads',
          '@assetBasePath' => BASEPATH.'web/uploads',
        ],

        'staticAssets' => SITE_URL.'assets',

        // Base site URL
        'siteUrl' => [
            'en' => SITE_URL,
            'de' => SITE_URL . 'de/',
            'es' => SITE_URL . 'es/',
            'fi' => SITE_URL . 'fi/',
            'ja' => SITE_URL . 'ja/'
        ],
        'browserLang' => substr(BROWSER_LANG, 0, 2),
        'md5date' => md5(date("m.d.y")."salt"),

        'tagManagerId' => getenv('GOOGLE_TAG_MANAGER_ID'),

        'sscApiUrl' => 'https://solution.solibri.com/'
    ],

    // Dev environment settings
    'dev' => [
        // Disable template caching for dev environment
        'enableTemplateCaching' => false,

        // Dev Mode (see https://craftcms.com/support/dev-mode)
        'devMode' => true,

        'sscApiUrl' => 'https://solution.solibri.com/'
    ],

    // Staging environment settings
    'staging' => [

        // Dev Mode (see https://craftcms.com/support/dev-mode)
        'devMode' => false,

        'sscApiUrl' => 'https://solution.solibri.com/'
    ],
];
