<?php
/**
 * Yii Application Config
 *
 * Edit this file at your own risk!
 *
 * The array returned by this file will get merged with
 * vendor/craftcms/cms/src/config/app.php and app.[web|console].php, when
 * Craft's bootstrap script is defining the configuration for the entire
 * application.
 *
 * You can define custom modules and system components, and even override the
 * built-in system components.
 *
 * If you want to modify the application config for *only* web requests or
 * *only* console requests, create an app.web.php or app.console.php file in
 * your config/ folder, alongside this one.
 */

return [
  'modules' => [
    'authy-module' => [
      'class' => \modules\authymodule\AuthyModule::class,
      'components' => [
        'authyModuleService' => [
          'class' => 'modules\authymodule\services\AuthyModuleService',
        ],
      ],
    ],
    'solibri-solution-module' => [
      'class' => \modules\solibrisolutionmodule\SolibriSolutionModule::class,
      'components' => [
        'solibriSolutionModuleService' => [
          'class' => 'modules\solibrisolutionmodule\services\SolibriSolutionModuleService',
        ],
      ],
    ],
    'pdf-diploma-module' => [
      'class' => \modules\pdfdiplomamodule\PdfdiplomaModule::class,
    ],
    'salesforce-module' => [
      'class' => \modules\salesforcemodule\SalesforceModule::class,
      'components' => [
        'salesforceModuleService' => [
          'class' => 'modules\salesforcemodule\services\SalesforceModuleService',
        ],
      ],
    ],
    'sprout-forms-custom-report-module' => [
      'class' => \modules\sproutformscustomreportmodule\SproutFormsCustomReportModule::class,
      'components' => [
        'sproutFormsCustomReportModuleService' => [
          'class' => 'modules\sproutformscustomreportmodule\services\SproutFormsCustomReportModuleService',
        ],
      ],
    ],
  ],
  'bootstrap' => ['authy-module', 'solibri-solution-module', 'pdf-diploma-module', 'salesforce-module', 'sprout-forms-custom-report-module'],
];