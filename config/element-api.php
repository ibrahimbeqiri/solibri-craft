<?php

use craft\elements\Entry;
use craft\elements\Tag;
use craft\helpers\UrlHelper;

return [
  'endpoints' => [
    'api/offices' => [
      'elementType' => Entry::class,
      'criteria' => [
        'search' => (Craft::$app->request->getQueryParam('search')) ? '*'.str_replace(' ', '* OR *', Craft::$app->request->getQueryParam('search').'*') : '',
        'section' => 'offices',
        'orderBy' => 'title asc'
      ],
      'paginate' => false,
      'elementsPerPage' => 100,
      'transformer' => function(Entry $entry) {

        $tags = [];
        $countries = [];

        foreach ($entry['countryCategories']->all() as $country) {
          array_push($countries, $country->title);
        }

        foreach ($entry['officeTags']->all() as $tag) {
          array_push($tags, $tag->title);
        }

        $mainCountry = null;

        if (count($entry->mainCountry)) {
          $mainCountry = $entry->mainCountry[0]->title;
        } elseif (count($entry->countryCategories)) {
          $mainCountry = $entry->countryCategories[0]->title;
        }

        return [
          'title' => $entry->title,
          'mainCountry' => $mainCountry,
          'countries' => $countries,
          'streetAddress' => $entry->streetAddress,
          'email' => $entry->email,
          'supportEmail' => $entry->supportEmail ? $entry->supportEmail : $entry->email,
          'website' => $entry->website,
          'phoneNumber' => $entry->phoneNumber,
          'id' => $entry->id,
          'tags' => join(", ", $tags)
        ];
      },
    ],
    'api/offices/tags' => [
      'elementType' => Tag::class,
      'criteria' => [
        'group' => 'officeTags',
        'order' => 'title'
      ],
      'paginate' => false,
      'elementsPerPage' => 100,
      'transformer' => function(Tag $tag) {
        return [
          'title' => $tag->title
        ];
      },
    ],
    'api/search' => [
      'elementType' => Entry::class,
      'criteria' => [
        'search' => (Craft::$app->request->getQueryParam('q')) ? '*'.str_replace(' ', '* OR *', Craft::$app->request->getQueryParam('q').'*') : '',
        'section' => 'pages, news, learn, careers, offices, about, brand, careersLanding, contact, howItWorks, press, support',
        'order' => 'score'
      ],
      'elementsPerPage' => 5,
      'pageParam' => 'pg',
      'transformer' => function(Entry $entry) {

        $url = ($entry->type == 'office') ?  "/contact?search=".$entry->title."#officeSearchApp" : $entry->url;

        return [
          'title' => $entry->title,
          'url' => $url
        ];
      },
    ],
    'api/learn/search' => [
      'elementType' => Entry::class,
      'criteria' => [
        'search' => (Craft::$app->request->getQueryParam('q')) ? '*'.str_replace(' ', '* OR *', Craft::$app->request->getQueryParam('q').'*') : '',
        'section' => 'learn',
        'order' => 'score'
      ],
      'elementsPerPage' => 5,
      'pageParam' => 'pg',
      'transformer' => function(Entry $entry) {
        return [
          'title' => $entry->title,
          'url' => $entry->url
        ];
      },
    ],
  ]
];
