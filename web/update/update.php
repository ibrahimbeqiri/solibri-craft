﻿<html>
<head>
<title>Solibri Product Update</title>
<link rel="SHORTCUT ICON" href="./favicon.ico">
</head>
<body>
<table height="100%" align="center" width="962" border="0" cellspacing="0" cellpadding="0">

	<tr>
		<td align="center" valign="middle"><a href="http://www.solibri.com/"><img src="./header.png" border="0" alt="Solibri, Inc. The World Leader in Model Checking" /></a></td>
	</tr>
	<tr height="100%">

		<td valign="top">
			<br/>
			    <p>
				<h2>Checking for Updates...</h2>
				</p>
				<?php
				
					/*
					 * Returns the major version. Like: 9.1 from 9.1.12.
					 */
					function get_major_version($x) {
						$x_group = explode ('.', $x);
						$size = sizeof($x_group);
						if($size > 1) {
							return $x_group[0].'.'.$x_group[1];
						} else {
							return $x_group[0];
						}
					}
				
					/*
					 * Takes in two version strings x & y.
					 * Returns true if x < y
					 * Verions are strings of digit groups like 9.1.10.4
					 * Where 9 is most significant and 4 is least significant group.
					 */
					
					function is_less_than($x, $y)  {
						if(strlen($x) < 1 || strlen($y) < 1) {
							return true;
						}

						$result = 0; // false

						$x_group = explode (".", $x); // x_group would be now array of individual groups
						$y_group = explode (".", $y); 

						$size = sizeof($x_group);

						for($i=0; $i < $size; $i++) {
							$xval = intval($x_group[$i]);
							$yval = intval($y_group[$i]);

							if($xval < $yval) {
								$result = 1; // true
								break;
							}elseif ($xval > $yval) {
								$result = 0; //false
								break;
							} // looks for next only when values at this group are equal

						}
						return $result;
					}
					
					/* Display relevant messages when SMC version is 8 or less */
					
					function print_msg($prod, $uver, $sver)  { // prod = product, uver = user version, sver = latest released SMC version
						
						print '<p><h2>Please update '.$prod.'!</h2></p>';
						print '<p><h3>Your Version:'.$uver.'</h3></p>';

						if(strcmp($prod, 'Solibri Model Checker') == 0) {
							print '<p><h3>Solibri Model Checker v' .$sver. ' is released.</h3></p>';
							print '<p>- To use it you need a <a href="https://solution.solibri.com">Solibri Solution Center</a> account. Please contact <a href="mailto:support@solibri.com">support@solibri.com</a> for more information</p>';
							print '<p>- Files saved with Solibri Model Checker v' .$sver. ' cannot be opened with Solibri Model Checker v' .$uver.' or earlier versions.</p>';
						} else {
							print '<p>If you use the desktop integration that allows to use Solibri Model Viewer offline it is recommended time to time update your version by running the software from link provided on the Solibri Model Viewer page. The update process is automatic.</p>';						
							print '<p><a href="http://www.solibri.com/solibri-model-viewer.html">Solibri Model Viewer</a></p>';
						}
					}				
					
					$user_product = $_REQUEST['product'];
					$user_version = $_REQUEST['buildnumber']; 

					$latest_major_version = "9.9";
					$latest_versions = array(
						"9.9"  => "9.9.5.112",
						"9.8"  => "9.8.47",
						"9.7"  => "9.7.19",
						"9.6"  => "9.6.21",
						"9.5"  => "9.5.28",
						"9.1"  => "9.1.29",
						"9.0"  => "9.0.10.11",
						"8.1"  => "8.1.0.80",
						"8.0"  => "8.0.0.104",
						"7.1"  => "7.1.0.103",
						"7.0"  => "7.0.0.224",
					);

					$user_major_version = get_major_version($user_version);
					$latest_user_version = $latest_versions[$user_major_version];
					
					if(strlen($user_product) < 1 || strlen($user_version) < 1) {
						print '<p><h3>Checking for updates failed.</h3></p>';
						print '<p><h3>Please, contact Solibri <a href="mailto:support@solibri.com" class="style1A">support</a>.</h3></p>';
					} else if(is_less_than($user_major_version, $latest_major_version)) {

						print '<p><h2>Solibri Model Checker v' .$latest_major_version. ' is released.</h2></p>';
						print '<p>- <a href="https://solution.solibri.com">Solibri Solution Center</a> account is needed. Please contact <a href="mailto:support@solibri.com">support@solibri.com</a> for more information</p>';
						print '<p>- The Solibri Model Checker v9.9 models are backward compatible with Solibri Model Checker v9.8, v9.7 and v9.6.</p>';
						//print '<p>- Files saved with Solibri Model Checker v' .$latest_major_version. ' cannot be opened with earlier versions.</p>';
					}
					
				    if(strcmp($user_product, 'Solibri Model Checker') == 0) { // for versions > 8.x
						if(is_less_than($user_version, $latest_user_version) == 1){
							print '<p><h2>Please update '.$user_product.'!</h2></p>';
							print '<p><h3>Your Version: '.$user_version.'</h3></p>';
							print '<p><h3>Latest Version: '.$latest_user_version.'</h3></p>';
							print '<p>Login <a href="https://solution.solibri.com">Solibri Solution Center</a> download the latest version.</p>';
						} else {
							print '<p><h2>'.$user_product.' is up-to-date.</h2></p>';
						}
					}
	
					print '<p><a href="http://www.solibri.com/category/release-notes/">Release Notes</a></p>';
				?>
				<hr>
				<h2>Professional Support</h2>
				<h3><a href="http://www.solibri.com/support/">Contact Support</a></h3>
			</td>
		</tr>
		<tr height="16">
    		<td align="center" colspan="2" height="15" valign="top" > 
      			<small>Copyright © 2019 Solibri, Inc ®. Solibri is a registered trademark. All other product or company names used are the trademarks or registered trademarks of their respective owners.</small>
			</td>
		</tr>
	</table>
</body>
